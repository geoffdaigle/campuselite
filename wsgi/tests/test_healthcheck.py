'''
    A Django Test Suite health check
'''

from django.test import Client, TestCase

class HealthCheck(TestCase):
   
    def test_assert(self):
        self.assertEqual( True, True )

    def test_site_is_up(self):
        c = Client()
        response = c.get('/')
        # Expect a 302 redirect because app.thecampuselite.com 
        # doesnt have its own home page.
        self.assertEqual( response.status_code, 302 )

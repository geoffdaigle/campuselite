import django
from django import forms as f
from django.contrib.auth.hashers import *
from campuselite.shortcuts import *
from ce_user.models import *
import re


class PassResetFormPassword( f.Form ):
    password1 = f.CharField(max_length=100, min_length=6, widget=f.widgets.PasswordInput)
    password2 = f.CharField(max_length=100, min_length=6, widget=f.widgets.PasswordInput)

    def __init__(self, user=None, prefix=None, data=None, initial=None):
        self.user = user
        super(PassResetFormPassword, self).__init__(prefix=prefix, data=data, initial=initial)


    def clean( self ):
        password1 = self.cleaned_data.get('password1', '')
        password2 = self.cleaned_data.get('password2', '')

        if password1 != password2:
            raise f.ValidationError(u"Your passwords did not match.")

        return self.cleaned_data

    def change_password( self ):
        self.user.password = make_password(self.cleaned_data.get('password1', ''))
        self.user.save()
        return True


class PassResetForm( f.Form ):
    email = f.EmailField()

    def __init__(self, prefix=None, data=None, initial=None):
        self.user = None
        super(PassResetForm, self).__init__(prefix=prefix, data=data, initial=initial)

    def clean( self ):
        email = self.cleaned_data.get('email', '')

        if not email:
            raise f.ValidationError(u"Somehow... you forgot to type in your email address...")

        try:
            self.user = CeUser.objects.get(email=email)
        except CeUser.DoesNotExist:
            raise f.ValidationError(u"We can't find that email address in our system.")
            
    def send_reset_email( self, rq ):
        import hashlib
        passkey = hashlib.sha224('cesalt'+self.user.email).hexdigest()[:15]

        m = "<p><strong>%s,</strong></p><br>" % self.user.first_name

        m += "<p>Follow this link to reset your password:</p>"
        m += "<p><a href=\"%s/verify-account/%s\">%s/password-reset/%s</a></p>" % (settings.MAIN_SITE, passkey, settings.MAIN_SITE, passkey)
        m += "<br><p>Thanks!</p>"
        m += "<p>- Campus Elite</p>"

        t = compile_email_template({ "body_content": m })
        send_user_email( rq, {
            "subject": "Password reset request",
            "message": t,
            "to": [self.user.email],
            "from": 'Campus Elite <alerts@thecampuselite.com>',
        } )


'''
	Handles user log-ins
	Join actions use this form to log in AFTER account is created
'''
class LoginForm(f.Form):
    email = f.EmailField()
    password = f.CharField( max_length=50, widget=f.widgets.PasswordInput )

    def __init__(self, prefix=None, data=None, initial=None):
        self.user = None
        super(LoginForm, self).__init__(prefix=prefix, data=data, initial=initial)


    def clean( self ):
    	email = self.cleaned_data.get('email', '')
    	password = self.cleaned_data.get('password', '')

    	if not email:
    		raise f.ValidationError(u"Somehow... you forgot to type in your email address...")
    	elif not password:
    		raise f.ValidationError(u"Please type in your password to log in.")


    	try:
    		self.user = CeUser.objects.get(email=email)
    	except CeUser.DoesNotExist:
    		raise f.ValidationError(u"We can't find that email address in our system.")
    		
    	if not check_password( password, self.user.password):
    		raise f.ValidationError(u"We found your account, but your password is incorrect.")




    def do_login( self, rq, user=None ):
        # so we can do a login without validating... like for joins
        if user:
            self.user = user

    	# for now, just fails silently because this wont usually have an issue
    	try:
	    	if "_auth_user_id" in rq.session:
	    	    if rq.session["_auth_user_id"] != self.user.pk:
	    	        rq.session.flush()
	    	else:
	    	    rq.session.cycle_key()
	    	    
	    	rq.session["_auth_user_id"] = self.user.pk
	    	if hasattr(rq, 'user'):
	    	    rq.user = self.user

	    	if hasattr(rq, 'auth'):
	    	    rq.auth = None
	    	
	    	rq.session.set_expiry(0)

	    	self.user.last_login = django.utils.timezone.now()
	    	self.user.save()

    		return True
    	except:
    		return False







ALLOWED_SCHOOL_ADMIN = 1
ALLOWED_SCHOOL_BU = 2
ALLOWED_SCHOOL_EMAILS = (
    (ALLOWED_SCHOOL_ADMIN, 'thecampuselite.com'),
    (ALLOWED_SCHOOL_BU, 'bu.edu'),
)



'''
    Handles user joins
    Creates every part of a new user's account in the database
'''
class JoinForm(f.Form):
    first_name = f.CharField( max_length=50 )
    last_name = f.CharField( max_length=50 )
    email = f.EmailField()
    password = f.CharField( max_length=50, min_length=6, widget=f.widgets.PasswordInput )

    def __init__(self, prefix=None, data=None, initial=None):
        self.user = None
        super(JoinForm, self).__init__(prefix=prefix, data=data, initial=initial)


    def clean( self ):
        email = self.cleaned_data.get('email', '')

        try:
            u = CeUser.objects.get(email=email)
            raise f.ValidationError(u"That email address is already in use. Did you already join?")
        except CeUser.DoesNotExist:
            pass

        return self.cleaned_data


    def clean_password( self ):
        p = self.cleaned_data.get("password","")
        has_character = False

        import re
        has_character = not re.match('^\w*$', p)

        if not has_character:
            raise f.ValidationError(u"Add at least one special character ( ! @ # $ % ^ & * ).")

        return p


    def clean_email( self ):
        email = self.cleaned_data.get("email","").lower()
        try:
            email_end = email.split('@')[1]
        except IndexError:
            raise f.ValidationError(u"Please provide a valid email address.")

        for k, v in ALLOWED_SCHOOL_EMAILS:
            if email == v or email_end == v:
                return email

        all_list = []
        for k, v in ALLOWED_SCHOOL_EMAILS:
            if k > 1:
                all_list.append( v )

        raise f.ValidationError(u"Sorry, that email address is not for a school we support! (Currently "+(", ".join(all_list))+")")



    def do_join( self, rq ):
        cd = self.cleaned_data

        import random

        user = CeUser()
        user.is_superuser = 0
        user.email = cd['email']

        # generate a unique username
        unique_username = False
        tries = 0
        basic_username = re.sub(r'\W+', '', cd['first_name'].lower().strip()) +'-'+ re.sub(r'\W+', '', cd['last_name'].lower().strip())
        while unique_username == False:
            usn = basic_username
            if tries > 0:
               usn += '-'+str(tries)
               
            try:
                u = CeUser.objects.get(username=usn)
                tries = tries + 1
            except CeUser.DoesNotExist:
                unique_username = usn


        user.username = unique_username
        user.first_name = cd['first_name'].strip()
        user.last_name = cd['last_name'].strip()
        user.password = make_password(cd['password'])
        user.save()

        up = CeUserProfile()
        up.user = user
        up.save()

        us = CeUserSettings()
        us.user = user
        us.save()

        un = CeUserNotificationSettings()
        un.user = user
        un.save()

        return user
        




'''
    Puts a photo file into storage before being saved in the DB
'''
class TempPhotoUploadForm( f.Form ):
    photo = f.FileField()




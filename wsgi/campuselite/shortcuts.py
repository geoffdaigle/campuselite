from django.shortcuts import render_to_response
from django.template import RequestContext
from django.template import Context
from campuselite import settings
from pytz import timezone
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from ce_messaging.models import *



'''
	Shorcut for rendering a view to http response.

	This could have been done with TEMPLATE_CONTEXT_PROCESSORS setting: 
	https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
	...but having it all in one place for now is fine.
'''
def render_template(requestobject, template_name, data_dict={}, nav=''):

	data_dict.update({
		"rq": requestobject,
		"settings": settings,
		"timezone": timezone( settings.TIME_ZONE ),
		'nav_active': nav,
	})

	# if we have a flash message to show at the top of the screen 
	try:
		data_dict.update({ 
			"flash_message": requestobject.flash_message
		})
		del requestobject.flash_message
	except AttributeError:
		pass
	try:
		data_dict.update({ 
			"success_message": requestobject.success_message
		})
		del requestobject.success_message
	except AttributeError:
		pass
	try:
		data_dict.update({ 
			"error_message": requestobject.error_message
		})
		del requestobject.error_message
	except AttributeError:
		pass


	# if we have a session var to show a flash message after a redirect
	try:
		data_dict.update({ 
			"flash_message": requestobject.session['flash_message']
		})
		del requestobject.session['flash_message']
	except KeyError:
		pass
	try:
		data_dict.update({ 
			"success_message": requestobject.session['success_message']
		})
		del requestobject.session['success_message']
	except KeyError:
		pass
	try:
		data_dict.update({ 
			"error_message": requestobject.session['error_message']
		})
		del requestobject.session['error_message']
	except KeyError:
		pass



	return render_to_response(
				template_name+'.html',
				data_dict, 
				context_instance = RequestContext( requestobject ) )




'''
	Creates an html email and returns it as a string
'''
def compile_email_template( context={}, template_name=None ):
	if not template_name:
		template_name = 'email-basic.html'

	return get_template('email/'+template_name).render( Context(context) )




'''
	send a message via email from the user 
'''
def send_user_email( rq=None, message={} ):

	# strip html tags 
	# from http://stackoverflow.com/questions/753052/strip-html-from-strings-in-python
	# THIS SHOULD BE MOVED TO A LIBRARY
	from HTMLParser import HTMLParser
	class MLStripper(HTMLParser):
	    def __init__(self):
	        self.reset()
	        self.fed = []
	    def handle_data(self, d):
	        self.fed.append(d)
	    def get_data(self):
	        return ''.join(self.fed)

	def strip_tags(html):
	    s = MLStripper()
	    s.feed(html)
	    return s.get_data()


	html_content = message.get('message', None)
	text_content = strip_tags( html_content )
	t = message.get('to', None)

	# silent fail if no message
	if html_content == None or t == None:
		return False

	f =  message.get('from', 'Campus Elite <alerts@thecampuselite.com>')

	msg = EmailObject()
	msg.subject = message.get("subject", 'Message from Campus Elite')
	msg.text = text_content
	msg.html = html_content
	msg.to = t[0]
	msg.fromuser = f
	msg.save()



def send_email_out( rq=None, message=None ):

	if not message:
		return False

	msg = EmailMultiAlternatives(
		message.subject,
		message.text,
		message.fromuser,
		[message.to],
	)
	msg.attach_alternative( message.html, "text/html" )
	msg.send()

	message.sent = True
	message.save()





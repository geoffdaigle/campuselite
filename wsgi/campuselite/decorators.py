
'''
    Shortcut wrappers to do things like check for authentication
'''

import json, socket
from django import http, shortcuts
from django.core import serializers
from campuselite import settings

def auth_required( function=None, redirect_path='/login', do_verify=True ):
    '''
        redirects if not logged in
    '''
    def _outer( view_func ):
        def _inner( rq, *args, **kwargs ):
            if rq.is_authenticated:
                if do_verify and not rq.is_verified:
                    return shortcuts.redirect( '/verify' )
                return view_func( rq, *args, **kwargs )
            else:
                return shortcuts.redirect( redirect_path+'?redirect='+settings.MAIN_SITE+rq.get_full_path() )

        # to properly wrap w/ params
        _inner.__doc__ = view_func.__doc__ 
        _inner.__dict__ = view_func.__dict__
        _inner.__name__ = view_func.__name__
        return _inner

    if function is None:
        return _outer
    else:
        return _outer( function )

def jsonify(fn, *args, **kwargs):
    """
        Decorator that returns a JSON response.
        Expects a dict to be return by the function/view it's wrapped around
    """
    def wrapper(*args, **kwargs):
        output = fn(*args, **kwargs)
        try:
            value = json.dumps(output)
        except TypeError:
            json_serializer = serializers.get_serializer('json')
            serializer = json_serializer()
            serializer.serialize(output)
            value = serializer.getvalue()
        resp = http.HttpResponse(value)
        resp["Content-type"] = "application/json"
        return resp
    return wrapper
from django.conf.urls import patterns, url, include
from rest_framework import routers
from ce_api import views, urls as api_urls
from ce_app import urls as app_urls
from django.views import static as static_views
from campuselite import settings
from django.conf import settings as dj_settings


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browseable API.
urlpatterns = patterns('',

    url(r'^about', 'campuselite.static_views.about'),
    url(r'^team', 'campuselite.static_views.team'),
    url(r'^contact', 'campuselite.static_views.contact'),
    url(r'^privacy', 'campuselite.static_views.privacy'),
    url(r'^terms', 'campuselite.static_views.terms'),

    url(r'^logout', 'campuselite.portal_views.logout'),
    url(r'^password-reset$', 'campuselite.portal_views.password_reset'),
    url(r'^password-reset/(?P<passkey>[0-9a-zA-Z]+)', 'campuselite.portal_views.password_reset_form'),
    url(r'^login', 'campuselite.portal_views.login'),
    url(r'^join', 'campuselite.portal_views.join'),
    url(r'^verify-account/(?P<passkey>[0-9a-zA-Z]+)/', 'campuselite.portal_views.verify_account'),


    url(r'^createuser$', 'ce_user.views.createuser'),
    url(r'^clearusers', 'ce_user.views.clearusers'),

    url(r'^api/', include(api_urls)),

    url(r'', include(app_urls)),
)

if settings.DEBUG:
    urlpatterns += patterns( '', 
    		url(r'^static/(?P<path>.*)$', 'django.views.static.serve', 
    			{'document_root': settings.STATIC_ROOT}) 
    )
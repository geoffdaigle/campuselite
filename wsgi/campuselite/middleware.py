
from ce_user.models import *
from django.contrib.auth.models import AnonymousUser


class CeAuthenticationMiddleware(object):
    '''
        Overrides the Django user authentication to fetch 
        our own users and put them onto the request
    '''

    def process_request(self, rq):
        assert hasattr(rq, 'session'), "The CE authentication middleware requires session middleware to be installed. Edit your MIDDLEWARE_CLASSES setting to insert 'django.contrib.sessions.middleware.SessionMiddleware'."
        class CeAuthUser(object):
            def __get__(self, rq, obj_type=None):
                if not hasattr(rq, '_cached_user'):
                    try:
                        user_id = rq.session["_auth_user_id"]
                        user = CeUser.objects.get(pk=user_id)
                    except KeyError:
                        user = AnonymousUser()
                    rq._cached_user = user
                return rq._cached_user

        rq.__class__.user = CeAuthUser()



class CampusEliteMiddleware( object ):
    '''
        Standard middleware to set up shortcuts on the 
        django request object
    '''

    def process_request( self, rq ):

        ## --------------------------------
        ## get authentication state
        ## --------------------------------
        rq.is_authenticated = rq.user.is_authenticated()
        if rq.is_authenticated:
            rq.is_verified = rq.user.verified
            rq.notifcount = rq.user.unread_notifs()
        else:
            rq.is_verified = False
       
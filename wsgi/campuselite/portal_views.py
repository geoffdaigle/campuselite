import datetime, json
from dateutil.relativedelta import relativedelta

import django 
from django.contrib.auth.models import AnonymousUser

from django.core import serializers
from django.http import Http404
from django import http, shortcuts

from lib.querystring_parser import parser

from campuselite.shortcuts import *
from campuselite.forms import *

from ce_user.models import *
from ce_user.forms import *
from ce_messaging.helpers import *

'''
	some test accounts
	geoff@campuselite.com / 097410
	mike@campuselite.com / 097410
	test@campuselite.com / okaysure
'''
def login( rq ):

	if rq.is_authenticated:
		return shortcuts.redirect("/")

	if rq.method == 'POST':		
		f = LoginForm( data=rq.POST )

		if f.is_valid():	
			if f.do_login( rq ):
				# success!
				if rq.GET.get('redirect', ''):
					return shortcuts.redirect(rq.GET.get('redirect', ''))

				return shortcuts.redirect("/")
			else:
				# unlikely this one will be called
				return render_template( rq, 'login', {"form": f} )
		else:
			# if invalid form - should be checked with JS
			return render_template( rq, 'login', {"form": f} )

	else:
		# not submitted yet
		f = LoginForm()
		return render_template( rq, 'login', {"form": f} )



def password_reset( rq ):
	if rq.is_authenticated:
		return shortcuts.redirect("/")

	if rq.method == 'POST':		
		f = PassResetForm( data=rq.POST )

		if f.is_valid():	
			f.send_reset_email( rq )
			return render_template( rq, 'password-reset', {"form": f, "success": True} )
		else:
			return render_template( rq, 'password-reset', {"form": f} )
	else:
		f = PassResetForm()
		return render_template( rq, 'password-reset', {"form": f} )



def password_reset_form( rq, passkey ):
	import hashlib

	user = None
	users = CeUser.objects.all()
	for u in users:
		passkeytotest = hashlib.sha224('cesalt'+u.email).hexdigest()[:15]

		if passkey == passkeytotest:
			user = u
			break

	if not user:
		return shortcuts.redirect('/login')

	if rq.method == 'POST':		
		f = PassResetFormPassword( data=rq.POST, user=u )

		if f.is_valid():	
			f.change_password()
			lf = LoginForm()
			lf.do_login( rq, u )
			return render_template( rq, 'password-reset-form', {"form": f, "success": True} )
		else:
			return render_template( rq, 'password-reset-form', {"form": f} )
	else:
		f = PassResetFormPassword( user=u )
		return render_template( rq, 'password-reset-form', {"form": f} )



def join( rq ):
	if rq.is_authenticated:
		return shortcuts.redirect("/")

	if rq.method == 'POST':		
		f = JoinForm( data=rq.POST )

		if f.is_valid():
			new_user = f.do_join( rq )

			# send verification
			lf = LoginForm()
			if lf.do_login( rq, new_user ):
				MessageHelper_SendVerify( rq )

				# success!
				MessageHelper_CreateNotification({
					"user": new_user,
					"text": "<strong>Welcome to Campus Elite!</strong> Click on this notification to edit your profile and add more information about yourself",
					"icon": 6,
					"link": new_user.profile_path(),
					"primary_image": "/static/image/ce-logo-grey.png",
				})
				return shortcuts.redirect("/verify")
			else:
				# unlikely this one will be called
				return render_template( rq, 'join', {"form": f} )
		else:
			# if invalid form - should be checked with JS
			return render_template( rq, 'join', {"form": f} )

	else:
		f = JoinForm()
		return render_template( rq, 'join', {"form": f} )



def verify_account( rq, passkey ):
	import hashlib
	passkeytotest = hashlib.sha224('cesalt'+rq.user.email).hexdigest()[:15]

	if passkey == passkeytotest:
		rq.user.verified = True
		rq.user.save()
		return shortcuts.redirect( '/start' )
	else:
		return render_template( rq, 'nomatchverify' )


def logout(rq):
    rq.session.flush()
    if hasattr(rq, 'user'):
        rq.user = AnonymousUser()
    return shortcuts.redirect("/")


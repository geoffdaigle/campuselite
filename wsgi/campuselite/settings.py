"""
Django settings for campuselite project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

ON_OPENSHIFT = False
if os.environ.has_key('OPENSHIFT_REPO_DIR'):
    ON_OPENSHIFT = True

ON_AMAZON = False
# make sure you dont add this to your own local machine
if BASE_DIR == '/home/campuselite/wsgi':
    ON_AMAZON = True

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '8g4!*iwk)pl0r@v2r@wz5j#c@2ti@)lx$hcx8_tkq%&%n!!nos'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True if not ON_AMAZON else False

TEMPLATE_DEBUG = True if not ON_AMAZON else False

ALLOWED_HOSTS = ['*']
ADMINS = (('geoff', 'geoffreydaigle@gmail.com'))


# Make sure you leave trailing slash!!
if os.environ.has_key('OPENSHIFT_REPO_DIR'):
    BASE_URL = 'http://dev-campuselite.rhcloud.com/'
    MAIN_SITE = 'http://dev-campuselite.rhcloud.com'
    
else:
    if DEBUG:
        BASE_URL = 'http://localhost:1234/'
        MAIN_SITE = 'http://localhost:1234'
    else:
        BASE_URL = 'http://app.thecampuselite.com/'
        MAIN_SITE = 'http://app.thecampuselite.com'


LIBRARY_VERSION = '0.1.1'


# Application definition

INSTALLED_APPS = (
    # system apps
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'rest_framework',
    'south', 

    # campus elite apps
    'ce_api',          # the rest_framework api layer
    'landing',         # static, info pages display layer
    'ce_app',          # the app display layer, loads pages for ember
    'ce_user',         # api
    'ce_diet',         # api
    'ce_workout',      # api
    'ce_post',         # api
    'ce_group',        # api
    'ce_messaging',    # api
    'ce_leaderboard',  # api
)


MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'campuselite.middleware.CeAuthenticationMiddleware',
    'campuselite.middleware.CampusEliteMiddleware',
)

ROOT_URLCONF = 'campuselite.urls'

WSGI_APPLICATION = 'campuselite.wsgi.application'

if os.environ.has_key('OPENSHIFT_REPO_DIR'):

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'STORAGE_ENGINE': 'InnoDB',
            'NAME': 'dev',
            'USER': 'adminUbdswJD',
            'PASSWORD': 'kTKCHGKQR6Ca',
            'HOST': '127.5.225.2',
            'PORT': '3306',
            'OPTIONS': { 'init_command':'SET storage_engine=INNODB,character_set_connection=utf8,collation_connection=utf8_general_ci' },
            'CONN_MAX_AGE':0,
        }
    }

else:
    if DEBUG:

        DATABASES = {
            'default': {
                'ENGINE': 'django.db.backends.mysql',
                'STORAGE_ENGINE': 'InnoDB',
                'NAME': 'campuselite',
                'USER': 'geoff',
                'PASSWORD': '097410',
                'HOST': '',
                'PORT': '',
                'OPTIONS': { 'init_command':'SET storage_engine=INNODB,character_set_connection=utf8,collation_connection=utf8_general_ci' },
                'CONN_MAX_AGE':0,
            }
        }
    else:
        DATABASES = {
            'default': {
                'ENGINE': 'django.db.backends.mysql',
                'STORAGE_ENGINE': 'InnoDB',
                'NAME': 'campuselite',
                'USER': 'ce_admin',
                'PASSWORD': 'ce_db38dg7dbc3',
                'HOST': 'campuselite-main.cdbeyx9hkn0r.us-east-1.rds.amazonaws.com',
                'PORT': '3306',
                'OPTIONS': { 'init_command':'SET storage_engine=INNODB,character_set_connection=utf8,collation_connection=utf8_general_ci' },
                'CONN_MAX_AGE':0,
            }
        }


TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)


# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'America/New_York'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# email settings
EMAIL_PORT = 587
EMAIL_USE_TLS = True
if DEBUG:
    EMAIL_HOST = 'smtp.gmail.com'
    EMAIL_HOST_USER = 'geoffreydaigle@gmail.com'
    EMAIL_HOST_PASSWORD = '097410a123'
else:
    EMAIL_HOST = 'email-smtp.us-east-1.amazonaws.com'
    EMAIL_HOST_USER = 'AKIAJIBWBSTTE7QP53GQ'
    EMAIL_HOST_PASSWORD = 'ApT5yJ22Czb0bthoagxF3rLObmKheAYwdVkpyF3VU8Gz'




# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)


BUCKET_NAME = 'campuselite'
AWS_ACCESS_KEY_ID = 'AKIAINVXYSP7M25EIMXQ'
AWS_SECRET_ACCESS_KEY = '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'

if not DEBUG:
    INSTALLED_APPS += ('storages',)
    STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
    AWS_STORAGE_BUCKET_NAME = 'campuselite'
    STATIC_URL = 'http://%s.s3.amazonaws.com/' % AWS_STORAGE_BUCKET_NAME


if DEBUG:
    FACEBOOK_ID = '455601777916055'
    FACEBOOK_SECRET = '678469afbc2fda9c892fc5bc68298d9c'
else:
    FACEBOOK_ID = '455600604582839'
    FACEBOOK_SECRET = '6a5dc075258a2ec3e98af79689e1e4f0'


# DJANGO REST API FRAMEWORK
REST_FRAMEWORK = {
    'PAGINATE_BY': 10,

    'TEST_REQUEST_DEFAULT_FORMAT': 'json',

    'DEFAULT_RENDERER_CLASSES': (
        'ce_api.parser_renderer.JSONRenderer',
        #'rest_framework.renderers.BrowsableAPIRenderer',
    ),

    'DEFAULT_PARSER_CLASSES': (
        'ce_api.parser_renderer.JSONParser',
        #'rest_framework.renderers.BrowsableAPIRenderer',
    ),

    'DEFAULT_MODEL_SERIALIZER_CLASS':
        'rest_framework.serializers.HyperlinkedModelSerializer',

    'DEFAULT_AUTHENTICATION_CLASSES': [
        'ce_api.authentication.CeAuthentication',
    ],

    'DEFAULT_PERMISSION_CLASSES': [
        #'rest_framework.permissions.IsAuthenticatedOrReadOnly',
        'rest_framework.permissions.AllowAny',
    ]
}

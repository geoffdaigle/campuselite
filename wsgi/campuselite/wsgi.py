import os
import sys
from django.core.handlers.wsgi import WSGIHandler

# add mosaic to python path
sys.path.append('/home/campuselite/wsgi')

os.environ['DJANGO_SETTINGS_MODULE'] = 'campuselite.settings'
os.environ['PYTHON_EGG_CACHE'] = '/home/apache/egg_cache'
application = WSGIHandler()
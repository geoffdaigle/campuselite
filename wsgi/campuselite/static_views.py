import datetime, json
from dateutil.relativedelta import relativedelta

import django 

from django.core import serializers
from django.http import Http404
from django import http, shortcuts

from lib.querystring_parser import parser

from campuselite.shortcuts import *


def about( rq ):
	return render_template( rq, 'about' )


def contact( rq ):
	return render_template( rq, 'contact' )


def team( rq ):
	return render_template( rq, 'team' )


def privacy( rq ):
	return render_template( rq, 'privacy' )


def terms( rq ):
	return render_template( rq, 'terms' )
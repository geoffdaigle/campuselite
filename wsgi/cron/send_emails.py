'''

	actually sends emails in a cron job

'''


import os, sys, time

os.chdir("/home/campuselite/wsgi")
#os.chdir("../")
print os.getcwd()
sys.path.append(os.getcwd())
os.environ['DJANGO_SETTINGS_MODULE'] = 'campuselite.settings'

from django.core import serializers
from django.http import Http404
from django import http, shortcuts
from django.core.paginator import Paginator, EmptyPage

from ce_messaging.models import *

from campuselite.shortcuts import *
from campuselite.forms import *
from campuselite import decorators



def main():
	emails = EmailObject.objects.filter(sent=False)

	for e in emails:
		print "sending email --> to "+e.to
		send_email_out(message=e)
		time.sleep(1)


if __name__ == "__main__":
    main()
'''

	Sends emails to users who have been invited to events

'''


import os, sys

os.chdir("/home/campuselite/wsgi")
#os.chdir("../")
print os.getcwd()
sys.path.append(os.getcwd())
os.environ['DJANGO_SETTINGS_MODULE'] = 'campuselite.settings'

from django.core import serializers
from django.http import Http404
from django import http, shortcuts
from django.core.paginator import Paginator, EmptyPage

from lib.querystring_parser import parser

from ce_user.fixtures import *

from campuselite.shortcuts import *
from campuselite.forms import *
from campuselite import decorators

from ce_group.models import *
from ce_group.forms import *
from ce_group.helpers import *
import datetime, time
from ce_messaging.helpers import *

def main():
	invites = CeGroupInvite.objects.filter(datetime_sent__isnull=True)

	for i in invites:

		try:
		    u = CeUser.objects.get( email=i.email )
		except CeUser.DoesNotExist:
		    continue

		# if they shut this off... skip it
		if not u.notification_settings.essential_alerts:
			continue

		m = '<p>Hi there!</p>'
		m += "<p><strong>"+i.group.creator.full_name()+"</strong> has invited you to their event <strong>"+i.group.name+"</strong>.</p><br>"
		m += "<p><a href='"+i.group.details_path_absolute()+"'>Go to event &rarr;</a></p><br>"
		m += '''<p>Thanks, and happy workout!</p>
		<p>Campus Elite</p>
		'''

		t = compile_email_template({ "body_content": m })
		send_user_email( message={
		    "subject": 'You have been invited to attend '+i.group.name,
		    "message": t,
		    "to": ['highwaytoinfinity@gmail.com' if settings.DEBUG else i.email],
		    "from": 'Campus Elite <alerts@thecampuselite.com>',
		} )

		i.datetime_sent = datetime.datetime.now()
		i.save()


		MessageHelper_CreateNotification({
	        "user": u,
	        "text": "<strong>"+i.group.creator.full_name()+"</strong> has invited you to their event <strong>"+i.group.name+"</strong>",
	        "icon": 4,
	        "link": i.group.details_path(),
	        "primary_image": i.group.creator.profile.avatar_thumb_path,
    	})

		time.sleep(1)


if __name__ == "__main__":
    main()
/*
 *  Campus Elite Gulp Configuration
 *  This builds our client-side files and runs tasks
 *  Geoff Daigle - @dailydaigle
 *
 */


// System Dependancies
var path = require('path');

// Dependancies
var gulp = require('gulp');
var uglify = require('gulp-uglify');    // minify javascript
var changed = require('gulp-changed');  // detect only changed files
var concat = require('gulp-concat');    // concatenate files
var less = require('gulp-less');        // compile less stylesheets
var jasmine = require('gulp-jasmine');  // javascript test runner
var through2 = require('through2');     // custom task pipestream wrapper
var plumber = require('gulp-plumber');     // plumbaaaa


// Relative paths to our processed files
var paths = {
	styles: [ './static/less/**/*.less' ],
	emberapp: [ './static/app-dev/**/*.js' ],
	scripts: ['./static/js-dev/**/*.js'],
	tests: ['./static/tests/jasmine-spec/**/*.js'],
	images: [],
	ignorePaths: [ 'less/includes/', 'less/components/', 'js-dev/includes/' ]
};


// Just a filter to ignore certain paths so
// our compilers dont create new folders for things.
// e.g. includes and components
var ignorePath = function () {
	function check (file, enc, next) {
		var passed = true;
		for (var i = 0; i < paths.ignorePaths.length; i++) {
			if ( file.path.indexOf(paths.ignorePaths[i]) !== -1 ){
				passed = false;
			}
		}
		if ( passed ) {
			this.push(file);
		}
		return next();
	}

	return through2.obj(check);
};


/* run jasmine tests */
gulp.task('js-test', function() {
	return gulp.src(paths.tests)
		.pipe( jasmine() );
});


/* minify less css */
gulp.task('less', function() {
	var DEST = './static/css';

	var l = less({
		lint: true,
		verbose: true,
		cleancss: true,
		showStack: true,
		paths: [ path.join(__dirname, 'static', 'less', 'includes') ]
	});
	l.on('error',function(e){
		console.log(e);
		l.end();
	});

	return gulp.src(paths.styles)
		.pipe(changed( DEST ))
		.pipe( ignorePath() )
		.pipe( l )
		.pipe( gulp.dest( DEST ) );
});


/* uglify js */
gulp.task('uglify', function() {
	var DEST = './static/js';

	return gulp.src(paths.scripts)
		.pipe( plumber() )
		.pipe(changed( DEST ))
		.pipe( ignorePath() )
		.pipe( uglify({ preserveComments: 'some' }) )
		.pipe( gulp.dest( DEST ) );
});



/* watch for changes */
gulp.task('watch', function() {
  gulp.watch(paths.styles, ['less']);
  gulp.watch(paths.scripts, ['js-test', 'uglify']);
  gulp.watch(paths.tests, ['js-test']);
});



/* run the tasks */
gulp.task('default', ['watch', 'js-test', 'less', 'uglify']);




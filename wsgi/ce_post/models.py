from django.db import models as m
from ce_user.models import CeUserLike
from ce_workout.models import *
from ce_group.models import *


CE_POST_TYPE_PHOTO = 1
CE_POST_TYPE_VIDEO = 2
CE_POST_TYPE_STATUS = 3
CE_POST_TYPE_LINK = 4
CE_POST_TYPE_BEST = 5
CE_POST_TYPE_WORKOUT = 6
CE_POST_TYPE_DIET = 7
CE_POST_TYPE_EVENT_CREATED = 8
CE_POST_TYPE_EVENT_COMPLETED = 9
CE_POST_TYPE_COMMENT = 10
CE_POST_TYPE_CHOICES = (
	(CE_POST_TYPE_PHOTO, 'Photo'),
	(CE_POST_TYPE_VIDEO, 'Video'),
	(CE_POST_TYPE_STATUS, 'Status'),
	(CE_POST_TYPE_BEST, 'Personal Best'),
	(CE_POST_TYPE_WORKOUT, 'Workout Update'),
	(CE_POST_TYPE_DIET, 'Diet Update'),
	(CE_POST_TYPE_EVENT_CREATED, 'Event Created'),
    (CE_POST_TYPE_EVENT_COMPLETED, 'Event Completed'),
	(CE_POST_TYPE_COMMENT, 'Comment'),
)



# A user post (status, link, photo(set), video, comment, profile updates, event updates)
class CePost( m.Model ):
    creator = m.ForeignKey('ce_user.CeUser', related_name="posts")
    post_type = m.PositiveSmallIntegerField(choices=CE_POST_TYPE_CHOICES)
    post_id = m.IntegerField() # there CAN be duplicates, as long as they have different post types
    datetime_created = m.DateTimeField(auto_now_add=True, db_index=True)
    datetime_updated = m.DateTimeField(auto_now=True)
    edited = m.BooleanField(default=False)
    deleted = m.BooleanField(default=False)


    def get_public_path( self ):
        return self.creator.profile_path_short()+"/posts/"+str(self.pk)

    def get_public_path_absolute( self ):
        return self.creator.profile_path_short_absolute()+"/posts/"+str(self.pk)


    def get_likes(self, serialize=False):
        if serialize:
            from ce_api import serializers as s

        likes = CeUserLike.objects.filter(post_id=self.pk).order_by('-datetime_created')

        if serialize:
            return s.UserLikeSerializer( likes ).data
        else:
            return likes

    def is_liked(self, user):
        try:
            p = CeUserLike.objects.get(post_id=self.pk, user_id=user.pk)
            return True
        except CeUserLike.DoesNotExist:
            return False


    def get_comments(self, serialize=False):
        if serialize:
            from ce_api import serializers as s

        comments = CeComment.objects.filter(post_id=self.pk, post_meta__deleted=False).order_by('-datetime_created').select_related('post_meta')

        if serialize:
            return s.PostCommentSerializer( comments ).data
        else:
            return comments


    ####
    ####  Depending on the post type, this gets the post details 
    ####  Primarily used for dynamic serialization
    ####
    def get_data(self, serialize=False):

        # for custom serialization
        if serialize:
            from ce_api import serializers as s

        def get_photo( post_id ):
            if serialize:
                return s.PhotoPostSerializer( CePhoto.objects.get(pk=post_id) ).data
            else:
                return CePhoto.objects.get(pk=post_id)

        def get_video( post_id ):
            if serialize:
                return s.VideoPostSerializer( CeVideo.objects.get(pk=post_id) ).data
            else:
                return CeVideo.objects.get(pk=post_id)

        def get_status( post_id ):
            if serialize:
                return s.StatusPostSerializer( CeStatus.objects.get(pk=post_id) ).data
            else:
                return CeStatus.objects.get(pk=post_id)

        def get_link( post_id ):
            if serialize:
                return s.LinkPostSerializer( CeLink.objects.get(pk=post_id) ).data
            else:
                return CeLink.objects.get(pk=post_id)


        def get_workout_log( post_id ):
            if serialize:
                return None # TODO
            else:
                return CeWorkoutLog.objects.get(pk=post_id)


        def get_event( post_id ):
            if serialize:
                return None # TODO
            else:
                return CeGroup.objects.get(pk=post_id)


        def get_comment( post_id ):
            if serialize:
                return s.PostCommentReverseSerializer( CeComment.objects.get(pk=post_id) ).data
            else:
                return CeComment.objects.get(pk=post_id)

        fetchers = {
            1 : get_photo,
            2 : get_video,
            3 : get_status,
            4 : get_link,
            #5 : prime,
            6 : get_workout_log,
            #7 : prime,
            8 : get_event,
            #9 : prime,
            10 : get_comment,
        }

        return fetchers[ self.post_type ]( self.post_id )

        



# A generic text status
class CeStatus( m.Model ):
    user = m.ForeignKey('ce_user.CeUser')
    caption = m.TextField(blank=True, max_length=10000)
    datetime_created = m.DateTimeField(auto_now_add=True)







def generate_image_link_path(instance, filename):
    return u"uploads/post/link/%s.jpg" % instance.link_image_hash

# A text status with a link 
class CeLink( m.Model ):
    user = m.ForeignKey('ce_user.CeUser')
    caption = m.TextField(blank=True, max_length=10000)
    link_href = m.TextField(blank=True, max_length=1000)
    link_title = m.CharField(max_length=255)
    link_source = m.CharField(max_length=255)
    link_description = m.CharField(max_length=255)
    link_image = m.ImageField(blank=True, upload_to=generate_image_link_path)
    link_image_hash = m.CharField(max_length=255)
    datetime_created = m.DateTimeField(auto_now_add=True)







def generate_image_photo_path(instance, filename):
    return u"uploads/post/photo/%s.jpg" % instance.photo_file_hash

def generate_image_photo_thumb_path(instance, filename):
    return u"uploads/post/photo/%s_thumb.jpg" % instance.photo_file_hash


# This is a photo or an album
class CePhoto( m.Model ):
    user = m.ForeignKey('ce_user.CeUser', related_name="photos")
    caption = m.TextField(blank=True, max_length=10000)
    photo_count = m.IntegerField(default=0) # makes it easy to determine layout
    datetime_created = m.DateTimeField(auto_now_add=True)


# This is where the actual photo ids are stores. Single photo in set is displayed differently
class CePhotoSet( m.Model ):
    photo_post = m.ForeignKey('ce_post.CePhoto', related_name="photoset")
    photo_file = m.ImageField(blank=True, upload_to=generate_image_photo_path)
    photo_file_thumb = m.ImageField(blank=True, upload_to=generate_image_photo_thumb_path)
    photo_file_hash = m.CharField(max_length=255)
    height = m.IntegerField()
    width = m.IntegerField()
    datetime_created = m.DateTimeField(auto_now_add=True)






# simply a data object with external video platform links
class CeVideo( m.Model ):
    user = m.ForeignKey('ce_user.CeUser')
    caption = m.TextField(blank=True, max_length=10000)
    video_id = m.CharField(max_length=255) # stripped part of the url 
    video_host = m.CharField(max_length=255) # youtube, vimeo, instagram, vine, etc -- determines embed code
    datetime_created = m.DateTimeField(auto_now_add=True)





# Comments are a post type so we can use this in overall activity displays
# Also a post type so that users can "like" comments without this system changing.
class CeComment( m.Model ):
    user = m.ForeignKey('ce_user.CeUser', related_name="comments")
    post = m.ForeignKey(CePost, related_name="comments")
    post_meta = m.ForeignKey(CePost) # the post object this belongs to
    comment = m.TextField(blank=True, max_length=10000)
    datetime_created = m.DateTimeField(auto_now_add=True)


    def is_edited( self ):
        return self.post_meta.edited












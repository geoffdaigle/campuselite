# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CePost'
        db.create_table(u'ce_post_cepost', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('creator', self.gf('django.db.models.fields.related.ForeignKey')(related_name='posts', to=orm['ce_user.CeUser'])),
            ('post_type', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('post_id', self.gf('django.db.models.fields.IntegerField')()),
            ('datetime_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('datetime_updated', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('edited', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('deleted', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'ce_post', ['CePost'])

        # Adding model 'CeStatus'
        db.create_table(u'ce_post_cestatus', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ce_user.CeUser'])),
            ('caption', self.gf('django.db.models.fields.TextField')(max_length=10000, blank=True)),
            ('datetime_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'ce_post', ['CeStatus'])

        # Adding model 'CeLink'
        db.create_table(u'ce_post_celink', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ce_user.CeUser'])),
            ('caption', self.gf('django.db.models.fields.TextField')(max_length=10000, blank=True)),
            ('link_href', self.gf('django.db.models.fields.TextField')(max_length=1000, blank=True)),
            ('link_title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('link_source', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('link_description', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('link_image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('link_image_hash', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('datetime_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'ce_post', ['CeLink'])

        # Adding model 'CePhoto'
        db.create_table(u'ce_post_cephoto', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='photos', to=orm['ce_user.CeUser'])),
            ('caption', self.gf('django.db.models.fields.TextField')(max_length=10000, blank=True)),
            ('photo_count', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('datetime_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'ce_post', ['CePhoto'])

        # Adding model 'CePhotoSet'
        db.create_table(u'ce_post_cephotoset', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('photo_post', self.gf('django.db.models.fields.related.ForeignKey')(related_name='photoset', to=orm['ce_post.CePhoto'])),
            ('photo_file', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('photo_file_thumb', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('photo_file_hash', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('height', self.gf('django.db.models.fields.IntegerField')()),
            ('width', self.gf('django.db.models.fields.IntegerField')()),
            ('datetime_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'ce_post', ['CePhotoSet'])

        # Adding model 'CeVideo'
        db.create_table(u'ce_post_cevideo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ce_user.CeUser'])),
            ('caption', self.gf('django.db.models.fields.TextField')(max_length=10000, blank=True)),
            ('video_id', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('video_host', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('datetime_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'ce_post', ['CeVideo'])

        # Adding model 'CeComment'
        db.create_table(u'ce_post_cecomment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='comments', to=orm['ce_user.CeUser'])),
            ('post', self.gf('django.db.models.fields.related.ForeignKey')(related_name='comments', to=orm['ce_post.CePost'])),
            ('post_meta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ce_post.CePost'])),
            ('comment', self.gf('django.db.models.fields.TextField')(max_length=10000, blank=True)),
            ('datetime_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'ce_post', ['CeComment'])


    def backwards(self, orm):
        # Deleting model 'CePost'
        db.delete_table(u'ce_post_cepost')

        # Deleting model 'CeStatus'
        db.delete_table(u'ce_post_cestatus')

        # Deleting model 'CeLink'
        db.delete_table(u'ce_post_celink')

        # Deleting model 'CePhoto'
        db.delete_table(u'ce_post_cephoto')

        # Deleting model 'CePhotoSet'
        db.delete_table(u'ce_post_cephotoset')

        # Deleting model 'CeVideo'
        db.delete_table(u'ce_post_cevideo')

        # Deleting model 'CeComment'
        db.delete_table(u'ce_post_cecomment')


    models = {
        u'ce_post.cecomment': {
            'Meta': {'object_name': 'CeComment'},
            'comment': ('django.db.models.fields.TextField', [], {'max_length': '10000', 'blank': 'True'}),
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'post': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'comments'", 'to': u"orm['ce_post.CePost']"}),
            'post_meta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ce_post.CePost']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'comments'", 'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_post.celink': {
            'Meta': {'object_name': 'CeLink'},
            'caption': ('django.db.models.fields.TextField', [], {'max_length': '10000', 'blank': 'True'}),
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link_description': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'link_href': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'blank': 'True'}),
            'link_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'link_image_hash': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'link_source': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'link_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_post.cephoto': {
            'Meta': {'object_name': 'CePhoto'},
            'caption': ('django.db.models.fields.TextField', [], {'max_length': '10000', 'blank': 'True'}),
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'photo_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'photos'", 'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_post.cephotoset': {
            'Meta': {'object_name': 'CePhotoSet'},
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'height': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'photo_file': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'photo_file_hash': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'photo_file_thumb': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'photo_post': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'photoset'", 'to': u"orm['ce_post.CePhoto']"}),
            'width': ('django.db.models.fields.IntegerField', [], {})
        },
        u'ce_post.cepost': {
            'Meta': {'object_name': 'CePost'},
            'creator': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'posts'", 'to': u"orm['ce_user.CeUser']"}),
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'datetime_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'edited': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'post_id': ('django.db.models.fields.IntegerField', [], {}),
            'post_type': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'ce_post.cestatus': {
            'Meta': {'object_name': 'CeStatus'},
            'caption': ('django.db.models.fields.TextField', [], {'max_length': '10000', 'blank': 'True'}),
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_post.cevideo': {
            'Meta': {'object_name': 'CeVideo'},
            'caption': ('django.db.models.fields.TextField', [], {'max_length': '10000', 'blank': 'True'}),
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ce_user.CeUser']"}),
            'video_host': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'video_id': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'ce_user.ceuser': {
            'Meta': {'object_name': 'CeUser'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'followers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'following'", 'symmetrical': 'False', 'through': u"orm['ce_user.CeUserFollow']", 'to': u"orm['ce_user.CeUser']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'verified': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'ce_user.ceuserfollow': {
            'Meta': {'ordering': "['-datetime_created']", 'object_name': 'CeUserFollow'},
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'follower_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'follower_user'", 'to': u"orm['ce_user.CeUser']"}),
            'following_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'following_user'", 'to': u"orm['ce_user.CeUser']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['ce_post']
# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding index on 'CePost', fields ['datetime_created']
        db.create_index(u'ce_post_cepost', ['datetime_created'])


    def backwards(self, orm):
        # Removing index on 'CePost', fields ['datetime_created']
        db.delete_index(u'ce_post_cepost', ['datetime_created'])


    models = {
        u'ce_post.cecomment': {
            'Meta': {'object_name': 'CeComment'},
            'comment': ('django.db.models.fields.TextField', [], {'max_length': '10000', 'blank': 'True'}),
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'post': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'comments'", 'to': u"orm['ce_post.CePost']"}),
            'post_meta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ce_post.CePost']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'comments'", 'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_post.celink': {
            'Meta': {'object_name': 'CeLink'},
            'caption': ('django.db.models.fields.TextField', [], {'max_length': '10000', 'blank': 'True'}),
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link_description': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'link_href': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'blank': 'True'}),
            'link_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'link_image_hash': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'link_source': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'link_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_post.cephoto': {
            'Meta': {'object_name': 'CePhoto'},
            'caption': ('django.db.models.fields.TextField', [], {'max_length': '10000', 'blank': 'True'}),
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'photo_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'photos'", 'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_post.cephotoset': {
            'Meta': {'object_name': 'CePhotoSet'},
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'height': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'photo_file': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'photo_file_hash': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'photo_file_thumb': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'photo_post': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'photoset'", 'to': u"orm['ce_post.CePhoto']"}),
            'width': ('django.db.models.fields.IntegerField', [], {})
        },
        u'ce_post.cepost': {
            'Meta': {'object_name': 'CePost'},
            'creator': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'posts'", 'to': u"orm['ce_user.CeUser']"}),
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'}),
            'datetime_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'edited': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'post_id': ('django.db.models.fields.IntegerField', [], {}),
            'post_type': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'ce_post.cestatus': {
            'Meta': {'object_name': 'CeStatus'},
            'caption': ('django.db.models.fields.TextField', [], {'max_length': '10000', 'blank': 'True'}),
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_post.cevideo': {
            'Meta': {'object_name': 'CeVideo'},
            'caption': ('django.db.models.fields.TextField', [], {'max_length': '10000', 'blank': 'True'}),
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ce_user.CeUser']"}),
            'video_host': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'video_id': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'ce_user.ceuser': {
            'Meta': {'object_name': 'CeUser'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'followers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'following'", 'symmetrical': 'False', 'through': u"orm['ce_user.CeUserFollow']", 'to': u"orm['ce_user.CeUser']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'verified': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'ce_user.ceuserfollow': {
            'Meta': {'ordering': "['-datetime_created']", 'object_name': 'CeUserFollow'},
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'follower_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'follower_user'", 'to': u"orm['ce_user.CeUser']"}),
            'following_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'following_user'", 'to': u"orm['ce_user.CeUser']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['ce_post']
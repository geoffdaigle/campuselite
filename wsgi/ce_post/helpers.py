from ce_post.models import *


'''
	Takes any object list and looks for "user" property... adds that to a new list
'''
def PostHelper_getUserList( items ):
	ulist = []
	for u in items:
		try:
			ulist.append( u.user )
		except:
			pass

	return ulist


'''
	Takes a list of posts and formats each one into the proper data structure for 
	displaying to the client.
'''
def PostHelper_FormatData( posts, rq ):
	posts_list = []
	for p in posts:

		post_data = {}
		post_instance = p.get_data()

		if p.post_type == CE_POST_TYPE_PHOTO:
			post_data['photoset'] = post_instance.photoset.all()[:6]
			post_data['odd_display'] = False
			if post_instance.photo_count > 6:
			 	post_instance.photo_count = 6

			if post_instance.photo_count % 2 == 1 and len(post_data['photoset']) != 6:
				post_data['odd_display'] = True


		post_data.update({
			"has_likes": p.postlikes.count() > 0,
			"is_liked": False,
			"has_comments": p.comments.count() > 0,
			"likes": p.postlikes.all(),
			"comments": p.comments.all(),
			"meta": p,
			"data": post_instance,
			# Django templates require a that I pass all this into one variable
			# in order to get multiple arguments into a filter function.
			# ... it's kind of a pain in the ass, honestly.
			"user_display_package": ( PostHelper_getUserList( p.postlikes.all() ), rq.user, 'gave a fist bump', 'gave a fist bump', p.creator.profile_path()+'/posts/'+str(p.pk)+'/bumped' ),
		})

		if rq.is_authenticated:
			post_data.update({
				"is_liked": p.is_liked( rq.user )
			})

		posts_list.append( post_data )

	return posts_list
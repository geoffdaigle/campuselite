from django import forms as f
from ce_post.models import *
from ce_messaging.helpers import *


# creates a status post
class PostStatusForm(f.Form):
    caption = f.CharField(max_length=10000, required=False)

    def save_data( self, user ):
        cd = self.cleaned_data

        p = CePost()
        p.creator = user
        p.post_type = CE_POST_TYPE_STATUS
        p.post_id = 0 #temp
        p.save()

        s = CeStatus()
        s.user = user
        s.caption = cd['caption']
        s.save()

        p.post_id = s.pk
        p.save()

        return p


class PostPhotosForm(f.Form):
    caption = f.CharField(max_length=10000, required=False)

    def save_data( self, user, images, location="media", thumb_ext='-thumb' ):
        cd = self.cleaned_data
        
        p = CePost()
        p.creator = user
        p.post_type = CE_POST_TYPE_PHOTO
        p.post_id = 0 #temp
        p.save()

        s = CePhoto()
        s.user = user
        s.caption = cd['caption']
        s.photo_count = len(images)
        s.save()

        p.post_id = s.pk
        p.save()

        for i in images:
            im = CePhotoSet()
            im.photo_post_id = s.pk
            # NOTE: THIS COULD CHANGE... 
            #       I DON'T HAVE TIME TO FUTURE-PROOF THE LOCATION 
            staticurl = settings.STATIC_URL
            im.photo_file = staticurl+location+'/'+i+'.jpg'
            im.photo_file_thumb = staticurl+location+'/'+i+thumb_ext+'.jpg'
            im.photo_file_hash = '12345' # in case we need this for something...
            im.height = 0 # hmm....
            im.width = 0  # i dont really need these
            im.save()

        return p

    def save_data_album( self, user, images, location="media", thumb_ext='-thumb' ):
        cd = self.cleaned_data
        
        s = CePhoto()
        s.user = user
        s.caption = cd['caption']
        s.photo_count = len(images)
        s.save()

        for i in images:
            im = CePhotoSet()
            im.photo_post_id = s.pk
            # NOTE: THIS COULD CHANGE... 
            #       I DON'T HAVE TIME TO FUTURE-PROOF THE LOCATION 
            staticurl = settings.STATIC_URL
            im.photo_file = staticurl+location+'/'+i+'.jpg'
            im.photo_file_thumb = staticurl+location+'/'+i+thumb_ext+'.jpg'
            im.photo_file_hash = '12345' # in case we need this for something...
            im.height = 0 # hmm....
            im.width = 0  # i dont really need these
            im.save()

        return s


# creates a video post
class PostVideoForm(f.Form):
    caption = f.CharField(max_length=10000, required=False)
    video_id = f.CharField(max_length=100, required=True)
    video_host = f.CharField(max_length=20, required=True)

    def save_data( self, user ):
        cd = self.cleaned_data
        
        p = CePost()
        p.creator = user
        p.post_type = CE_POST_TYPE_VIDEO
        p.post_id = 0 #temp
        p.save()

        s = CeVideo()
        s.user = user
        s.caption = cd['caption']
        s.video_host = cd['video_host']
        s.video_id = cd['video_id']
        s.save()

        p.post_id = s.pk
        p.save()

        return p



# sanitizes comments
class CommentForm(f.Form):
    post_id = f.IntegerField( min_value=1, required=False )
    comment_id = f.IntegerField( min_value=1, required=False )
    comment = f.CharField(max_length=10000, required=False)
    remove = f.BooleanField( required=False )

    def save_data( self, user ):

        # Comments are a CePost and a CeComment
        # - post_meta belongs to this comment
        # - post is a reference to what was commented on


        cd = self.cleaned_data

        if cd['comment_id'] and not cd['remove']:
            try:
                c = CeComment.objects.get(pk=cd['comment_id'])
                c.comment = cd['comment']
                postobj = c.post
                postobj.edited = True
                c.save()
                postobj.save()
            except CeComment.DoesNotExist:
                return { "status": "error" }

        if cd['remove']:
            try:
                c = CeComment.objects.get(pk=cd['comment_id'])  
                postobj = c.post
                postobj.deleted = True
                postobj.save()
            except CeComment.DoesNotExist:
                pass
        else:
            p = CePost()
            p.creator = user
            p.post_type = CE_POST_TYPE_COMMENT
            p.post_id = 0 #temp
            p.save()

            c = CeComment()
            c.user = user
            c.comment = cd['comment']
            c.post_id = cd['post_id']
            c.post_meta = p
            c.save()

            # three writes happen for one comment, i wonder if this can be made smaller
            p.post_id = c.pk
            p.save()

            UserCommentNotification( cd['post_id'], user, cd['comment'] )


        return { "status": "success", 'id': c.pk }
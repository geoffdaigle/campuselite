from django import forms as f
from ce_workout.models import *
from ce_post.models import *

'''
    Data validation forms related to this app
'''

class WorkoutFollowForm(f.Form):
    user_id = f.IntegerField(min_value=1)
    workout_id = f.IntegerField(min_value=1)


class WorkoutForm(f.Form):
    name = f.CharField()
    info_id = f.IntegerField(min_value=0)
    weight_label = f.IntegerField(min_value=0)
    score_label = f.IntegerField(min_value=0)
    distance_label = f.IntegerField(min_value=0)

    def save_data( self, user ):
        cd = self.cleaned_data

        w = CeWorkout()
        w.user_id = user.pk
        w.name = cd['name']
        w.info_id = cd['info_id']
        w.weight_label = cd['weight_label']
        w.score_label = cd['score_label']
        w.distance_label = cd['distance_label']
        w.save()
        return w


    def update_data( self, workout_id ):
        cd = self.cleaned_data

        w = CeWorkout.objects.get(pk=workout_id)
        w.name = cd['name']
        w.info_id = cd['info_id']
        w.weight_label = cd['weight_label']
        w.score_label = cd['score_label']
        w.distance_label = cd['distance_label']
        w.save()

        return w



class WorkoutLogForm(f.Form):
    reps = f.IntegerField(min_value=0)
    weight = f.IntegerField(min_value=0)
    weight_label = f.IntegerField(min_value=0)
    time = f.CharField()
    score = f.IntegerField()
    score_label = f.IntegerField(min_value=0)
    distance = f.IntegerField(min_value=0)
    distance_label = f.IntegerField(min_value=0)

    def save_data( self, workout_id ):
        cd = self.cleaned_data

        wl = CeWorkoutLog()
        wl.workout_id = workout_id
        wl.reps = cd['reps']
        wl.weight = cd['weight']
        wl.weight_label = cd['weight_label']
        wl.time = cd['time']
        wl.score = cd['score']
        wl.score_label = cd['score_label']
        wl.distance = cd['distance']
        wl.distance_label = cd['distance_label']
        wl.save()

        # check for personal best
        is_pb = False
        if cd['weight']:
            wli = CeWorkoutLog.objects.filter(workout_id=workout_id).order_by('-weight')[:1][0]
            is_pb = ( wli.pk == wl.pk )

        if cd['reps'] and not is_pb:
            wli = CeWorkoutLog.objects.filter(workout_id=workout_id).order_by('-reps')[:1][0]
            is_pb = ( wli.pk == wl.pk )

        if cd['time'] != '0' and not is_pb:
            wli = CeWorkoutLog.objects.filter(workout_id=workout_id).order_by('-time')[:1][0]
            is_pb = ( wli.pk == wl.pk )

        if cd['score'] and not is_pb:
            wli = CeWorkoutLog.objects.filter(workout_id=workout_id).order_by('-score')[:1][0]
            is_pb = ( wli.pk == wl.pk )

        if cd['distance'] and not is_pb:
            wli = CeWorkoutLog.objects.filter(workout_id=workout_id).order_by('-distance')[:1][0]
            is_pb = ( wli.pk == wl.pk )

        if is_pb:
            wl.personal_best = True
            wl.save()

        return wl

    # logs a workout and creates a new post about it at the same time
    def save_post_data( self, user, workout_id ):
        cd = self.cleaned_data

        p = CePost()
        p.creator = user
        p.post_type = CE_POST_TYPE_WORKOUT
        p.post_id = 0 #temp
        p.save()

        w = self.save_data( workout_id=workout_id )

        p.post_id = w.pk
        p.save()

        return p

    def update_data( self, log_id ):
        cd = self.cleaned_data

        wl = CeWorkoutLog.objects.get(pk=log_id)
        wl.reps = cd['reps']
        wl.weight = cd['weight']
        wl.weight_label = cd['weight_label']
        wl.time = cd['time']
        wl.score = cd['score']
        wl.score_label = cd['score_label']
        wl.distance = cd['distance']
        wl.distance_label = cd['distance_label']
        wl.save()

        return w


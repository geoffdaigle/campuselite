# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CeWorkoutType'
        db.create_table(u'ce_workout_ceworkouttype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('reps', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('weight', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('time', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('score', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('distance', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'ce_workout', ['CeWorkoutType'])

        # Adding model 'CeWorkout'
        db.create_table(u'ce_workout_ceworkout', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='workouts', to=orm['ce_user.CeUser'])),
            ('info', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ce_workout.CeWorkoutType'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('weight_label', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('score_label', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('distance_label', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('datetime_updated', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'ce_workout', ['CeWorkout'])

        # Adding model 'CeWorkoutLog'
        db.create_table(u'ce_workout_ceworkoutlog', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('workout', self.gf('django.db.models.fields.related.ForeignKey')(related_name='logentries', to=orm['ce_workout.CeWorkout'])),
            ('reps', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('weight', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('weight_label', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('time', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('score', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('score_label', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('distance', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('distance_label', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('datetime_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('personal_best', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'ce_workout', ['CeWorkoutLog'])

        # Adding model 'CeWorkoutFollow'
        db.create_table(u'ce_workout_ceworkoutfollow', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='followed_workouts', to=orm['ce_user.CeUser'])),
            ('workout', self.gf('django.db.models.fields.related.ForeignKey')(related_name='followers', to=orm['ce_workout.CeWorkout'])),
            ('datetime_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'ce_workout', ['CeWorkoutFollow'])


    def backwards(self, orm):
        # Deleting model 'CeWorkoutType'
        db.delete_table(u'ce_workout_ceworkouttype')

        # Deleting model 'CeWorkout'
        db.delete_table(u'ce_workout_ceworkout')

        # Deleting model 'CeWorkoutLog'
        db.delete_table(u'ce_workout_ceworkoutlog')

        # Deleting model 'CeWorkoutFollow'
        db.delete_table(u'ce_workout_ceworkoutfollow')


    models = {
        u'ce_user.ceuser': {
            'Meta': {'object_name': 'CeUser'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'followers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'following'", 'symmetrical': 'False', 'through': u"orm['ce_user.CeUserFollow']", 'to': u"orm['ce_user.CeUser']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'verified': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'ce_user.ceuserfollow': {
            'Meta': {'ordering': "['-datetime_created']", 'object_name': 'CeUserFollow'},
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'follower_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'follower_user'", 'to': u"orm['ce_user.CeUser']"}),
            'following_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'following_user'", 'to': u"orm['ce_user.CeUser']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'ce_workout.ceworkout': {
            'Meta': {'object_name': 'CeWorkout'},
            'datetime_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'distance_label': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ce_workout.CeWorkoutType']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'score_label': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'workouts'", 'to': u"orm['ce_user.CeUser']"}),
            'weight_label': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'})
        },
        u'ce_workout.ceworkoutfollow': {
            'Meta': {'ordering': "['-datetime_created']", 'object_name': 'CeWorkoutFollow'},
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'followed_workouts'", 'to': u"orm['ce_user.CeUser']"}),
            'workout': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'followers'", 'to': u"orm['ce_workout.CeWorkout']"})
        },
        u'ce_workout.ceworkoutlog': {
            'Meta': {'ordering': "['-datetime_created']", 'object_name': 'CeWorkoutLog'},
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'distance': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'distance_label': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'personal_best': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'reps': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'score': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'score_label': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'time': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'weight': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'weight_label': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'workout': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'logentries'", 'to': u"orm['ce_workout.CeWorkout']"})
        },
        u'ce_workout.ceworkouttype': {
            'Meta': {'object_name': 'CeWorkoutType'},
            'distance': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'reps': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'score': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'time': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'weight': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['ce_workout']
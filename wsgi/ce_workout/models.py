from django.db import models as m
from ce_user.models import CeUser


# Pre-defined workouts types (These are all Lifting)
# This could go into a database but there really isnt any reason to do it.

CE_WORKOUT_LIFTS_CHOICES = (
    # chest
    (1, 'Chest - Bench Press'),
    (2, 'Chest - Incline Barbell Press'),
    (3, 'Chest - Decline Barball Press'),
    (4, 'Chest - Dumbell Press'),
    (5, 'Chest - Dumbell Incline Press'),
    (6, 'Chest - Dumbell Decline Press'),
    (7, 'Chest - Dumbell Flyes'),
    (8, 'Chest - Dumbell Incline Flyes'),
    (9, 'Chest - Dumbell Decline Flyes'),
    (10, 'Chest - Machine Flat Press'),
    (11, 'Chest - Machine Incline Press'),
    (12, 'Chest - Pushups'),
    (13, 'Chest - Pullovers'),
    (14, 'Chest - Cable Crossovers'),
    (15, 'Chest - Pec Decks'),

    # back
    (16, 'Back - Deadlifts'),
    (17, 'Back - Seated Rows'),
    (18, 'Back - Barbell Rows'),
    (19, 'Back - Dumbell Rows'),
    (20, 'Back - Machine Rows'),
    (21, 'Back - Wide Grip Pulldowns'),
    (22, 'Back - Close Grip Pulldowns'),
    (23, 'Back - Underhand Pulldowns'),
    (24, 'Back - Rack Pulls'),
    (25, 'Back - Pullups'),
    (26, 'Back - Chins'),
    (27, 'Back - T-Bar Rows'),
    (28, 'Back - Hyperextentions'),
    (29, 'Back - Straight Arm Pullovers'),
    (30, 'Back - Dumbell Pullovers'),

    # delts
    (31, 'Delts - Lateral Raises'),
    (32, 'Delts - Front Raises'),
    (33, 'Delts - Bent-over Raises'),
    (34, 'Delts - Barbell Press'),
    (35, 'Delts - Dumbell Press'),
    (36, 'Delts - Smith Machine Press'),
    (37, 'Delts - Machine Overhead Press'),
    (38, 'Delts - Clean and Jerk'),
    (39, 'Delts - Shrugs'),
    (40, 'Delts - Upright Rows'),
    (41, 'Delts - Reverse Peck Decks'),

    # Legs
    (42, 'Legs - Squats'),
    (43, 'Legs - Leg Press'),
    (44, 'Legs - Hack Squats'),
    (45, 'Legs - Lunges'),
    (46, 'Legs - Leg Extensions'),
    (47, 'Legs - Seated Leg Curls'),
    (48, 'Legs - Standing Leg Curls'),
    (49, 'Legs - Front Squats'),
    (50, 'Legs - Lying Leg Curls'),
    (51, 'Legs - Stiff Legged Deadlifts'),
    (52, 'Legs - Seated Calf Raises'),
    (53, 'Legs - Standing Calf Raises'),
    (54, 'Legs - Glute-Ham Raises'),
    (55, 'Legs - Good Morning'),
    (56, 'Legs - Romanian Deadlifts'),
    (57, 'Legs - Sumo Deadlifts'),
    (58, 'Legs - Thigh Abductor'),

    # Arms
    (59, 'Arms - Barbell Curls'),
    (60, 'Arms - Concentration Curls'),
    (61, 'Arms - Dumbell Curls'),
    (62, 'Arms - Incline Dumbell Curls'),
    (63, 'Arms - Cable Curls'),
    (64, 'Arms - Hammer Curls'),
    (65, 'Arms - Reverse Curls'),
    (66, 'Arms - Preacher Curls'),
    (67, 'Arms - Skullcrushers'),
    (68, 'Arms - Dips'),
    (69, 'Arms - Pushdowns'),
    (70, 'Arms - Overhead Extentions'),
    (71, 'Arms - Close-Grip Bench Press'),
    (72, 'Arms - Palms-Down Wrist Curls'),
    (73, 'Arms - Palms-Up Wrist Curls'),

    (74, 'Abs - Sit-ups'),
    (75, 'Abs - Crunches'),
    (76, 'Abs - Reverse Crunches'),
    (77, 'Abs - Machine Crunches'),
    (78, 'Abs - Air Bicycles'),
    # (79, 'Abs - Planks'),  # <-- not reps or weight?
    

    (80, 'Cardio - Running'),
    (81, 'Cardio - Walking'),
    (82, 'Cardio - Biking'),
)




# Defines which fields should be used when logging, and allows us to create images and 
# videos about certain kinds of workouts

'''
    Existing so far:
    - Sport - Points, Goals
    - Lifting - Weight and Reps
    - Cardio - Time and Distance
'''
class CeWorkoutType( m.Model ):
    name = m.CharField(max_length=255)
    reps = m.BooleanField(default=False) 
    weight = m.BooleanField(default=False)
    time = m.BooleanField(default=False) 
    score = m.BooleanField(default=False) 
    distance = m.BooleanField(default=False)



CE_WORKOUT_SCORE_LABEL_GOALS = 1
CE_WORKOUT_SCORE_LABEL_POINTS = 2
CE_WORKOUT_SCORE_LABEL_CHOICES = (
    (CE_WORKOUT_SCORE_LABEL_GOALS, 'Goals'),
    (CE_WORKOUT_SCORE_LABEL_POINTS, 'Points'),
)

CE_WORKOUT_DIST_LABEL_MILES = 1
CE_WORKOUT_DIST_LABEL_YARDS = 2
CE_WORKOUT_DIST_LABEL_FEET = 3
CE_WORKOUT_DIST_LABEL_KILOMETERS = 4
CE_WORKOUT_DIST_LABEL_METERS = 5
CE_WORKOUT_DIST_LABEL_CHOICES = (
    (CE_WORKOUT_DIST_LABEL_MILES, 'mi'),
    (CE_WORKOUT_DIST_LABEL_YARDS, 'yd'),
    (CE_WORKOUT_DIST_LABEL_FEET, 'ft'),
    (CE_WORKOUT_DIST_LABEL_KILOMETERS, 'km'),
    (CE_WORKOUT_DIST_LABEL_METERS, 'm'),
)


CE_WORKOUT_WEIGHT_LABEL_LB = 1
CE_WORKOUT_WEIGHT_LABEL_KG = 2
CE_WORKOUT_WEIGHT_LABEL_CHOICES = (
    (CE_WORKOUT_WEIGHT_LABEL_LB, 'lbs'),
    (CE_WORKOUT_WEIGHT_LABEL_KG, 'kg'),
)


# The user's workout instance
class CeWorkout( m.Model ):
    user = m.ForeignKey('ce_user.CeUser', related_name="workouts")
    info = m.ForeignKey('ce_workout.CeWorkoutType')
    name = m.CharField(max_length=255)
    weight_label = m.IntegerField(default=0, blank=True, choices=CE_WORKOUT_WEIGHT_LABEL_CHOICES)
    score_label = m.IntegerField(default=0, blank=True, choices=CE_WORKOUT_SCORE_LABEL_CHOICES)
    distance_label = m.IntegerField(default=0, blank=True, choices=CE_WORKOUT_DIST_LABEL_CHOICES)
    datetime_updated = m.DateTimeField(auto_now_add=True)

    def get_workout_type(self):
        return self.info.name

    def get_latest_log(self, serialize=False):
        try:
            d = list(self.logentries.all())
            d = d[0]
            if serialize:
                from ce_api import serializers as s
                return s.WorkoutLogSerializer( d ).data
            else:
                return d
        except IndexError:
            return None

    def get_best_log(self, serialize=False):
        try:
            d = list(self.logentries.order_by('-personal_best', '-datetime_created'))
            d = d[0]
            if serialize:
                from ce_api import serializers as s
                return s.WorkoutLogSerializer( d ).data
            else:
                return d
        except IndexError:
            return None



# A log item where the user can 
class CeWorkoutLog( m.Model ):
    workout = m.ForeignKey('ce_workout.CeWorkout', related_name="logentries")
    reps = m.IntegerField(default=0) 
    weight = m.IntegerField(default=0) 
    weight_label = m.IntegerField(default=0, blank=True, choices=CE_WORKOUT_WEIGHT_LABEL_CHOICES) # "lbs", "kg"
    time = m.CharField(max_length=255)
    score = m.IntegerField(default=0)
    score_label = m.IntegerField(default=0, blank=True, choices=CE_WORKOUT_SCORE_LABEL_CHOICES) # "Goals", "Points", etc
    distance = m.IntegerField(default=0) 
    distance_label = m.IntegerField(default=0, blank=True, choices=CE_WORKOUT_DIST_LABEL_CHOICES) # Miles, Yards, Feet, Kilometers, Meters
    datetime_created = m.DateTimeField(auto_now_add=True)
    personal_best = m.BooleanField(default=False)

    class Meta:
        ordering = ['-datetime_created']



# Allows users to follow a workout
class CeWorkoutFollow( m.Model ):
    user = m.ForeignKey('ce_user.CeUser', related_name="followed_workouts")
    workout = m.ForeignKey('ce_workout.CeWorkout', related_name="followers")
    datetime_created = m.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-datetime_created']







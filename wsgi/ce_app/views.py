import datetime, json
from dateutil.relativedelta import relativedelta
from pytz import timezone

from django.core import serializers
from django.http import Http404
from django import http, shortcuts
from django.core.paginator import Paginator, EmptyPage
from django.db.models import Count

from lib.querystring_parser import parser

from ce_user.fixtures import *

from campuselite.shortcuts import *
from campuselite.forms import *
from campuselite import decorators
from ce_user.models import *
from ce_user.forms import *
from ce_user.helpers import *
from ce_workout.models import *
from ce_workout.forms import *
from ce_diet.models import *
from ce_diet.forms import *
from ce_diet.helpers import *
from ce_group.models import *
from ce_group.forms import *
from ce_group.helpers import *
from ce_post.models import *
from ce_post.forms import *
from ce_post.helpers import *
from ce_messaging.models import *
from ce_messaging.helpers import *



'''
    The user's dashboard that they see when logged in
    Shows a feed user posts
    Allows user to update their feed with a post component

    DO NOT USE @decorators.auth_required OR THERE WILL BE A REDIRECT LOOP
'''
def dashboard( rq ):

    # regular user dash or school feed
    if rq.is_authenticated:

        if not rq.is_verified:
            return shortcuts.redirect( '/verify' )

        today = datetime.datetime.now()
        today_day = datetime.datetime(today.year, today.month, today.day, 0, 0, 0)
        tomorrow = today + relativedelta(days=1)
        dayafter = today + relativedelta(days=2)

        localtz = timezone( settings.TIME_ZONE )
        today = localtz.localize(today)
        tomorrow = localtz.localize(tomorrow)
        today_day = localtz.localize(today_day)
        dayafter = localtz.localize(dayafter)

        # get a list of users they are following 
        fol = rq.user.following_newest()
        fol_ids = [ rq.user.pk ]
        for f in fol:
            fol_ids.append( f.follower_user_id )

        context = {
            # show whole school's feed and not the following-only one
            'school_feed': rq.GET.get('all', None),

            # for the post widget
            'workouts': rq.user.workouts.all().select_related('info').order_by('name'),
        }

        nav = 'school' if context['school_feed'] is not None else 'home'

        # upcoming events
        groups = CeGroupInstance.objects.filter(cancelled=False, event_datetime__gte=today)[:10]

        groups_list = []
        for g in groups:
            g.user_display_package = ([], rq.user, 'is attending', 'are attending')
            groups_list.append( g )

        context['events'] = groups_list;

        context['show_current_group'] = False;
        
        if groups:
            today_group = groups[0]
            if today_group.event_datetime < dayafter:
                context['show_current_group'] = True;

        # todo: ajax
        context["suggested_events"] = GroupHelper_SuggestedGroups( rq.user )
        
        # todo: ajax
        context["suggested_friends"] = UserHelper_SuggestedFriends( rq.user )

        if rq.GET.get('ob', None):
            context['onboarding_success'] = True 

        return render_template( rq, 'app/dashboard', context, nav=nav )
    
    # show the home page and login stuff
    else:
        return shortcuts.redirect('/login')




'''
    Ajax handler for feed
'''
@decorators.auth_required
def fetch_feed_page( rq ):
    fol = rq.user.following_newest()
    fol_ids = [ rq.user.pk ]
    for f in fol:
        fol_ids.append( f.follower_user_id )

    school_feed = rq.GET.get('all', None)
    user_only = rq.GET.get('user', None)
    page = int(rq.GET.get('page', '1'))

    if user_only is not None:
        posts = CePost.objects.filter(deleted=False, creator_id=int(user_only)).exclude(post_type=10).order_by('-datetime_created')
        if not posts:
            return http.HttpResponse( '' )
    else:
        posts = CePost.objects.filter(deleted=False, creator__verified=True, creator__deleted=False).exclude(post_type=10).order_by('-datetime_created')
        if school_feed:
            posts = posts.exclude(creator_id__in=fol_ids)
        else:
            posts = posts.filter(creator_id__in=fol_ids)

    try:
        posts = Paginator(posts, 6)
        posts = posts.page( page )

        context = {
            "next": posts.has_next(),
            "posts": PostHelper_FormatData( posts.object_list, rq )
        }

        context['school_feed'] = bool(school_feed)

        if len(fol_ids) == 1: # includes yourself 
            context["no_friends"] = True
            context["suggested_friends"] = UserHelper_SuggestedFriends( rq.user, limit=10, add_extra=True )
        else:
            if not posts and not school_feed:
                context["suggested_friends"] = UserHelper_SuggestedFriends( rq.user, limit=10, add_extra=True )
            context["no_friends"] = False

        if school_feed:
            context["no_friends"] = False

        return render_template( rq, 'app/ajax/post-list', context )
    except EmptyPage:
        return http.HttpResponse( '' )



'''
    A hard-link to a public post so that it can be shared elsewhere
'''
def user_public_post( rq, username, post_id ):
    context = {}

    try:
        user = CeUser.objects.get( username=username, deleted=False, verified=True )
        context["user"] = user
    except CeUser.DoesNotExist:
        raise Http404

    try:
        post = CePost.objects.get( pk=post_id )
    except CePost.DoesNotExist:
        raise Http404

    context['post'] = PostHelper_FormatData( [post], rq )[0]


    if rq.is_authenticated:
        if not rq.is_verified:
            return shortcuts.redirect( '/verify' )

        # todo: ajax
        context["suggested_events"] = GroupHelper_SuggestedGroups( rq.user )
        
        # todo: ajax
        context["suggested_friends"] = UserHelper_SuggestedFriends( rq.user )

        if context['post']['meta'].post_type == 1:
            context['post_img'] = settings.MAIN_SITE + str(context['post']['photoset'][0].photo_file)
        else:
            context['post_img'] = settings.MAIN_SITE + '/static/image/ce-logo-large.jpg'

        return render_template( rq, 'app/public-post', context, nav='home' )
    else:
        # supresses comment and bump actions
        context['advert_mode'] = True
        return render_template( rq, 'app/public-post-anon', context )



'''
    Shows who has fist-bumped a post
'''
@decorators.auth_required
def user_post_bumped( rq, username, post_id ):
    context = {}

    try:
        user = CeUser.objects.get( username=username, deleted=False )
        context["user"] = user
    except CeUser.DoesNotExist:
        raise Http404

    try:
        post = CePost.objects.get( pk=post_id )
    except CePost.DoesNotExist:
        raise Http404

    context['post'] = PostHelper_FormatData( [post], rq )[0]

    bump_list = []
    bs = context['post']['user_display_package'][0]

    for b in bs:
        if rq.user.pk != b.pk:
            b.is_followed = rq.user.is_following( b )
        bump_list.append( b )

    context['bumpers'] = bump_list

    context["suggested_events"] = GroupHelper_SuggestedGroups( rq.user )


    return render_template( rq, 'app/user-post-bumped', context )




'''
    A place where a user can edit their settings
'''
@decorators.auth_required
def user_settings( rq ):
    
    if rq.method == 'POST':
        first_name = rq.POST.get('first_name', '').strip()
        last_name = rq.POST.get('last_name', '').strip()
        username = rq.POST.get('username', '').strip()
        email = rq.POST.get('email', '').strip()

        essential_alerts = True if rq.POST.get('essential_alerts', None) is not None else False
        interaction_alerts = True if rq.POST.get('interaction_alerts', None) is not None else False
        dev_updates_news = True if rq.POST.get('dev_updates_news', None) is not None else False
        marketing_promos = True if rq.POST.get('marketing_promos', None) is not None else False

        print rq.POST
        if first_name:
            rq.user.first_name = first_name
        if last_name:
            rq.user.last_name = last_name

        if email and email != rq.user.email:
            try:
                u = CeUser.objects.get(email=email, deleted=False)
                email = False
            except CeUser.DoesNotExist:
                rq.user.email = email

        if username and username != rq.user.username:
            try:
                u = CeUser.objects.get(username=username, deleted=False)
                username = False
            except CeUser.DoesNotExist:
                rq.user.username = username

        if first_name or last_name or email or username:
            rq.user.save()


        rq.user.notification_settings.essential_alerts = essential_alerts
        rq.user.notification_settings.interaction_alerts = interaction_alerts
        rq.user.notification_settings.dev_updates_news = dev_updates_news
        rq.user.notification_settings.marketing_promos = marketing_promos
        rq.user.notification_settings.save()


        rq.session['success_message'] = "Your account settings have been updated."
        return shortcuts.redirect( rq.user.profile_path() )
    
    import hashlib
    passwordpasskey = hashlib.sha224('cesalt'+rq.user.email).hexdigest()[:15]

    return render_template( rq, 'app/user-settings', {'passkey': passwordpasskey} )



'''
    Ajax call for getting the number of unviewed notifications
'''
@decorators.auth_required
@decorators.jsonify
def get_notifications_number( rq ):
    ncount = rq.user.notifications.filter(shown=False).count()
    return {"notifications": ncount}


'''
    Sets the "shown" param to notifications
'''
@decorators.auth_required
@decorators.jsonify
def notifs_shown( rq ):

    if rq.method == 'POST':
        ids = rq.POST.getlist('n[]')
        notifs = rq.user.notifications.filter(pk__in=ids)
        for n in notifs:
            n.shown = True
            n.save()


    ncount = rq.user.notifications.filter(shown=False).count()
    return {"left": ncount}


'''
    Looks for new notifications and pushes them to the client
'''
@decorators.auth_required
def check_new_notifications( rq ):
    gtid = int(rq.GET.get('gt', '0') if rq.GET.get('gt', '0') != 'null' else '0')
    notifs = rq.user.notifications.filter(shown=False, pk__gt=gtid)

    if notifs:
        context = {
            "next": True,
            "notifs": notifs
        }
    else:
        context = { "no_new": True }

    return render_template( rq, 'includes/component-notification-instance', context )

'''
    Ajax call for fetching notifications in the header popup
'''
@decorators.auth_required
def get_notifications( rq ):
    page = int(rq.GET.get('page', '1'))
    prefetch = rq.GET.get('prefetch', False)
    notifs = list(rq.user.notifications.all())

    notifs = Paginator(notifs, 10)
    notifs = notifs.page( page )

    context = {
        "next": notifs.has_next(),
        "notifs": notifs.object_list
    }

    if prefetch == False:
        for n in notifs.object_list:
            n.shown = True
            n.save()

    return render_template( rq, 'includes/component-notification-instance', context )





'''
    A list of the user's notifications in chronological order
    This is a PAGE and not an ajax handler
'''
@decorators.auth_required
def user_notifications( rq ):
    page = int(rq.GET.get('page', '1'))
    notifs = list(rq.user.notifications.all())

    notifs = Paginator(notifs, 15)
    notifs = notifs.page( page )

    context = {
        "prev_num": notifs.previous_page_number() if notifs.has_previous() else 0,
        "prev": notifs.has_previous(),
        "next_num": notifs.next_page_number() if notifs.has_next() else 0,
        "next": notifs.has_next(),
        "notifs": notifs.object_list
    }

    return render_template( rq, 'app/user-notifications', context )








'''
    A user page - both current user and publically viewable user
    Lots of moving parts.

    Includes:
        - profile
        - diet
        - workouts
            - workout log
        - following/followers
        - posts
        - likes
        - workouts following
        - diets following
        - photos (just a single gallery)
        - Events completed
        - Events created
        - Trophies for being on leaderboard
        - Personal message (anything)
        - Personal message with personal event scheduled
'''
@decorators.auth_required
def user_view( rq, username ):
    context = {}

    try:
        user = CeUser.objects.get( username=username, deleted=False )
        context["user"] = user

        today = datetime.datetime.now()


        if rq.user.pk != user.pk:
            user.is_followed = rq.user.is_following( user )
            user.diet_followed = rq.user.follows_diet( user )

        # get top 2 groups they have upcoming
        context["upcoming_events"] = CeGroupInstance.objects.filter( group__creator=user, cancelled=False, group__deleted=False, event_datetime__gt=today ).order_by('event_datetime')[:2]
        context["past_events"] = CeGroupInstance.objects.filter( group__creator=user, completed=True, cancelled=False, group__deleted=False, event_datetime__lt=today ).order_by('-event_datetime')[:2]

        context['diet_follower_package'] = DietHelper_FollowerPackage( user, rq.user )

        # diet is fetched using related field on user object

        # get list of workouts (not logs - show only best)
        context["workouts"] = user.workouts.annotate(wcount=Count('logentries')).order_by('-wcount')[:3]

        # get user photos 
        # TODO: fetch by ajax
        context['photos'] = CePhotoSet.objects.filter(photo_post__user=user).order_by('-datetime_created')[:4]
        context['photocount'] = CePhotoSet.objects.filter(photo_post__user=user).count()

        if user.profile.avatar_medium != '':
            user.avatar_photo_post = False
            try:
                user.avatar_photo_post = CePhotoSet.objects.get(photo_file_thumb__contains=user.profile.avatar_medium)
            except CePhotoSet.DoesNotExist:
                pass

        # get user posts 
        # TODO: fetch by ajax
        posts = CePost.objects.filter(deleted=False, creator=user).exclude(post_type=10).order_by('-datetime_created')[:20]
        context['posts'] = PostHelper_FormatData( posts, rq )

    except KeyError:
        raise Http404
    except CeUser.DoesNotExist:
        raise Http404

    fols = len(rq.user.followers_newest())
    if fols < 3:
        context['buddy_up_text'] = "Psst... you can create a private event with this user."
    
    return render_template( rq, 'app/user-view', context )



@decorators.auth_required
def user_photos( rq, username ):
    context = {}

    try:
        user = CeUser.objects.get( username=username, deleted=False )
        context["user"] = user
    except CeUser.DoesNotExist:
        raise Http404

    if rq.user.pk != user.pk:
            user.is_followed = rq.user.is_following( user )

    # todo: ajax
    context["suggested_events"] = GroupHelper_SuggestedGroups( rq.user )
    
    # todo: ajax
    context["suggested_friends"] = UserHelper_SuggestedFriends( rq.user )

    context['photos'] = CePhotoSet.objects.filter(photo_post__user=user).order_by('-datetime_created')
    context['photocount'] = CePhotoSet.objects.filter(photo_post__user=user).count()

    return render_template( rq, 'app/user-photos', context )



'''
    Deletes a single photo if a user owns it.
    This also removes the photo from a photoset post.
    If a photoset post has no photos after this action, the photoset post gets deleted.
'''
@decorators.auth_required
def user_delete_photo( rq, photo_id ):
    if rq.method != 'POST':
        raise Http404

    from django.views.defaults import server_error

    try:
        photo = CePhotoSet.objects.get(pk=photo_id)
    except CePhotoSet.DoesNotExist:
        return server_error(rq)

    photo_post = photo.photo_post
    photo_post.photo_count = photo_post.photo_count - 1

    if photo_post.user != rq.user:
        return server_error(rq)

    photo.delete()

    if photo_post.photo_count == 0:
        photopost_id = photo_post.pk
        photo_post.delete()
        try:
            postobj = CePost.objects.get(deleted=False, post_type=CE_POST_TYPE_PHOTO, post_id=photopost_id)
            postobj.deleted = True
            postobj.save()
        except CePost.DoesNotExist:
            pass
    else:
        photo_post.save()

    data = json.dumps( {"status": "deleted"} )
    return http.HttpResponse( data, content_type="application/json" )




'''
    When viewing a photoset, this fetches a complete photoset and sends it to the browser
    so a user can browse all of the photos without having to leave the photo corral
'''
@decorators.auth_required
def load_user_photos( rq, username ):
    context = {}
    try:
        user = CeUser.objects.get( username=username, deleted=False )
    except CeUser.DoesNotExist:
        raise Http404
    photos = CePhotoSet.objects.filter(photo_post__user=user).order_by('-datetime_created')
    p_list = []
    for photo in photos:
        p_list.append({
            "id": photo.pk,
            "photo": str(photo.photo_file),
            "photo_thumb": str(photo.photo_file_thumb),
            "height": photo.height,
            "width": photo.width,
        })

    context[ user.username ] = p_list

    data = json.dumps( context )
    return http.HttpResponse( data, content_type="application/json" )


@decorators.auth_required
def user_follows( rq, username ):

    follow_type = rq.GET.get('t', 'following');
    page = int(rq.GET.get('page', '1'))

    context = {"follow_type": follow_type}

    try:
        user = CeUser.objects.get( username=username, deleted=False )
        context["user"] = user
    except CeUser.DoesNotExist:
        raise Http404

    follow_list = []

    if follow_type == 'following':
        # for    this user ----> people   use "following"
        follows = user.following.all()
    else:
        # for    this user <------ people   use "followers"
        follows = user.followers.all()

    
    context["suggested_events"] = GroupHelper_SuggestedGroups( rq.user )

    follows = Paginator(follows, 15)
    follows = follows.page( page )

    follow_list = []
    for f in follows.object_list:
        f.is_followed = rq.user.is_following( f )
        follow_list.append( f )

    context.update({
        "prev_num": follows.previous_page_number() if follows.has_previous() else 0,
        "prev": follows.has_previous(),
        "next_num": follows.next_page_number() if follows.has_next() else 0,
        "next": follows.has_next(),
        "follows": follow_list
    })

    return render_template( rq, 'app/user-follows', context )



'''
   a list of workouts
'''
@decorators.auth_required
def user_workouts( rq, username ):
    try:
        user = CeUser.objects.get( username=username, deleted=False )
    except CeUser.DoesNotExist:
        raise Http404

    if rq.user.pk == user.pk:
        return shortcuts.redirect('/workouts')


    user.is_followed = rq.user.is_following( user )

    context = {
        'user': user, 
        'workouts': user.workouts.all().select_related('info').order_by('name'),
    }

    context["suggested_events"] = GroupHelper_SuggestedGroups( rq.user )

    return render_template( rq, 'app/user-workouts', context )



'''
    an in-depth list of a user's single workout
'''
@decorators.auth_required
def user_workout_detail( rq, username, wid ):
    context = {}

    page = int(rq.GET.get('page', '1'))

    context["suggested_events"] = GroupHelper_SuggestedGroups( rq.user )

    try:
        user = CeUser.objects.get( username=username, deleted=False )
        context["user"] = user
    except CeUser.DoesNotExist:
        raise Http404

    try:
        workout = CeWorkout.objects.get( pk=wid )
        context['workout'] = workout
    except CeWorkout.DoesNotExist:
        raise Http404


    le = workout.logentries.all()

    le = Paginator(le, 20)
    le = le.page( page )

    context.update({
        "prev_num": le.previous_page_number() if le.has_previous() else 0,
        "prev": le.has_previous(),
        "next_num": le.next_page_number() if le.has_next() else 0,
        "next": le.has_next(),
        "logentries": le.object_list
    })


    return render_template( rq, 'app/user-workout-detail', context )




'''
    The page where a user can edit their profile.
    Lots of moving parts on this page in order to 
    edit information.

'''
@decorators.auth_required
def user_edit( rq, username ):
    
    context = {}

    try:
        user = CeUser.objects.get( username=username, deleted=False )
        context["user"] = user
    except CeUser.DoesNotExist:
        raise Http404

    if user.pk != rq.user.pk:
        raise Http404

    if rq.method == 'POST':
        f = UserProfileForm( rq.POST, instance=user.profile )
        if f.is_valid():
            # deal with the regular data
            f.save()

            # now save the interest list
            # clear old list
            rq.user.interests.all().delete()

            # write new list
            c = rq.POST.copy();
            for k, v in c.items():
                if 'interest_' in k:
                    interest = CeUserInterest()
                    interest.user = rq.user
                    interest.interest_type = v
                    interest.save()

            rq.session['success_message'] = "Your profile has been updated."
            return shortcuts.redirect( rq.user.profile_path() )
    else:
        f = UserProfileForm( instance=user.profile )

    context['form'] = f

    from ce_user.fixtures import CE_MAJOR_CHOICES

    context['autocomplete_majors'] = CE_MAJOR_CHOICES
    context['interest_choices'] = CE_GROUP_TYPE_CHOICES

    return render_template( rq, 'app/user-edit', context )




'''
    User can see all of their workouts and edit them
'''
@decorators.auth_required
def user_edit_workouts( rq ):

    context = {}

    context['workouts'] = CeWorkout.objects.filter(user_id=rq.user.pk).order_by('-datetime_updated')

    return render_template( rq, 'app/user-edit-workouts', context, nav='workouts' )




'''
    Create a new workout that can be logged
'''
@decorators.auth_required
def user_new_workout( rq ):

    context = {
        'autocomplete_workouts': CE_WORKOUT_LIFTS_CHOICES,
        'score_labels': CE_WORKOUT_SCORE_LABEL_CHOICES,
        'weight_labels': CE_WORKOUT_WEIGHT_LABEL_CHOICES,
        'distance_labels': CE_WORKOUT_DIST_LABEL_CHOICES,
    }

    if rq.method == 'POST':
        f = WorkoutForm( rq.POST )
        context['form'] = f
        if f.is_valid():
            new_workout = f.save_data( rq.user )
            rq.session['success_message'] = "New workout \""+new_workout.name+"\" created succesfully."
            return shortcuts.redirect( '/workouts' )
    else:
        context['form'] = WorkoutForm()

    return render_template( rq, 'app/user-new-workout', context, nav='workouts' )



'''
    Delete a single workout.
    Can't edit because that would potentially mess up logging... plus it doesnt make much sense
'''
@decorators.auth_required
def user_delete_workout( rq, wid ):

    context = {}

    try:
        workout = CeWorkout.objects.get( pk=wid, user=rq.user )
    except CeWorkout.DoesNotExist:
        raise Http404

    context['workout'] = workout

    if rq.method == 'POST' and rq.POST.get('d', None) is not None:
        workout.delete()
        rq.session['success_message'] = 'Workout "'+workout.name+'" has been deleted.'
        return shortcuts.redirect( '/workouts' )
    else:
        return render_template( rq, 'app/user-workout-delete', context, nav='workouts' )





'''
    User can see all the diets that they follow here
'''
@decorators.auth_required
@decorators.jsonify
def follow_diet( rq ):
    if rq.method == 'POST':
        do_follow = rq.POST.get('follow', None)
        user_to_follow = rq.POST.get('user', None)
        user_exists = True

        try:
            u = CeUser.objects.get( pk=int(user_to_follow) )
        except CeUser.DoesNotExist:
            user_exists = False
            

        if do_follow is not None and user_exists:
            if bool(int(do_follow)):
                d = CeDietFollow()
                d.diet_user = u
                d.user = rq.user
                d.save()

                MessageHelper_CreateNotification({
                    "user": u,
                    "text": "<strong>"+rq.user.full_name()+"</strong> started following your diet.",
                    "icon": 3,
                    "link": u.profile_path()+'/diet/followers',
                    "primary_image": rq.user.profile.avatar_thumb_path,    
                })

                # if they shut this off... skip it
                if u.notification_settings.interaction_alerts:
                    subject = rq.user.first_name+" started following your diet"
                    m = "<p>Howdy,</p>"
                    m += "<p><strong>"+rq.user.full_name()+"</strong> started following your diet on Campus Elite.</p>"
                    m += "<p><a href='"+rq.user.profile_path_absolute()+"'>Check out their profile &rarr;</a></p><br>"
                    m += '''<p>Keep on keepin' on,</p>
                    <p>Campus Elite</p>
                    '''

                    t = compile_email_template({ "body_content": m })
                    send_user_email( message={
                        "subject": subject,
                        "message": t,
                        "to": ['highwaytoinfinity@gmail.com' if settings.DEBUG else u.email],
                        "from": 'Campus Elite <alerts@thecampuselite.com>',
                    } )
            else:
                try:
                    CeDietFollow.objects.get(diet_user=u, user=rq.user).delete()
                except CeDietFollow.DoesNotExist:
                    pass


    return {"status":"okay"}




'''
    User can see who is following their diet here
'''
@decorators.auth_required
def user_diet_followers( rq, username ):
    context = {}

    page = int(rq.GET.get('page', '1'))
   
    try:
        user = CeUser.objects.get( username=username, deleted=False )
        context["user"] = user
    except CeUser.DoesNotExist:
        raise Http404

    follow_list = []

    follows = CeDietFollow.objects.filter(diet_user=user)

    context["suggested_events"] = GroupHelper_SuggestedGroups( rq.user )

    follows = Paginator(follows, 15)
    follows = follows.page( page )

    follow_list = []
    for f in follows.object_list:
        u = f.user
        u.is_followed = rq.user.is_following( u )
        follow_list.append( u )

    context.update({
        "prev_num": follows.previous_page_number() if follows.has_previous() else 0,
        "prev": follows.has_previous(),
        "next_num": follows.next_page_number() if follows.has_next() else 0,
        "next": follows.has_next(),
        "follows": follow_list
    })

    return render_template( rq, 'app/user-diet-followers', context )





'''
    User can see all the diets that they follow here
'''
@decorators.auth_required
def user_diets_followed( rq ):

    page = int(rq.GET.get('page', '1'))

    context = {}
    context["suggested_events"] = GroupHelper_SuggestedGroups( rq.user )
    diets = rq.user.followed_diets.all()

    diets = Paginator(diets, 10)
    diets = diets.page( page )

    diet_list = []
    for d in diets.object_list:
        d.diet_follower_package = DietHelper_FollowerPackage( d.diet_user, rq.user )
        diet_list.append( d )

    context.update({
        "prev_num": diets.previous_page_number() if diets.has_previous() else 0,
        "prev": diets.has_previous(),
        "next_num": diets.next_page_number() if diets.has_next() else 0,
        "next": diets.has_next(),
        "diets": diet_list
    })

    return render_template( rq, 'app/user-diets-followed', context )




'''
    User can edit their diet here
'''
@decorators.auth_required
def user_edit_diet( rq ):
    context = {}

    if rq.method == 'POST':
        f = DietForm( rq.POST )
        if f.is_valid():
            f.save_changes( rq.user )
            rq.session['success_message'] = "Your diet has been updated."
            return shortcuts.redirect( rq.user.profile_path() )
    else:
        user_diet = rq.user.get_diet()
        f = DietForm( instance=user_diet )

    context['form'] = f
    
    return render_template( rq, 'app/user-edit-diet', context )



'''
    Shows events that you joined or are pending
'''
@decorators.auth_required
def user_events_joined( rq ):
    page = int(rq.GET.get('page', '1'))
    today = datetime.datetime.now()

    groups = CeGroupAttendee.objects.filter(user=rq.user, group__event_datetime__gt=today).select_related('group');
    group_list = []
    for g in groups:
        g.group.attend_status = g.group.is_attending( rq.user )
        group_list.append( g.group )

    group_list = Paginator(group_list, 10)
    group_list = group_list.page( page )


    context = {
        "prev_num": group_list.previous_page_number() if group_list.has_previous() else 0,
        "prev": group_list.has_previous(),
        "next_num": group_list.next_page_number() if group_list.has_next() else 0,
        "next": group_list.has_next(),
        "events": group_list.object_list
    }

    return render_template( rq, 'app/user-events-joined', context, nav='events' )



'''
    Allows a user to manage their personally created events.
    Users can:
        - change event info
        - Add or remove dates for event
        - Approve or deny people who applied to join the event
        - Cancel event or mark event as complete
'''
@decorators.auth_required
def user_events_manage( rq ):
    page = int(rq.GET.get('page', '1'))

    group_list = []
    groups = CeGroup.objects.filter( creator=rq.user, deleted=False ).order_by('-datetime_created')
    
    for g in groups:
        instances = g.instances.all()
        pending_count = 0;
        for i in instances:
            pending_count = pending_count + i.attendees.filter(approved=False).count()
        group_list.append({
            'group': g,
            'pending': pending_count,
        })

    group_list = Paginator(group_list, 10)
    group_list = group_list.page( page )


    context = {
        "prev_num": group_list.previous_page_number() if group_list.has_previous() else 0,
        "prev": group_list.has_previous(),
        "next_num": group_list.next_page_number() if group_list.has_next() else 0,
        "next": group_list.has_next(),
        "groups": group_list.object_list
    }

    return render_template( rq, 'app/user-events-manage', context, nav='events' )



'''
    Ajax request handler for approving and denying users from events
'''
@decorators.auth_required
def user_events_approvedeny( rq ):
    if rq.method != 'POST':
        raise Http404

    data = {'status': 'success'}

    try:
        g = CeGroupAttendee.objects.get( pk=rq.POST.get('attendee', 0) )
    except CeGroupAttendee.DoesNotExist:
        data = {'status': 'error'}

    if rq.POST.get('approve', None) is not None:
        g.approved = 1
        g.save()

        MessageHelper_CreateNotification({
            "user": g.user,
            "text": "<strong>"+rq.user.full_name()+"</strong> accepted your request to attend <strong>"+g.group.group.name+"</strong>",
            "icon": 5,
            "link": g.group.group.details_path(),
            "primary_image": g.group.group.creator.profile.avatar_thumb_path,
        })

    elif rq.POST.get('remove', None) is not None:
        g.delete()

       
    resp = http.HttpResponse( json.dumps(data) )
    resp["Content-type"] = "application/json"
    return resp




'''
    User can edit all details for a single event
    ALSO doubles as the post handler
'''
@decorators.auth_required
def user_events_edit( rq, event_id ):

    try:
        group = CeGroup.objects.get(pk=event_id, creator=rq.user, deleted=False )
    except CeGroup.DoesNotExist:
        raise Http404


    # update event + instances all at once
    if rq.method == 'POST':
        pdata = parser.parse( rq.POST.urlencode() )

        # TODO for validation
        group.category = pdata['category'];
        group.name = pdata.get('name', '');
        group.description = pdata.get('description', '');
        group.level = pdata['level'];
        group.max_attendees = pdata.get('max_attendees', 1);
        group.location = pdata.get('location', '');
        group.save();

        instance_pks = []
        instances = group.instances.all()
        for i in instances:
            instance_pks.append( (i, i.pk) )

        instances_done = []
        if pdata.get('dates', False):
            for k, v in pdata['dates'].items():
                if v['pk'] != '':
                    try:
                        g_instance = CeGroupInstance.objects.get( pk=int(v['pk']) )
                    except CeGroupInstance.DoesNotExist:
                        pass

                    instances_done.append( int(v['pk']) )
                else:
                    g_instance = CeGroupInstance()
                    g_instance.group = group

                g_instance.notes = v.get('notes', '')
                g_instance.location = v.get('location', '')
                g_instance.event_datetime = v.get('time', '')
                g_instance.save()

                att = g_instance.attendees.all()
                for a in att:
                    GroupEditedNotification( g_instance, a.user )

            for i, pk in instance_pks:
                if pk not in instances_done:
                    att = i.attendees.all()
                    for a in att:
                        GroupDeletedNotification( i, a.user )


        else:
            if instances:
                for i in instances:
                    att = i.attendees.all()
                    for a in att:
                        GroupDeletedNotification( i, a.user )


        data = {"status": "okay"}
        resp = http.HttpResponse( json.dumps(data) )
        resp["Content-type"] = "application/json"
        rq.session['success_message'] = 'Event "'+pdata['name']+'" has been updated!'
        return resp

    # show the edit page
    else:
        context = {}

        if rq.method == 'POST':
            f = GroupForm( rq.POST, instance=group )
            if f.is_valid():
                new_event = f.save_data( rq.user )
                return shortcuts.redirect( new_event.details_path()+'/times' )
        else:
            f = GroupForm( instance=group )

        context['form'] = f
        context['instances'] = group.instances.all().order_by('event_datetime')
        context['group'] = group

        return render_template( rq, 'app/user-events-edit', context, nav='events' )


'''
    Allows a user to create a brand new event
'''
@decorators.auth_required
def user_events_new( rq, username=None ):

    context = {}

    if username:
        try:
            u = CeUser.objects.get( username=username )
        except CeUser.DoesNotExist:
            raise Http404

    if rq.method == 'POST':
        f = GroupForm( rq.POST )
        if f.is_valid():
            new_event = f.save_data( rq.user )
            if new_event.private:
                new_event.withuser = u
                new_event.save()
            return shortcuts.redirect( new_event.details_path()+'/times' )
    else:
        f = GroupForm( initial={
            "private": True if username else False,
            "max_attendees": 1 if username else 5,
        } )

    context['form'] = f
    context['buddy'] = u if username else False

    return render_template( rq, 'app/user-events-new', context, nav='events' )



'''
    User can edit times for a single event
'''
@decorators.auth_required
def user_events_new_times( rq, event_id ):

    context = {}

    try:
        group = CeGroup.objects.get(pk=event_id, creator=rq.user, deleted=False)
    except CeGroup.DoesNotExist:
        raise Http404


    if rq.method == 'POST':
        pdata = parser.parse( rq.POST.urlencode() )
        if pdata.get('dates', False):
            for k, v in pdata['dates'].items():
                if v['pk'] != '':
                    try:
                        g_instance = CeGroupInstance.objects.get( pk=int(v['pk']) )
                    except CeGroupInstance.DoesNotExist:
                        pass
                else:
                    g_instance = CeGroupInstance()
                    g_instance.group = group

                g_instance.notes = v.get('notes', '')
                g_instance.location = v.get('location', '')
                g_instance.event_datetime = v.get('time', '')
                g_instance.save()

            if not group.private:
                # create new post
                p = CePost()
                p.creator = rq.user
                p.post_type = CE_POST_TYPE_EVENT_CREATED
                p.post_id = group.pk
                p.save()
            else:
                GroupBuddyupNotification( group, group.withuser )


        data = {"status": "okay", "redirect": "/events/"+str(group.pk)}

        resp = http.HttpResponse( json.dumps(data) )
        resp["Content-type"] = "application/json"
        rq.session['success_message'] = 'Event "'+group.name+'" has been created!<br>How about inviting buddies to attend?'
        return resp
        

    else:

        context['group_times'] = group.instances.all().order_by('event_datetime')
        context['group'] = group
        return render_template( rq, 'app/user-events-new-times', context, nav='events' )



'''
    Asks a user if they really want to delete the event
'''
@decorators.auth_required
def user_events_delete( rq, event_id ):
    context = {}

    try:
        group = CeGroup.objects.get(pk=event_id, creator=rq.user, deleted=False )
    except CeGroup.DoesNotExist:
        raise Http404

    context['group'] = group

    if rq.method == 'POST' and rq.POST.get('d', None) is not None:
        for i in group.instances.all():
            i.cancelled = True;
            i.save()

        group.deleted = True
        group.save();
        rq.session['success_message'] = 'Event "'+group.name+'" has been deleted.'
        return shortcuts.redirect( '/events/manage' )
    else:

        return render_template( rq, 'app/user-events-delete', context, nav='events' )



'''
    Like user_events_new, but it sends a private invite to a single user
'''
@decorators.auth_required
def user_events_with_user( rq, username ):
    
    return render_template( rq, 'app/user-events-new', {} )





'''
    This is for mobile, mostly.
    A separate page to log workouts to save load time when on a phone.
    Uses an infinite-timeout cookie to automatically load this page and 
    log a user in when they visit campuselite.com.
'''
@decorators.auth_required
def user_log_workout( rq ):
    
    return render_template( rq, 'app/user-log-workout', {} )





'''
    The leaderboards page
'''
@decorators.auth_required
def leaderboards( rq ):
    
    context = {}
    context["suggested_events"] = GroupHelper_SuggestedGroups( rq.user )

    return render_template( rq, 'app/leaderboards', context, nav='leaderboard' )





'''
    A page where users can search for events in a calendar-like interface.
    Allows a quick-view of the event and a quick-join request
'''
@decorators.auth_required
def events_list( rq ):
    
    context = {}

    today = datetime.datetime.now()

    context['time_map'] = {
        'today': today,
        'tomorrow': today + relativedelta(days=1),
        'two_days': today + relativedelta(days=2),
        'three_days': today + relativedelta(days=3),
        'one_week': today + relativedelta(days=7),
        'two_weeks': today + relativedelta(days=14),
        'one_month': today + relativedelta(days=30),
    }

    context['group_types'] = CE_GROUP_TYPE_CHOICES
    context['buddies'] = rq.user.buddies( only_id=False )

    return render_template( rq, 'app/events-list', context, nav='events' )



'''
    Does the filtering for events
'''
@decorators.auth_required
def events_fetch_list( rq ):
    
    if rq.method == 'POST':

        today = datetime.datetime.now()
        localtz = timezone( settings.TIME_ZONE )
        today = localtz.localize(today)

        print today

        datemysql = "{:%Y-%m-%d}".format(today)
        timemysql = "{:%H\:%M\:%S}".format(today)

        context = {}

        # if getting specific user
        p_user = rq.POST.get('user', '')
        p_past = bool( int( rq.POST.get('past', '0') ) )

        # otherwise
        p_with = rq.POST.get('with', '')
        p_type = rq.POST.get('type', '')
        p_level = rq.POST.get('level', '')
        p_start = rq.POST.get('start', '')
        p_end = rq.POST.get('end', '')
        p_page = rq.POST.get('page', '1')
        p_buddies = rq.POST.get('buddies', None)
        p_page = int( p_page )

        if p_buddies:
            p_buddies = p_buddies.split( ',' )

        context['filter_title'] = 'All events'

        if p_user != '':
            if p_past:
                groups = CeGroupInstance.objects.filter( group__private=False, group__creator__username=p_user, completed=True, cancelled=False, group__deleted=False, event_datetime__lt=today ).order_by('-event_datetime')
            else:
                groups = CeGroupInstance.objects.filter( group__private=False, group__creator__username=p_user, cancelled=False, group__deleted=False, event_datetime__gt=today ).order_by('event_datetime')

            try:
                context['filter_title'] = CeUser.objects.get(username=p_user).full_name()+"'s" + ( ' past' if p_past else ' upcoming') + ' events'
            except CeUser.DoesNotExist:
                pass

        else:
            groups = CeGroupInstance.objects.filter(group__private=False, cancelled=False, event_datetime__gte=today)

            if p_with == 'buddies':
                # only select events where buddies (shared follower/following) are approved to attend
                buddies = rq.user.buddies( only_id=True )
                groups = groups.filter(attendees__user_id__in=buddies, attendees__approved=1)

            elif p_with == 'only':
                # double-check that these are actually buddies...
                if p_buddies:
                    groups = groups.filter(attendees__user_id__in=p_buddies, attendees__approved=1)
                else:
                    buddies = rq.user.buddies( only_id=True )
                    groups = groups.filter(attendees__user_id__in=buddies, attendees__approved=1)


            if p_type != '' and p_type != 'any':
                # simple category filter
                groups = groups.filter(group__category=int(p_type))

            if p_level:
                groups = groups.filter(group__level=int(p_level))


            if p_start:
                date_object = datetime.datetime.strptime(p_start+' 00:00:00', "%Y-%m-%d %H:%M:%S")
                date_object = localtz.localize(date_object)
                print date_object
                groups = groups.filter(event_datetime__gte=date_object)

            if p_end:
                date_object = datetime.datetime.strptime(p_end+' 00:00:00', "%Y-%m-%d %H:%M:%S")
                date_object = localtz.localize(date_object)
                groups = groups.filter(event_datetime__lte=date_object)    


        groups = Paginator(groups, 10)
        groups = groups.page( int(p_page) )

        if int(p_page) > 1:
            context['filter_title'] = context['filter_title'] + ' - Page '+str(p_page)


        context["next"] = groups.has_next()

        groups_list = []
        for g in groups:
            g.attend_status = g.is_attending( rq.user )
            g.user_display_package = ([], rq.user, 'is attending', 'are attending')
            groups_list.append( g )


        context['events'] = groups_list;

        return render_template( rq, 'app/ajax/events-filter-results', context )
    else:
        raise Http404




'''
    A single event with all of the information
'''
def events_details( rq, event_id ):

    context = {}

    today = datetime.datetime.now()
    localtz = timezone( settings.TIME_ZONE )
    today = localtz.localize(today)

    try:
        context['group'] = CeGroup.objects.get(pk=event_id)
    except CeGroup.DoesNotExist:
        raise Http404

    groups_list = []
    for g in context['group'].instances.filter(cancelled=False, event_datetime__gte=today):
        if rq.is_authenticated:
            g.attend_status = g.is_attending( rq.user )
            g.user_display_package = ([], rq.user, 'is attending', 'are attending', '/events/'+str(event_id)+'/attending')
        groups_list.append( g )

    context['events'] = groups_list;
    context['past_events'] = context['group'].instances.filter(cancelled=False, event_datetime__lt=today);
    
    if rq.is_authenticated:
        if not rq.is_verified:
            return shortcuts.redirect( '/verify' )
        return render_template( rq, 'app/events-details', context, nav='events' )
    else:
        context['show_invited'] = rq.GET.get('invite', False)
        return render_template( rq, 'app/events-details-anon', context )




'''
    Allows users to invite their buddies or facebook friends to an event
'''
@decorators.auth_required
def events_invite( rq, event_id ):
    context = {}

    try:
        context['group'] = CeGroup.objects.get(pk=event_id)
    except CeGroup.DoesNotExist:
        raise Http404

    if rq.GET.get('s', ''):
        rq.success_message = 'Thanks for sharing! Hopefully your event becomes really popular.'

    context['post_img'] = settings.MAIN_SITE + '/static/image/ce-logo-large.jpg'
    invites = context['group'].invitations.all()
    i_arr = []
    for i in invites:
        i_arr.append( i.email )

    fol_us = []
    fol = rq.user.following_newest()
    for f in fol:
        u = f.follower_user
        u.invite_sent = ( u.email in i_arr )
        fol_us.append( u )

    context['users'] = fol_us

    return render_template( rq, 'app/events-invite', context )




'''
    Saves a group invitation and lets cron job send the email
'''
@decorators.auth_required
def events_send_invite( rq ):
    context = {}

    from django.views.defaults import server_error
    if rq.method != 'POST':
        raise Http404

    group_id = rq.POST.get('group', 0)
    email = rq.POST.get('email', '')

    try:
        g = CeGroup.objects.get(pk=group_id)
    except CeGroup.DoesNotExist:
        raise server_error( rq )

    try:
        u = CeUser.objects.get(email=email)
    except CeGroup.DoesNotExist:
        raise server_error( rq )

    if not email:
        raise server_error( rq ) 

    try:
        i = CeGroupInvite.objects.get(email=email)
        data = {"status": "okay"}
    except CeGroupInvite.DoesNotExist:
        i = CeGroupInvite()
        i.email = email
        i.group = g
        i.save()
        MessageHelper_CreateNotification({
            "user": u,
            "text": "<strong>"+rq.user.full_name()+"</strong> has invited you to their event <strong>"+g.name+"</strong>",
            "icon": 4,
            "link": g.group.group.details_path(),
            "primary_image": rq.user.profile.avatar_thumb_path,
        })
        data = {"status": "okay"}


    resp = http.HttpResponse( json.dumps(data) )
    resp["Content-type"] = "application/json"

    return resp




'''
    A single event with all of the information
'''
@decorators.auth_required
def user_edit_supplements( rq ):
    context = {}
    return render_template( rq, 'app/supplements-edit', context )



'''
    a full list of people to motivate
'''
def people_to_motivate( rq ):
    context = {}
    context["suggested_events"] = GroupHelper_SuggestedGroups( rq.user )
    context["suggested_friends"] = UserHelper_SuggestedFriends( rq.user, limit=False )
    return render_template( rq, 'app/people-to-motivate', context )













'''
    Show a "we sent you an email" page + resend button
'''
@decorators.auth_required( do_verify=False )
def verify_email( rq ):
    #if rq.is_verified:
        #return shortcuts.redirect( '/' )

    resent = False
    if rq.method == 'POST':
        MessageHelper_SendVerify( rq )
        resent = True


    return render_template( rq, 'onboarding/verification', {"resent": resent} )


'''
    Quick dev-only shortcut to verify any email address without getting sent one
'''
@decorators.auth_required( do_verify=False )
def skip_verify( rq ):
    if not rq.GET.get('please', False):
        return shortcuts.redirect( '/verify' )

    rq.user.verified = True
    rq.user.save()

    return shortcuts.redirect( '/start' )




'''
    Onboarding screen shown after join
'''
@decorators.auth_required
def onboarding_intro( rq ):

    context = {
        'next_url': '/start/events',
        'next_text': 'Next page',
        'interests': CE_GROUP_TYPE_CHOICES,
    }

    return render_template( rq, 'onboarding/onboarding-intro', context )



'''
    Onboarding screen shown after join
    NOT BEING USED
'''
@decorators.auth_required
def onboarding_interests( rq ):
    context = {
        'next_url': '/start/events',
        'next_text': 'Next page',
    }
    return render_template( rq, 'onboarding/onboarding-interests', context )



'''
    Onboarding screen shown after join
'''
@decorators.auth_required
def onboarding_events( rq ):

    context = {
        'next_url': '/start/motivate',
        'next_text': 'Next page',
        "events": GroupHelper_SuggestedGroups( rq.user, limit=10 ),
    }
    
    return render_template( rq, 'onboarding/onboarding-events', context )



'''
    Onboarding screen shown after join
'''
@decorators.auth_required
def onboarding_friends( rq ):

    context = {
        'next_url': '/start/workouts',
        'next_text': 'Next page',
        "suggested_friends": UserHelper_SuggestedFriends( rq.user, limit=10, add_extra=True ),
    }
    
    return render_template( rq, 'onboarding/onboarding-friends', context )



'''
    Onboarding screen shown after join
'''
@decorators.auth_required
def onboarding_workouts( rq ):

    context = {
        'next_url': '/?ob=1',
        'success': False,
        'next_text': 'Go to feed',
        'autocomplete_workouts': CE_WORKOUT_LIFTS_CHOICES,
        'score_labels': CE_WORKOUT_SCORE_LABEL_CHOICES,
        'weight_labels': CE_WORKOUT_WEIGHT_LABEL_CHOICES,
        'distance_labels': CE_WORKOUT_DIST_LABEL_CHOICES,
    }

    context['workouts'] = CeWorkout.objects.filter(user_id=rq.user.pk).order_by('-datetime_updated')

    if rq.method == 'POST':
        f = WorkoutForm( rq.POST )
        context['form'] = f
        if f.is_valid():
            new_workout = f.save_data( rq.user )
            rq.session['success_message'] = "New workout \""+new_workout.name+"\" created succesfully."
            context['success'] = True
    else:
        context['form'] = WorkoutForm()
    
    return render_template( rq, 'onboarding/onboarding-workouts', context )


'''
    To collect data by ajax during onboarding
'''
@decorators.auth_required
@decorators.jsonify
def onboarding_ajax_goals(rq):
    if rq.method == 'POST':
        interests = rq.POST.getlist('interests[]')
        goals = rq.POST.get('goals', '')
        level = rq.POST.get('level', '0')

        # delete all users interests 
        rq.user.interests.all().delete()
        for i in interests:
            inter = CeUserInterest()
            inter.interest_type = int(i)
            inter.user = rq.user
            inter.save()

        rq.user.profile.goals = goals
        rq.user.profile.fitness_level = int(level)
        rq.user.profile.save()

    return {'status': 'success'}






'''
    Shows a collection of users based on a search criteria that could be followed
'''
@decorators.auth_required
def similar_users( rq, sim_type, sim_id ):

    itype_map = {
        "interests": "interest",
        "majors": "major",
        "minors": "minor",
        "colleges": "college",
        "fitness-level": "fitness level",
    }

    context = {
        "interest_type": itype_map[ sim_type ]
    }

    fol = rq.user.following_newest()
    fol_ids = [ rq.user.pk ]
    for f in fol:
        fol_ids.append( f.follower_user_id )

    context['users'] = []
    context['supplimentary'] = []
    if sim_type == 'interests':
        context['type_display'] = get_interest_display( int(sim_id) )
        interestmatches = CeUserInterest.objects.filter(interest_type=sim_id, user__verified=True, user__deleted=False).exclude(user_id__in=fol_ids).select_related('user').order_by('?')[:30]
        for i in interestmatches:
            context['users'].append( i.user )


    if not context['users']:
        context['supplimentary'] = CeUser.objects.filter(verified=True, deleted=False).exclude(pk__in=fol_ids).order_by('?')[:20]

    context["suggested_events"] = GroupHelper_SuggestedGroups( rq.user )


    return render_template( rq, 'app/similar-users', context )



'''
    Ajax handler for posting statuses, photos, videos, and logs to the feed
'''
@decorators.auth_required
def post_create( rq ):

    from django.views.defaults import server_error
    if rq.method != 'POST':
        raise Http404

    post_type = rq.POST.get('type', None)
    if not post_type:
        return server_error(rq)

    # text post
    if post_type == 'text':
        f = PostStatusForm( data=rq.POST )
        if f.is_valid():
            new_post = f.save_data( user=rq.user )

    # photo/photoset post
    elif post_type == 'photo':
        print rq.POST.getlist('images[]')
        f = PostPhotosForm( data=rq.POST )
        if f.is_valid():
            new_post = f.save_data( user=rq.user, images=rq.POST.getlist('images[]') )

    # embedded video post
    elif post_type == 'video':
        f = PostVideoForm( data=rq.POST )
        if f.is_valid():
            new_post = f.save_data( user=rq.user )

    # workout log
    elif post_type == 'log':
        try:
            # double check that it exists
            w = CeWorkout.objects.get(pk=rq.POST.get('workout_id', 0), user=rq.user)

            workout_data = {
                "reps": rq.POST.get('reps', 0),
                "weight": rq.POST.get('weight', 0), 
                "weight_label": rq.POST.get('weight_label', 0), 
                "time": rq.POST.get('time', 0), 
                "score": rq.POST.get('score', 0), 
                "score_label": rq.POST.get('score_label', 0), 
                "distance": rq.POST.get('distance', 0), 
                "distance_label": rq.POST.get('distance_label', 0), 
            }
            f = WorkoutLogForm( data=workout_data )
            if f.is_valid():
                new_post = f.save_post_data( user=rq.user, workout_id=w.pk )
            else:
                print "Invalid..."

        except CeWorkout.DoesNotExist:
            print "Workout doesnt exist"
            return server_error(rq)

    # anything else is garbage
    else:
        print "Incorrect post type!!"
        return server_error(rq)

    # if something wasnt valid, also send a fail response
    if not new_post:
        print "no new post???"
        return server_error(rq)

    # output the html of the new post listitem
    context = {}
    context['post'] = PostHelper_FormatData( [new_post], rq )[0]

    return render_template( rq, 'app/ajax/post-list-item', context )







@decorators.jsonify
def check_username( rq ):
    if rq.method != 'POST':
        raise Http404

    username = rq.POST.get('username', '');

    ret = {"available": True if username != '' else False}
    try:
        u = CeUser.objects.get(username=username, deleted=False) 
        ret = {"available": False}
    except CeUser.DoesNotExist:
        pass

    return ret



@decorators.jsonify
def check_email( rq ):
    if rq.method != 'POST':
        raise Http404

    email = rq.POST.get('email', '');

    ret = {"available": True if email != '' else False}
    try:
        u = CeUser.objects.get(email=email, deleted=False)
        ret = {"available": False}
    except CeUser.DoesNotExist:
        pass

    return ret


@decorators.jsonify
def save_user_image( rq ):
    if rq.method != 'POST':
        raise Http404

    image_name = rq.POST.get('image', '');

    if image_name:

        # save new image
        i300 = image_name+'-300.jpg'
        i120 = image_name+'-120.jpg'
        i50 = image_name+'-50.jpg' 

        # We dont create a post for their first profile photo, because
        # every user will probably do it and thats annoying
        do_post = True
        if rq.user.profile.avatar_large == '':
           do_post = False 

        today = datetime.datetime.now()
        localtz = timezone( settings.TIME_ZONE )
        today = localtz.localize(today)
        cutoff = today - relativedelta(days=2)

        if rq.user.date_joined > cutoff:
           do_post = False 

        rq.user.profile.avatar_large = i300
        rq.user.profile.avatar_medium = i120
        rq.user.profile.avatar_thumb = i50
        rq.user.profile.save()

        # create a photo post 
        image_data = {
            "caption": rq.user.first_name+" updated their profile photo:"
        }
        f = PostPhotosForm( data=image_data )
        if f.is_valid():
            if do_post:
                new_post = f.save_data( user=rq.user, images=[image_name], location="image/user", thumb_ext='-120' )
            else:
                # doesnt create a feed post but does create an album image
                new_post = f.save_data_album( user=rq.user, images=[image_name], location="image/user", thumb_ext='-120' )


        return {"success": True}

    return {"success": False}



@decorators.jsonify
def upload_post_images( rq ):
    from django.views.defaults import server_error
    print rq.FILES
    files = rq.FILES.getlist('photosToUpload[]', None)
    print files

    # different from avatar because this processes multiple files
    def crop_and_rename(fil):
        print "3"
        from PIL import Image
        from PIL.ExifTags import TAGS
        import glob, os

        extension = os.path.splitext(fil.name)[1]

        if extension.lower() not in ['.jpg', '.jpeg', '.gif', '.bmp', '.png']:
            return False, False, False

        max_size = 1000, 600
        size_200 = 200, 200
        print "4"

        name = os.urandom(4).encode('hex') + '-' + os.urandom(8).encode('hex')
        tempName = settings.BASE_DIR+'/static/media/temp/'+name+extension

        # first, save file in temp
        print "5"
        #os.chmod(tempName, 0777)
        print "5.1"

        with open(tempName, 'wb+') as destination:
            print "5.2"
            os.chmod(tempName, 0777)
            print "5.5"
            for chunk in fil.chunks():
                print "writing..."
                destination.write(chunk)

        print "6"
        os.chmod(tempName, 0777)

        nameOrig = settings.BASE_DIR+'/static/media/'+name+'.jpg'
        name200 = settings.BASE_DIR+'/static/media/'+name+'-thumb.jpg'
        print "7"

        rotate_orient = [
            (3, 180),
            (4, 180),
            (5, 270),
            (6, 270),
            (7, 90),
            (8, 90),
        ]

        im = Image.open(tempName) 
        im.load()
        w2, h2 = im.size

        do_rotate = False
        rotate_amount = 0

        print "original:"
        print w2, h2

        # attempt to fix orientation issue
        if extension.lower() in ['.jpg', '.jpeg']:
            exifdict = im._getexif()
            if exifdict is not None and len(exifdict):
                for k in exifdict.keys():
                    if k in TAGS.keys():
                        #print "t"
                        #print TAGS[k], exifdict[k]
                        if TAGS[k] == 'Orientation':
                            for kk, r in rotate_orient:
                                if int(exifdict[k]) == kk:
                                    do_rotate = True
                                    rotate_amount = r
                                    break;
                    else:
                        #print "reg"
                        #print k, exifdict[k]
                        if k == 'Orientation':
                            for kk, r in rotate_orient:
                                if int(exifdict[k]) == kk:
                                    do_rotate = True
                                    rotate_amount = r
                                    break;

        if do_rotate:
            print "ROTATING "+str(rotate_amount)+' DEG'
            imrot = im.rotate(rotate_amount)
            imrot.save( tempName, "JPEG" )

            im = Image.open(tempName) 
            im.load()
            w2, h2 = im.size 

            print "new:"
            print w2, h2
 
        print "8"
        if extension in ['.png', '.gif']:
            try:
                # use a whitelayer underneath to create a no-transparent version
                # http://stackoverflow.com/questions/9166400/convert-rgba-png-to-rgb-with-pil 
                whitelayer = Image.new('RGB', (w2, h2), "white" )
                whitelayer.paste( im, mask=im.split()[3] )
                whitelayer.thumbnail( max_size, Image.ANTIALIAS )
                whitelayer.save( nameOrig, "JPEG" )   
            except IndexError:
                # this might not have alpha data for some reason
                im.thumbnail( max_size, Image.ANTIALIAS )
                im.save( nameOrig, "JPEG" )
        else:
            im.thumbnail( max_size, Image.ANTIALIAS )
            im.save( nameOrig, "JPEG" ) 
        print "9"

        os.chmod(nameOrig, 0777)

        im2 = Image.open(nameOrig)
        w, h = im2.size
        if w > h:
            diff = w-h
            c = im2.crop( ( ( diff / 2 ), 0, (w - ( diff / 2 )), h) )
        elif w < h:
            diff = h-w
            c = im2.crop((0, ( diff / 2 ), w, ( h - ( diff / 2 ) ) ))
        else:
            c = im2
        c.thumbnail(size_200, Image.ANTIALIAS)
        c.save(name200, "JPEG")

        os.chmod(name200, 0777)

        print "10"
        name_ret = 'media/'+name+'-thumb.jpg'
        s3_names = (  '/media/'+name+'-thumb.jpg', '/media/'+name+'.jpg' )
        os.remove( tempName )
        return name_ret, name, s3_names


    if files:
        name_list = [] # to save in database in the post
        path_list = [] # to show on screen preview
        path_s3_list = [] # to show on screen preview
        print "1"
        for f in files:
            filename, name_s, s3files = crop_and_rename( f )
            name_list.append( name_s )
            path_list.append( filename )
            path_s3_list.append( s3files )
        print "2"

        if not settings.DEBUG:
            import boto
            from boto.s3.key import Key

            print "connect"
            c = boto.connect_s3()
            print "boto"
            b = c.get_bucket('campuselite')
            print "campus"
            for thu, orig in path_s3_list:
                print "upload"
                print thu
                k = Key(b)
                k.key = thu
                k.set_contents_from_filename(settings.BASE_DIR+'/static'+thu)

                print "upload 2"
                print orig
                k2 = Key(b)
                k2.key = orig
                k2.set_contents_from_filename(settings.BASE_DIR+'/static'+orig)
        
        return {"images": path_list, 'imagesToSave': name_list}


    #if not valid
    server_error( rq )





@decorators.jsonify
def upload_image( rq ):
    import logging
    from django.views.defaults import server_error
    if not settings.DEBUG:
        logging.basicConfig(filename='/home/campuselite/wsgi/python_error_log.log',level=logging.DEBUG)
    print( 'doing1')

    def crop_and_rename(fil):
        from PIL import Image
        from PIL.ExifTags import TAGS
        import glob, os
        print( 'doing2')

        extension = os.path.splitext(fil.name)[1]
        if extension.lower() not in ['.jpg', '.jpeg', '.gif', '.bmp', '.png']:
            return False, False

        max_size = 600, 600
        size_300 = 300, 300
        size_120 = 120, 120
        size_50 = 50, 50
        print( 'doing3')

        
        # first, save file in temp
        name = os.urandom(6).encode('hex') + '-' + os.urandom(8).encode('hex')
        tempName = settings.BASE_DIR+'/static/image/temp/'+name+extension
        with open(settings.BASE_DIR+'/static/image/temp/'+name+extension, 'wb+') as destination:
            os.chmod(settings.BASE_DIR+'/static/image/temp/'+name+extension, 0777)
            for chunk in fil.chunks():
                destination.write(chunk)

        print( 'doing4')


        nameOrig = settings.BASE_DIR+'/static/image/user/'+name+'.jpg'
        name300 = settings.BASE_DIR+'/static/image/user/'+name+'-300.jpg'
        name120 = settings.BASE_DIR+'/static/image/user/'+name+'-120.jpg'
        name50 = settings.BASE_DIR+'/static/image/user/'+name+'-50.jpg'
        print( 'doing5')

        os.chmod(settings.BASE_DIR+'/static/image/temp/'+name+extension, 0777)
        print( 'doingChmod')
        im = Image.open(settings.BASE_DIR+'/static/image/temp/'+name+extension)     
        w, h = im.size
        im.load()
        print( 'doing6')

        do_rotate = False
        rotate_amount = 0

        rotate_orient = [
            (3, 180),
            (4, 180),
            (5, 270),
            (6, 270),
            (7, 90),
            (8, 90),
        ]

        # attempt to fix orientation issue
        if extension.lower() in ['.jpg', '.jpeg']:
            exifdict = im._getexif()
            if exifdict is not None and len(exifdict):
                for k in exifdict.keys():
                    if k in TAGS.keys():
                        #print "t"
                        #print TAGS[k], exifdict[k]
                        if TAGS[k] == 'Orientation':
                            for kk, r in rotate_orient:
                                if int(exifdict[k]) == kk:
                                    do_rotate = True
                                    rotate_amount = r
                                    break;
                    else:
                        #print "reg"
                        #print k, exifdict[k]
                        if k == 'Orientation':
                            for kk, r in rotate_orient:
                                if int(exifdict[k]) == kk:
                                    do_rotate = True
                                    rotate_amount = r
                                    break;

        if do_rotate:
            print "ROTATING "+str(rotate_amount)+' DEG'
            imrot = im.rotate(rotate_amount)
            imrot.save( tempName, "JPEG" )

            im = Image.open(tempName) 
            im.load()
            w, h = im.size 


        if w > h:
            diff = w-h
            c = im.crop( ( ( diff / 2 ), 0, (w - ( diff / 2 )), h) )
        elif w < h:
            diff = h-w
            c = im.crop((0, ( diff / 2 ), w, ( h - ( diff / 2 ) ) ))
        else:
            c = im

        print( 'doing7')
        if extension in ['.png', '.gif']:
            try:
                # use a whitelayer underneath to create a no-transparent version
                # http://stackoverflow.com/questions/9166400/convert-rgba-png-to-rgb-with-pil 
                whitelayer = Image.new('RGB', c.size, "white" )
                whitelayer.paste( c, mask=c.split()[3] )
                whitelayer.thumbnail( max_size, Image.ANTIALIAS )
                whitelayer.save( nameOrig, "JPEG" )   
            except IndexError:
                # this might not have alpha data for some reason
                c.thumbnail( max_size, Image.ANTIALIAS )
                c.save( nameOrig, "JPEG" )
        else:
            c.thumbnail( max_size, Image.ANTIALIAS )
            c.save(nameOrig, "JPEG")
        print( 'doing8')

        im = Image.open(nameOrig)
        im.thumbnail(size_300, Image.ANTIALIAS)
        im.save(name300, "JPEG")
        os.chmod(name300, 0777)
        
        im = Image.open(nameOrig)
        im.thumbnail(size_120, Image.ANTIALIAS)
        im.save(name120, "JPEG")
        os.chmod(name120, 0777)

        im = Image.open(nameOrig)
        im.thumbnail(size_50, Image.ANTIALIAS)
        im.save(name50, "JPEG")
        os.chmod(name50, 0777)
        print( 'doing9')

        name_ret = 'image/user/'+name+'-300.jpg'
        os.remove( settings.BASE_DIR+'/static/image/temp/'+name+extension )
        return name_ret, name


    form = TempPhotoUploadForm(rq.POST, rq.FILES)
    print( 'doing11')
    if form.is_valid():
        print( 'doing12')
        filename, name_s = crop_and_rename(rq.FILES['photo'])
        if not settings.DEBUG and filename != False:
            import boto
            from boto.s3.key import Key

            c = boto.connect_s3()
            b = c.get_bucket('campuselite')
            
            k = Key(b)
            k.key = '/image/user/'+name_s+'-50.jpg'
            k.set_contents_from_filename(settings.BASE_DIR+'/static/image/user/'+name_s+'-50.jpg')

            k2 = Key(b)
            k2.key = '/image/user/'+name_s+'-120.jpg'
            k2.set_contents_from_filename(settings.BASE_DIR+'/static/image/user/'+name_s+'-120.jpg')

            k3 = Key(b)
            k3.key = '/image/user/'+name_s+'-300.jpg'
            k3.set_contents_from_filename(settings.BASE_DIR+'/static/image/user/'+name_s+'-300.jpg')

            k4 = Key(b)
            k4.key = '/image/user/'+name_s+'.jpg'
            k4.set_contents_from_filename(settings.BASE_DIR+'/static/image/user/'+name_s+'.jpg')

        print( 'doing13')
        return {"image": filename, 'imageToSave': name_s}

    #if not valid
    #server_error( rq )
    return {"image": False, 'imageToSave': False}


@decorators.auth_required
@decorators.jsonify
def flag_post_inappropriate( rq, pid ):
    from django.views.defaults import server_error

    try:
        post = CePost.objects.get( pk=pid )
        m = """
        <p><strong>Someone flagged a post as inappropriate</strong></p>
        <br>
        """

        m += "<p>Flagged by: %s</p>" % rq.user.full_name()
        m += "<p>%s's profile: <a href='%s'>%s</p></p><br>" % (rq.user.first_name, rq.user.profile_path_short_absolute(), rq.user.profile_path_short_absolute())
        m += "<p>Post flagged: <a href='%s'>%s</p>" % (post.get_public_path_absolute(), post.get_public_path_absolute())
        m += "<p>Post creator's profile: <a href='%s'>%s</p>" % (post.creator.profile_path_short_absolute(), post.creator.profile_path_short_absolute())

        t = compile_email_template({ "body_content": m })
        send_user_email( rq, {
            "subject": 'Post ID '+pid+' flagged by '+rq.user.full_name(),
            "message": t,
            "to": ['ptewarie@gmail.com'],
            "from": 'Campus Elite <alerts@thecampuselite.com>',
        } )

        return {"flagged": True}
    except CePost.DoesNotExist:
        pass

    server_error( rq )



@decorators.auth_required
@decorators.jsonify
def delete_post( rq, pid ):
    from django.views.defaults import server_error

    try:
        post = CePost.objects.get( pk=pid, creator=rq.user )
        post.deleted = True
        post.save()

        return {"deleted": True}
    except CePost.DoesNotExist:
        pass

    server_error( rq )



@decorators.auth_required
@decorators.jsonify
def edit_post( rq, pid ):
    from django.views.defaults import server_error

    try:
        post = CePost.objects.get( pk=pid, creator=rq.user )

        pi = post.get_data()
        pi.caption = rq.POST['caption']
        pi.save()

        post.edited = True
        post.save()

        return {"edited": True}
    except CePost.DoesNotExist:
        pass
    except KeyError:
        pass

    server_error( rq )


@decorators.auth_required
@decorators.jsonify
def edit_comment( rq, pid ):
    from django.views.defaults import server_error

    try:
        c = CeComment.objects.get( pk=pid, user=rq.user )

        c.comment = rq.POST['comment']
        c.save()

        return {"edited": True}
    except CeComment.DoesNotExist:
        pass
    except KeyError:
        pass

    server_error( rq )


@decorators.auth_required
@decorators.jsonify
def delete_comment( rq, pid ):
    from django.views.defaults import server_error

    try:
        c = CeComment.objects.get( pk=pid, user=rq.user )
        c.delete()

        return {"edited": True}
    except CeComment.DoesNotExist:
        pass
    except KeyError:
        pass

    server_error( rq )



@decorators.auth_required
def test_email( rq ):
    from django.http import HttpResponse


    m = """
    <p>Hello there!</p>
    <p>This is a test email!</p>
    <p>Sincerely,</p>
    <p>Campus Elite</p>
    """

    t = compile_email_template({ "body_content": m })
    send_user_email( rq, {
        "subject": 'Test email - CE',
        "message": t,
        "to": ['highwaytoinfinity@gmail.com'],
        "from": 'Campus Elite <alerts@thecampuselite.com>',
    } )

    response = HttpResponse()
    response.write("<p>Sent!</p>")
    return response



'''
    Search handler for the global search bar.
    Will need to start indexing database columns to make this fast.
'''
@decorators.auth_required
@decorators.jsonify
def global_search( rq ):

    from django.db.models import Q

    term = rq.GET.get('term', '')
    search_results = { 'results': [] }

    fol = rq.user.following_newest()
    fol_ids = [ rq.user.pk ]
    for f in fol:
        fol_ids.append( f.follower_user_id )

    events = list(CeGroup.objects.filter(name__icontains=term, creator=rq.user, deleted=False)[:5])
    events.extend( list( CeGroup.objects.filter(name__icontains=term, instances__attendees__user=rq.user, deleted=False)[:5] ) )
    workouts = CeWorkout.objects.filter(name__icontains=term, user=rq.user)[:5]
    users = CeUser.objects.filter(Q(pk__in=fol_ids) & Q(deleted=False) & Q(first_name__icontains=term) | Q(last_name__icontains=term)).select_related('profile')[:5]


    if len(users) == 0:
        user_list = CeUser.objects.filter(Q(pk__in=fol_ids) & Q(deleted=False)).select_related('profile')
        users = []
        ulen = 0
        for u in user_list:
            if term.lower() in u.full_name().lower():
                users.append( u )

            ulen = ulen + 1
            if ulen == 5:
                break

    for e in events:
        obj = {
            "type": "event", 
            "link": "/events/"+str(e.pk), 
            "text": e.name,
        }
        search_results['results'].append( obj )

    for w in workouts:
        obj = {
            "type": "workout", 
            "link": rq.user.profile_path()+'/workout/'+str(w.pk), 
            "text": w.name,
        }
        search_results['results'].append( obj )

    for u in users:
        obj = {
            "type": "user", 
            "link": u.profile_path(),
            "text": u.full_name(),
            "image": u.profile.avatar_thumb_path,
        }
        search_results['results'].append( obj )


    search_results['results'] = search_results['results'][:5]

    return search_results



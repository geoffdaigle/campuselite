from django.conf.urls import patterns, url, include
from ce_app import views


urlpatterns = patterns('',
    # -------------------------------------------------------------
    # Real pages
    # -------------------------------------------------------------
    url(r'^/$', 'ce_app.views.dashboard'),
    url(r'^$', 'ce_app.views.dashboard'),
    url(r'^settings$', 'ce_app.views.user_settings'),
    url(r'^notifications$', 'ce_app.views.user_notifications'),

    # long-form
    url(r'^user/(?P<username>[0-9a-zA-Z\-\_\.]+)$', 'ce_app.views.user_view'),
    # alternative
    url(r'^u/(?P<username>[0-9a-zA-Z\-\_\.]+)$', 'ce_app.views.user_view'),

    url(r'^u/(?P<username>[0-9a-zA-Z\-\_\.]+)/posts/(?P<post_id>[0-9]+)$', 'ce_app.views.user_public_post'),
    url(r'^user/(?P<username>[0-9a-zA-Z\-\_\.]+)/posts/(?P<post_id>[0-9]+)$', 'ce_app.views.user_public_post'),
    url(r'^u/(?P<username>[0-9a-zA-Z\-\_\.]+)/posts/(?P<post_id>[0-9]+)/bumped$', 'ce_app.views.user_post_bumped'),
    url(r'^user/(?P<username>[0-9a-zA-Z\-\_\.]+)/posts/(?P<post_id>[0-9]+)/bumped$', 'ce_app.views.user_post_bumped'),


    url(r'^user/(?P<username>[0-9a-zA-Z\-\_\.]+)/edit$', 'ce_app.views.user_edit'),
    url(r'^user/(?P<username>[0-9a-zA-Z\-\_\.]+)/diet/followers$', 'ce_app.views.user_diet_followers'),
    url(r'^user/(?P<username>[0-9a-zA-Z\-\_\.]+)/photos$', 'ce_app.views.user_photos'),
    url(r'^user/(?P<username>[0-9a-zA-Z\-\_\.]+)/follow$', 'ce_app.views.user_follows'),
    url(r'^user/(?P<username>[0-9a-zA-Z\-\_\.]+)/workouts$', 'ce_app.views.user_workouts'),
    url(r'^user/(?P<username>[0-9a-zA-Z\-\_\.]+)/workout/(?P<wid>[0-9]+)$', 'ce_app.views.user_workout_detail'),
    url(r'^events/manage$', 'ce_app.views.user_events_manage'),
    url(r'^events/joined$', 'ce_app.views.user_events_joined'),
    url(r'^events/manage/(?P<event_id>[0-9]+)$', 'ce_app.views.user_events_edit'),
    url(r'^events/delete/(?P<event_id>[0-9]+)$', 'ce_app.views.user_events_delete'),
    url(r'^events/new$', 'ce_app.views.user_events_new'),
    url(r'^events/attendees$', 'ce_app.views.user_events_approvedeny'),
    url(r'^events/(?P<event_id>[0-9]+)/times$', 'ce_app.views.user_events_new_times'),
    url(r'^events/with/(?P<username>[0-9a-zA-Z\-\_\.]+)$', 'ce_app.views.user_events_new'),
    url(r'^log$', 'ce_app.views.user_log_workout'),
    url(r'^leaderboards$', 'ce_app.views.leaderboards'),
    url(r'^similar/(?P<sim_type>(interests|majors|minors|schools|fitness-level)+)/(?P<sim_id>[0-9]+)$', 'ce_app.views.similar_users'),
    url(r'^events$', 'ce_app.views.events_list'),
    url(r'^events/(?P<event_id>[0-9]+)$', 'ce_app.views.events_details'),
    url(r'^events/(?P<event_id>[0-9]+)/invite$', 'ce_app.views.events_invite'),
    url(r'^supplements$', 'ce_app.views.user_edit_supplements'),
    url(r'^workouts$', 'ce_app.views.user_edit_workouts'),
    url(r'^workouts/new$', 'ce_app.views.user_new_workout'),
    url(r'^workouts/delete/(?P<wid>[0-9]+)$', 'ce_app.views.user_delete_workout'),
    url(r'^people\-to\-motivate$', 'ce_app.views.people_to_motivate'),

    url(r'^diet$', 'ce_app.views.user_edit_diet'),
    url(r'^diets$', 'ce_app.views.user_diets_followed'),

    # -------------------------------------------------------------
    # Onboarding
    # -------------------------------------------------------------

    url(r'^verify$', 'ce_app.views.verify_email'),
    url(r'^skip_verify$', 'ce_app.views.skip_verify'),
    url(r'^start$', 'ce_app.views.onboarding_intro'),
    url(r'^start/interests$', 'ce_app.views.onboarding_interests'),
    url(r'^start/events$', 'ce_app.views.onboarding_events'),
    url(r'^start/motivate$', 'ce_app.views.onboarding_friends'),
    url(r'^start/workouts$', 'ce_app.views.onboarding_workouts'),
    url(r'^start/ajax/goals$', 'ce_app.views.onboarding_ajax_goals'),


    # -------------------------------------------------------------
    # Ajax handlers
    # -------------------------------------------------------------

    url(r'^search$', 'ce_app.views.global_search'),

    url(r'^notifications/list$', 'ce_app.views.get_notifications'),
    url(r'^notifications/count$', 'ce_app.views.get_notifications_number'),
    url(r'^notifications/shown$', 'ce_app.views.notifs_shown'),
    url(r'^notifications/check$', 'ce_app.views.check_new_notifications'),

    url(r'^feed$', 'ce_app.views.fetch_feed_page'),

    url(r'^events/query$', 'ce_app.views.events_fetch_list'),
    url(r'^events/sendinvite$', 'ce_app.views.events_send_invite'),
    url(r'^post/create$', 'ce_app.views.post_create'),
    url(r'^upload-image$', 'ce_app.views.upload_image'),
    url(r'^update-image$', 'ce_app.views.save_user_image'),
    url(r'^post/tempimg$', 'ce_app.views.upload_post_images'),

    url(r'^photo/delete/(?P<photo_id>[0-9]+)$', 'ce_app.views.user_delete_photo'),


    url(r'^post/flag/(?P<pid>[0-9]+)$', 'ce_app.views.flag_post_inappropriate'),
    url(r'^post/delete/(?P<pid>[0-9]+)$', 'ce_app.views.delete_post'),
    url(r'^post/edit/(?P<pid>[0-9]+)$', 'ce_app.views.edit_post'),

    url(r'^comment/delete/(?P<pid>[0-9]+)$', 'ce_app.views.delete_comment'),
    url(r'^comment/edit/(?P<pid>[0-9]+)$', 'ce_app.views.edit_comment'),

    url(r'^diets/follow$', 'ce_app.views.follow_diet'),

    # prefetch photos for corral
    url(r'^user/(?P<username>[0-9a-zA-Z\-\_\.]+)/photos/load$', 'ce_app.views.load_user_photos'),

    # availablility checkers
    url(r'^check-email$', 'ce_app.views.check_email'),
    url(r'^check-username$', 'ce_app.views.check_username'),


    # test emails
    url(r'^test$', 'ce_app.views.test_email'),


)
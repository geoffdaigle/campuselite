from django.template import Library
import re

register = Library()

@register.filter
def url_target_blank(text):
    return text.replace('<a ', '<a target="_blank" ')


'''
	Shortcut for doing a numeric for loop in a template
'''
@register.filter
def get_range( value ):
	return range( value )



'''
	Creates an html list of users that are grouped together
	(attending or fist bumped or commented, etc) showing 
	buddies and people who you motivate before anything else.

	e.g. "Mike Bardeaux, Geoff Daigle, and 1 other gave a fist bump."
'''
@register.filter
def users_included( user_package ):

	if not user_package:
		return ''

	user_list = user_package[0]
	this_user = user_package[1]
	singular_action = user_package[2]
	plural_action = user_package[3]
	all_url = user_package[4]

	if len( user_list ) == 0:
		return ''

	include_you = False
	others = 0
	buddies = []

	for u in user_list:
		if u.pk == this_user.pk:
			include_you = True
		elif this_user is not None:
			if this_user.is_following( u ) and len(buddies) < 2 :
				buddies.append( u )
			else:
				others = others + 1
		else:
			others = others + 1

	output_str = ''
	other_str = ''
	use_plural = True

	if include_you:
		buddies = ['You'] + buddies


	lbuddies = len(buddies)

	if others > 0 and lbuddies == 0:
		infl = 'people'
		if others == 1:
			infl = 'person'
			use_plural = False
		other_str = '<a href="'+all_url+'">'+str(others)+' '+infl+'</a>'

	if others > 0 and lbuddies > 0: 
		infl = 'others'
		if others == 1:
			infl = 'other'
		other_str = ' and <a href="'+all_url+'">'+str(others)+' '+infl+'</a>'

	if lbuddies == 1 and others == 0 and not include_you:
		use_plural = False

	if lbuddies > 0: 
		for key, b in enumerate(buddies):
			if b == 'You':
				output_str += '<span class="you-bump">You, </span>' if (lbuddies > 1 and others > 0 ) or (lbuddies > 2) else '<span class="you-bump">You </span>'
			else:
				if key == 0 and (key+1) == lbuddies and others == 0:
					output_str += '<a href="'+b.profile_path()+'">'+b.full_name()+'</a>'
				elif (key+1) == lbuddies and others == 0:
					output_str += 'and <a href="'+b.profile_path()+'">'+b.full_name()+'</a>'
				else:
					output_str += '<a href="'+b.profile_path()+'">'+b.full_name()+'</a>, ' if lbuddies > 1 else '<a href="'+b.profile_path()+'">'+b.full_name()+'</a>'


	return output_str + other_str + ' ' + ( plural_action if use_plural else singular_action )








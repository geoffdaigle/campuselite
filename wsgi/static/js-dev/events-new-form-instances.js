
(function() {

	var saving = false;

	var editFormSubmit = function() {

		// no double-submits
		if (saving) return false;
		saving = true;

		// Returns state object that was been created through event-dates component.
		// Assumes campuselite.exportEventDates() exists
		var datesObj = campuselite.exportEventDates();

		var formData = {
			dates: datesObj
		};

		if (typeof window.eventprivate !== 'undefined' ) {
			if (window.eventprivate && datesObj.length === 0) {
				alert('Please select at least one date for this event.');
				saving = false;
				return false;
			}
		}

		campuselite.serverQuery( window.location.href, {
			data: formData,
			method: 'post',
			onDone: function( returned ) {
				returned = $.parseJSON(returned);
				saving = false;
				window.location = returned.redirect;
			}
		} );

	};

	$(document).ready(function() {
		$('.c-primary-action').click( editFormSubmit );
	});

})();
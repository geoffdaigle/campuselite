
( function() {

	var iniSuggestions = function() {

		if (typeof majorTypes === 'undefined') {
			console.error( '"majorTypes" is not defined! Cant create suggestion list...' );
			return false;
		}

		var majors = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			local: majorTypes
		});

		majors.initialize();

		$('#id_major').typeahead({
			hint: true,
			highlight: true,
			minLength: 1
		},
		{
			name: 'workouts',
			displayKey: 'value',
			source: majors.ttAdapter()
		});

		$('#id_minor').typeahead({
			hint: true,
			highlight: true,
			minLength: 1
		},
		{
			name: 'workouts',
			displayKey: 'value',
			source: majors.ttAdapter()
		});
	};





	var iniInterests = function() {

		var addInterest = function() {
			var id = $(this).val();

			var i = interestsPicked.indexOf( parseInt(id) );
			if (i !== -1) {
				$(this).val(0)
				return false;
			}

			var name = $(this).find('option:selected').html();

			var o = $('<div class="interest-row" data-interest="'+id+'">'+
				'<strong>'+name+'</strong>'+
				'<input type="hidden" name="interest_'+id+'" value="'+id+'" />'+
				'<i class="fa fa-times remove-interest remove-interest-trigger"></i>'+
			'</div>');

			o.find('.remove-interest-trigger').removeClass('remove-interest-trigger').click( removeInterest );
			o.hide();

			$('.interest-list').prepend(o);
			o.fadeIn();

			$(this).val(0)
			interestsPicked.unshift( parseInt(id) );
		}

		var removeInterest = function() {
			if (interestsPicked.length > 1) {
				var obj = $(this).closest('.interest-row');
				var id = parseInt( obj.data('interest') );
				obj.fadeOut( function() {
					obj.remove();
				});
				var i = interestsPicked.indexOf( id );
				if (i !== -1) {
					interestsPicked.splice(i, 1);
				}
			} else {
				alert('You must have at least one interest. Please add another interest before removing interests.');
			}
		};

		$('.interest-adder select').change( addInterest );
		$('.remove-interest-trigger').removeClass('remove-interest-trigger').click( removeInterest );

	};


	$(document).ready(function() {
		iniSuggestions();
		iniInterests();
		
	});

})();

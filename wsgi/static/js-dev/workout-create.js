
(function() {

	var labelForms = null;
	var labelFormScore = null;
	var labelFormWeight = null;
	var labelFormDistance = null;
	var workoutTypeShowing = 1;

	var scoreHidden = null;
	var weightHidden = null;
	var distanceHidden = null;


	// Using twitter typeahead and twitter bloodhound for suggestion list.
	// http://twitter.github.io/typeahead.js/examples/
	// ---
	// The list of workouts is usually outputted directly into the dom in a SCRIPT
	// tag within the template, and creates a variable called 'workoutTypes'.
	var iniSuggestions = function() {

		if (typeof workoutTypes === 'undefined') {
			console.error( '"workoutTypes" is not defined! Cant create suggestion list...' );
			return false;
		}

		var workouts = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			local: workoutTypes
		});

		workouts.initialize();

		$('#id_name').typeahead({
			hint: true,
			highlight: true,
			minLength: 1
		},
		{
			name: 'workouts',
			displayKey: 'value',
			source: workouts.ttAdapter()

		// if we select something from the list, its always going to be a lift
		}).on('typeahead:selected', function ( e, sel ) {
			if (sel.id >= 80){
				$('#id_workout_info').val(3).change();			
			} else {
				$('#id_workout_info').val(2).change();			
			}
		});
	};


	// Show the coreect label choice depending on the workout type
	var changeableLabels = function () {
		var val = parseInt( $(this).val() );
		workoutTypeShowing = val;
		labelForms.hide();
		if ( val === 1 ) {
			labelFormScore.show();
			labelFormScore.find('select').change();
		} else if ( val === 2 ) {
			labelFormWeight.show();
			labelFormWeight.find('select').change();
		} else if ( val === 3 ) {
			labelFormDistance.show();
			labelFormDistance.find('select').change();
		}
	};
	

	// The elements sent on form submit are hidden so we can
	// programatically assign their values depending on the
	// workout type selected.
	var recordLabelValues = function() {
		// the value of 1 of the 3 select boxes
		var val = parseInt( $(this).val() ); 

		// now we hook up which place to put the info
		if ( workoutTypeShowing === 1 ) {
			scoreHidden.val( val );
			weightHidden.val( 0 );
			distanceHidden.val( 0 );
		} else if ( workoutTypeShowing === 2 ) {
			scoreHidden.val( 0 );
			weightHidden.val( val );
			distanceHidden.val( 0 );
		} else if ( workoutTypeShowing === 3 ) {
			scoreHidden.val( 0 );
			weightHidden.val( 0 );
			distanceHidden.val( val );
		}
		scoreHidden.change();
		weightHidden.change();
		distanceHidden.change();
	};

	
	// Assign and initialize
	$(document).ready( function() {
		labelForms = $('.label-form');
		labelFormScore = $('.score-label');
		labelFormWeight = $('.weight-label');
		labelFormDistance = $('.distance-label');

		scoreHidden = $('#id_score_label');
		weightHidden = $('#id_weight_label');
		distanceHidden = $('#id_distance_label');

		$('#id_workout_info').change(changeableLabels);
		labelFormScore.find('select').change(recordLabelValues);
		labelFormWeight.find('select').change(recordLabelValues);
		labelFormDistance.find('select').change(recordLabelValues);

		labelFormScore.find('select').change();

		iniSuggestions();
	});

})();

(function () {

	
	var openApproved = function() {
		$(this).closest('.approved').find('.approved-list').show();
		$(this).remove();
	};


	var approve = function() {

		var obj = $(this);
		var data = {
			attendee: obj.data('id'),
			approve: true		
		};

		var desc = $(this).closest('.event-description');
		var row = $(this).closest('li');

		$.ajax({
			url: '/events/attendees',
			type: 'post',
			data: data,
			dataType: 'json'
		}).done(function( returned ){
			row.detach();
			obj.hide();
			desc.find('.approved ul').prepend( row ).show();
			desc.find('.approved').show();
			desc.find('.open-approved').hide();
			
			var pc = desc.find('.approved-counter');
			pc.html( parseInt(pc.html()) + 1 );
			var pu = desc.find('.pending-counter');
			pu.html( parseInt(pu.html()) -1 );
		}).fail( function( e ) {
			window.ajaxFail(e);
		});
	};


	var deny = function() {
		var obj = $(this);
		var desc = $(this).closest('.event-description');
		var row = $(this).closest('li');

		var data = {
			attendee: obj.data('id'),
			remove: true		
		};

		$.ajax({
			url: '/events/attendees',
			type: 'post',
			data: data,
			dataType: 'json'
		}).done(function( returned ){
			row.remove();
		}).fail( function( e ) {
			window.ajaxFail(e);
		});
	};


	$(document).ready( function() {
		$('.approve-btn').click(approve);
		$('.remove-btn').click(deny);
		$('.open-approved').click(openApproved)
	});

})();
(function() {

	var feedObj =  null
	var feedBod = null
	var loader = null
	var feedPage = 1;
	var fetchMore = true;
	var loading = false;
	var prefetched = false;
	var schoolfeed = false;
	var nextHeight = 0;
	var subTenDone = {};
	var idslist = [];
	var newestId = 0;

	var fetchPosts = function() {
		if ( loading ) return false;
		if ( ! fetchMore ) return false;
		loader.show();
		loading = true;
		var url = '/feed?page='+feedPage; 
		url += (typeof window.schoolfeed !== 'undefined')? "&all=1" : '';
		if (typeof window.userfeed !== 'undefined') {
			var url = '/feed?page='+feedPage+'&user='+window.userfeed;
		}
		campuselite.serverQuery( url, {
			method: 'get',
			onDone: appendPosts
		});
	};

	var infinitescroll = function() {
		if (nextHeight - feedBod.scrollTop()  <=  50) {
			fetchPosts();
		} 
	};


	var calcNextHeight = function() {
		nextHeight = feedObj.height() + feedObj.offset().top - $(window).height() ;
		if ( nextHeight < 0 ) {
			nextHeight = 0;
		} 
	}


	var appendPosts = function( data ) {
		loader.hide();
		feedObj.append( data );

		feedObj.find('.timeago').removeClass('timeago').timeago();
		campuselite.setupPostEditActions();
		campuselite.fistBumpAssignHandlers();
		campuselite.commentEditActions();
		campuselite.commentAssignHandlers();
		campuselite.photoCorral();
		campuselite.followAssignHandlers();
		
		setTimeout( function() {
			calcNextHeight();
			loading = false;
			prefetched = true;
		}, 500);
		if (feedObj.find('.end-of-posts').length > 0) {
			fetchMore = false;
		}
		if ( ! prefetched ) {
			$(document).on('scroll', infinitescroll);
		}
		feedPage = feedPage + 1;
	};



	var setupFeed = function() {
		feedObj = $('.post-list-feed');
		feedBod = $(window);
		loader = $('.post-feed-loading');
	};



	$(document).ready(function() {
		setupFeed();
		if (feedObj.length > 0) {
			fetchPosts();
		}
	});

})();
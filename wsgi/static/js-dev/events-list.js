
(function() {

	var queryResultsEl = $('.events-filter-output');
	var buddylist = null;
	var buddysel = null;
	var dataCache = null;
	var calcNextHeight = null;
	var fetchMore = true;

	var filterSelects = {
		with: null,
		type: null,
		start: null,
		end: null,
		level: null
	};


	


	var dobuddytoggle = function() {
		if ($(this).val() === 'only') {
			buddylist.show().focus();
		} else {
			buddylist.hide();
		}
	};

	var hideSpinner = function() {
		$('.events-filter-loading').hide();
	};

	var showSpinner = function() {
		$('.events-filter-loading').show();
	};


	var fetchEvents = function( data, newPage, callback ) {
		campuselite.serverQuery( '/events/query', {
			data: data,
			onDone: function( returned ) {
				returned = $(returned);

				if (returned.wrap('<div></div>').parent().find('.end-of-events').length > 0) {
					fetchMore = false;
				}
				campuselite.joinEventAssignHandlers( returned );

				if (typeof newPage === 'undefined') {
					queryResultsEl.html( returned );
				} else {
					queryResultsEl.append( returned );
				}
			},
			afterDone: function() { 
				hideSpinner();
				if (typeof callback === 'function')  {
					callback();
				}
			}
		});
	};


	var runSearch = function() {
		showSpinner();
		var data = {
			with: filterSelects.with.val(),
			type: filterSelects.type.val(),
			start: filterSelects.start.val(),
			end: filterSelects.end.val(),
			level: filterSelects.level.val()
		};

		if (data.with === 'only') {
			dv = buddysel.val();
			if (dv !== null){
				data.buddies = dv.join(',');
			}	
		}

		dataCache = $.extend({}, data);
		fetchMore = true;
		pushStickyState( data, '/events?'+window.campuselite.toQueryString(data) );
		fetchEvents( data, undefined, calcNextHeight );
	};


	var setControls = function( data ) {
		if (typeof data.with !== 'undefined')
			filterSelects.with.val( data.with );
		if (filterSelects.with.val() === 'only') {
			buddylist.show().focus();
		} else {
			buddylist.hide();
		}
		/*if (typeof data.buddies !== 'undefined'){
			buddystr = data.buddies.split(',');
			buddysel.val( buddystr );
		}*/

		if (typeof data.type !== 'undefined')
			filterSelects.type.val( data.type );
		if (typeof data.start !== 'undefined')
			filterSelects.start.val( data.start );
		if (typeof data.end !== 'undefined')
			filterSelects.end.val( data.end );
		if (typeof data.level !== 'undefined')
			filterSelects.level.val( data.level );
	};

	var setupInfiniteScroll = function() {

		var feedObj =  null
		var feedBod = null
		var loader = null
		var feedPage = 1;
		var loading = false;
		var prefetched = false;
		var schoolfeed = false;
		var nextHeight = 0;
		var subTenDone = {};
		var idslist = [];
		var newestId = 0;

		var fetchPosts = function() {
			if ( loading ) return false;
			if ( ! fetchMore ) return false;
			loader.show();
			loading = true;
			if (dataCache !== null) {
				if (typeof dataCache.page !== 'undefined') {
					dataCache.page++;
				} else {
					dataCache.page = 2;
				}

				fetchEvents( dataCache, true, calcNextHeight );
			}
		};

		var infinitescroll = function() {
			if (nextHeight - feedBod.scrollTop()  <=  50) {
				fetchPosts();
			} 
		};


		calcNextHeight = function() {
			nextHeight = feedObj.height() + feedObj.offset().top - $(window).height() ;
			if ( nextHeight < 0 ) {
				nextHeight = 0;
			} 
		}



		feedObj = $('.events-filter-output')
		feedBod = $(window);
		loader = $('.events-filter-loading');

		$(document).on('scroll', infinitescroll);

		calcNextHeight();

	}


	// so HTML5 browsers can change the URL for permalinks 
	// http://stackoverflow.com/questions/824349/modify-the-url-without-reloading-the-page

	if ( window.history.pushState ) {
		var pushStickyState = function ( data, urlPath ){
			window.history.pushState( { "querydata":data }, "", urlPath );
		}

		window.onpopstate = function( e ){
		    if( e.state ){
		    	setControls( e.state.querydata );
		        fetchEvents( e.state.querydata );
		    }
		};
	} else {
		var pushStickyState = function (data, urlPath){ /* empty handler */ }
	}



	$(document).ready(function() {

		buddylist = $('.only-buddies-list');
		buddysel = $('#buddies');

		filterSelects = {
			with: campuselite.INPUT('with', 'select'),
			type: campuselite.INPUT('type', 'select'),
			start: campuselite.INPUT('start', 'select'),
			end: campuselite.INPUT('end', 'select'),
			level: campuselite.INPUT('level', 'select')
		}

		// intial query on load

		if (campuselite.GET('user') !== null) {
			var defaults = {
				user: campuselite.GET('user'),
				past: campuselite.GET('past')
			}
			if (defaults.past === null) defaults.past = 0;
		} else {
			var defaults = {
				with: campuselite.GET('with'),
				type: campuselite.GET('type'),
				start: campuselite.GET('start'),
				end: campuselite.GET('end'),
				level: campuselite.GET('level'),
				buddies: campuselite.GET('buddies')
			};
			if (defaults.with === null) delete defaults.with;
			if (defaults.start === null) delete defaults.start;
			if (defaults.end === null) delete defaults.end;
			if (defaults.type === null) delete defaults.type;
			if (defaults.level === null) delete defaults.level;
			if (defaults.buddies === null) delete defaults.buddies;
		}

		setControls( defaults );

		dataCache = $.extend({}, defaults);

		fetchEvents( defaults, undefined, setupInfiniteScroll );

		// set up handlers for more querying
		$('.event-filter-controls select').on( 'change', runSearch );
		filterSelects.with.on('change', dobuddytoggle);

		buddysel.select2({
			width: '100%',
			height: '44px',
			formatNoMatches: "<strong>No buddies found.</strong><br>Users must mutually motivate each other to be buddies.",
		    placeholder: "Search for buddies (mutual followers)"
		});

	});

})();
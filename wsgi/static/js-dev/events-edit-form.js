
(function() {

	var saving = false;

	var editFormSubmit = function() {

		// no double-submits
		if (saving) return false;
		saving = true;

		// Returns state object that was been created through event-dates component.
		// Assumes campuselite.exportEventDates() exists
		var datesObj = campuselite.exportEventDates();

		var formData = {
			name: campuselite.INPUT('name', 'input').val(),
			category: campuselite.INPUT('category', 'select').val(),
			location: campuselite.INPUT('location', 'input').val(),
			level: campuselite.INPUT('level', 'select').val(),
			max_attendees: campuselite.INPUT('max_attendees', 'input').val(),
			description: campuselite.INPUT('description', 'textarea').val(),
			dates: datesObj
		};

		campuselite.serverQuery( window.location.href, {
			data: formData,
			method: 'post',
			onDone: function( returned ) {
				saving = false;
				window.location = '/events/manage';
			}
		} );

	};

	$(document).ready(function() {
		$('.c-primary-action').click( editFormSubmit );
	});

})();
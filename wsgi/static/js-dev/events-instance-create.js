
(function() {

	var createObj = null;
	var listObj = null;
	var timeEditForm = null;
	var editingTime = false;

	var dtPickerConfig = {
		format:'Y-m-d H:i:s',
		inline:true,
		minDate: 0,
		formatTime: 'g:i a',
		step: 15
	};

	var dtPickerEditConfig = {
		format:'Y-m-d H:i:s',
		minDate: 0,
		formatTime: 'g:i a',
		step: 15
	};

	var state = [];

	var outputStateToPage = function() {
		editingTime = false;

		// string compare times because they are all normalized
		state.sort( function(a, b){
			var diff = a.realtime.diffMinutes( b.realtime );
			if (diff > 0) {
				diff = -1;
			} else if (diff < 0) {
				diff = 1;
			}
			return diff;
		});

		// This is actually removing and re-appending dom elements.
		// Could possibly be sluggish on mobile but perhaps that's not a big deal yet.
		// Keep an eye on it!
		listObj.html('');

		if (state.length > 0) {
			$.each( state, function( key, val ) {
				val.el.attr( 'data-index', key );
				listObj.append( val.el );
			});
		} else {
			listObj.html('<li class="no-scheduled"><strong>You have not scheduled any dates for this event.</strong><br>'+
				'Use the form below to add your first one!</li>');
		}
	};


	// Used so our widget can communicate to other, bigger parts of the form
	// and is able to save the data to the server
	campuselite.exportEventDates = function() {
		outputStateToPage();
		var exportable = [];
		$.each( state, function( key, val ) {
			exportable.push({
				time: val.time,
				notes: val.notes,
				loc: val.loc,
				pk: val.pk
			});
		});

		//console.log( exportable );
		return exportable;
	};


	var pushState = function(etime, loc, notes, pk, updateIndex) {

		if (typeof pk === 'undefined') {
			pk = null;
		} else {
			if (pk !== null) {
				pk = parseInt(pk);
			}
		}
		if (typeof updateIndex === 'undefined') {
			updateIndex = false;
		}

		loc = campuselite.cleanInput( loc );
		notes = campuselite.cleanInput( notes );

		var datetime = new XDate( etime );
		var el = $('<li>'+
				'<div class="cal"><div class="mo">'+datetime.toString('MMM').toUpperCase()+'</div>'+
				'<div class="day">'+datetime.toString('d')+'</div></div>'+
				'<span class="edate">'+datetime.toString('dddd, MMMM d')+' at '+datetime.toString('h:mmTT')+'</span>'+
				((loc!=='')?'<div class="eloc"><strong class="label">Location:</strong> '+loc+'</div>':'')+
				((notes!=='')?'<div class="enotes"><strong class="label">Notes:</strong> '+notes+'</div>'
						:'<div class="enotes no-notes">No notes available</div>')+
				'<a class="edit-date"><i class="fa fa-pencil"></i></a>'+
				'<a class="remove-date"><i class="fa fa-times"></i></a>'+
				'</li>');

		var dataObj = {
			realtime: datetime,
			time: etime,
			notes: notes,
			loc: loc,
			pk: pk,
			el: el
		};

		if (updateIndex !== false) {
			state[ updateIndex ] = dataObj;
		} else {
			state.push( dataObj );
		}
	};


	var showEditTools = function () {
		if (editingTime) {
			// reset everything ... and you lose changes
			outputStateToPage();
		}

		var thisLi = $(this).closest('li');
		var index = parseInt( thisLi.data('index') );
		var stateObj = state[ index ];
		editingTime = true;

		thisLi.replaceWith( timeEditForm );
		setupEditForm( stateObj, function( formData ) {
			// save callback, so we get the index in-scope
			saveEditChanges( stateObj, index, formData );
		} );
	};


	var setupEditForm = function( data, saveCallback ) {
		var obj = listObj.find('.edit-form');
		obj.find('.date-display').html( data.realtime.toString('dddd, MMMM d')+' at '+data.realtime.toString('h:mmTT') );
		obj.find('#id_location').val( data.loc );
		obj.find('textarea').val( data.notes );
		obj.find('#id_datetime').val( data.realtime.toString('yyyy-MM-dd HH:mm:ss') ).datetimepicker( dtPickerEditConfig );

		var sendToSave = function() { 
			saveCallback( {
				time: obj.find('#id_datetime').val(),
				loc: obj.find('#id_location').val(),
				notes: obj.find('textarea').val()
			} );
		};

		obj.find('.edit-date-submit').click( sendToSave );
		obj.find('input, textarea').onEnter( sendToSave );
		obj.find('.edit-date-cancel').click(cancelEditChanges);
	};

	var saveEditChanges = function( stateObj, index, formData ) {
		pushState(formData.time, formData.loc, formData.notes, stateObj.pk, index);
		outputStateToPage();
	};

	var cancelEditChanges = function() {
		outputStateToPage();
	};


	var addTimeToList = function() {
		// if it's there... we dont need this
		$('.add-event-helper').remove();

		var loc = createObj.find('.picker-location').val();
		var notes = createObj.find('.picker-notes').val();
		var etime = createObj.find('.picker-time').val();
		
		pushState(etime, loc, notes);

		outputStateToPage();

		$('.xdsoft_today_button').trigger('mousedown.xdsoft');
		createObj.find('.picker-location').val('');
		createObj.find('.picker-notes').val('');
	};


	// Takes existing html (usually on edit version of page) and converts it to state format
	var collectCurrentTimes = function() {
		listObj.find('li').each(function() {
			pushState( 
				$(this).find('.edate').data('stamp'), 
				$(this).find('.eloc').html(), 
				$(this).find('.enotes').html(),
				$(this).data('id')
			);
		});
		outputStateToPage();
	};

	var removeTimeFromList = function() {

		var doRemoval = function ( obj ) {
			var index = parseInt( obj.closest('li').data('index') );
			delete state[ index ]; // creates undefined element
			state.splice( index, 1 ); // changes the length of array
			outputStateToPage();
		};

		if ( typeof campuselite.checkBeforeRemove !== 'undefined' ) {
			if (confirm( 'Are you sure you want to remove this scheduled date? Everyone who'+
				' is currently attending will be notified of cancellation after you save changes.' )) {
				doRemoval( $(this) );
			}
		} else {
			doRemoval( $(this) );
		}
		
	};


	$(document).ready(function() {
		createObj = $('.c-date-create-form');
		listObj = $('.time-list');
		createObj.find('.submitter button').click( addTimeToList );
		listObj.on('click', 'a.remove-date', removeTimeFromList );
		listObj.on('click', 'a.edit-date', showEditTools );
		timeEditForm = $('.edit-form-template').html();

		collectCurrentTimes();

		var d = new XDate()
		createObj.find('.picker-time').val( d.toString('yyyy-MM-dd HH:mm:ss') ).datetimepicker( dtPickerConfig );

	});

})();
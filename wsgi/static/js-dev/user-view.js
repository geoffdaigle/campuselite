(function() {
	var photoForm = $("#photo_upload");
	var photoNameToSave = null;
	var canCheck = false;
	var imupobj = null;
	var startVal = null;
	var checkTO = null;

	var acceptImageUpdate = function() {
		var src = photoForm.find('.prev-image').data('filename');
		campuselite.serverQuery('/update-image', {
			data: { image: photoNameToSave },
			dataType: 'json',
			onDone: function( returned ){
				$('.avatar').attr('src', window.AppConfig.staticRoot+src);
				photoForm.find('.modal_close').click();
			}
		});
	};

	var setUpPhotoForm = function() {
		canCheck = true;
		$('.upload-step input').val('');
		startVal = imupobj.val();
		checkTO = setInterval( checkForChange, 70 );
	};
	
	var resetPhotoFormAgain = function() {
		canCheck = true;
		resetPhotoForm();
	}

	var resetPhotoFormClear = function() {
		clearTimeout( checkTO );
		canCheck = false;
		resetPhotoForm();
	}
	
	var resetPhotoForm = function() {
		photoForm.find('.upload-step').show();
		photoForm.find('.review-step').hide();
		$('.upload-step input').val('');
		startVal = imupobj.val();
	};

	var doPhotoUpload = function( returned, status ) {
		if (status === 200 && returned.imageToSave !== false ) {
			photoNameToSave = returned.imageToSave;
			photoForm.find('.prev-image').attr('src', window.AppConfig.staticRoot+returned.image);
			photoForm.find('.prev-image').data('filename', returned.image);
			photoForm.find('.upload-step').hide();
			photoForm.find('.review-step').show();
		} else {
			alert('Something went wrong with your photo upload. Was the file a JPG, PNG, or GIF?');
			resetPhotoForm();
			canCheck = true;
		}
	};

	var checkForChange = function() {
		if ( canCheck ) {
			var nowVal = imupobj.val();
			if (startVal !== nowVal) {
				canCheck = false;
				$('#image_upload_form').submit();
			}
		}
	};

	$(document).ready( function() {
		var progressBar = $('.progress-bar span');
		$(".public_url").leanModal( {closeButton: ".modal_close"} );
		$(".photo_upload").leanModal( {
			onOpen: setUpPhotoForm,
			onClose: resetPhotoFormClear,
			closeButton: ".modal_close"
		} );
		$('#public_url input').clickSelect();

		$('#image_upload_form').ajaxForm({
			beforeSend: function() {
				progressBar.html('0%');
				progressBar.parent().show();
		    },
		    uploadProgress: function( event, position, total, percentComplete ) {
		    	if ( percentComplete < 100 ){
			        progressBar.html( String( percentComplete )+"%" );
			    } else {
		        	progressBar.html( '100%<br>Rendering preview...' );
			    }
		    },
		    success: function() {
		        progressBar.html( '100%<br>Rendering preview...' );
		    },
			complete: function(xhr) {
				progressBar.parent().hide();
		    	doPhotoUpload( $.parseJSON(xhr.responseText), xhr.status )
		    } 
		});

		imupobj = $('#image_upload_form input');

		$('.image-accept').click( acceptImageUpdate );
		$('.image-cancel').click( resetPhotoFormAgain );
	});
})();
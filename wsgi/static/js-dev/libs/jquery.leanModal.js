/**
 *   LeanModal 2
 *   Geoff Daigle - 2014 - @dailydaigle
 *
 *   Based on http://leanmodal.finelysliced.com.au/
 *
 *   Uses a more functional approach to the architecture,
 *   plus options for callbacks and an additional, direct-control 
 *   jQuery function.
 *
 *   leanModalCtrl - Returns a controller object with open, close, centerVertical, and refresh methods
 *                   so you can do these things without using a trigger element.
 */

 /*
    // Background overlay css
    #lean_overlay {
        position: fixed;
        z-index:100;
        top: 0px;
        left: 0px;
        height:100%;
        width:100%;
        background: #000;
        display: none;
    }
 */

(function($){

    var leanOverlay = null;

    // ** modify all defaults here **
    var setOptions = function( options ){
        var defaults = {
            top: 0,
            overlay: 0.5,
            closeButton: null,
            onOpen: function(){},
            onClose: function(){}
        }
        
        return $.extend(defaults, options);
    };


    // Puts overlay element into dom only once
    var appendOverlay = function() {
        var overlay = $('#lean_overlay');
        if (overlay.length < 1) {
            var overlay = $("<div id='lean_overlay'></div>");
            $("body").append(overlay);
        }
        leanOverlay = overlay
    }; 


    // hides modal then calls onClose option
    var closeModal = function (modalId, options){
        leanOverlay.fadeOut(200);
        $(modalId).css({ 'display' : 'none' });
        if ( typeof options.onClose === 'function' ) {
            options.onClose();
        }
    };


    // centers the modal vertically on the screen
    var centerVertical = function( element ){
        var w = $(window);
        var e = $(element);
        parentHeight = w.height();  
        elementHeight = e.height();
        e.css('top', parentHeight/2 - elementHeight/2);
    };


    // Places modal in screen position based on options.
    // This is called every time modal is opened.
    // This is NOT called when screen is resized.
    var reposition = function( modalId, options ) {
        var modalHeight = $(modalId).outerHeight();
        var modalWidth = $(modalId).outerWidth();

        $(modalId).css({ 
            'display' : 'block',
            'position' : 'fixed',
            'z-index': 11000,
            'left' : 50 + '%',
            'margin-left' : -(modalWidth/2) + "px",
            'top' : options.top + "px"
        
        });
    };


    // shows selected modal then calls onOpen method
    var openModal = function( modalId, options, e ) {
        var modalObj = $(modalId);

        var closeaction = function() { 
            closeModal(modalId, options);                    
        };

        leanOverlay.off('click', closeaction).on('click', closeaction);
        $(options.closeButton).off('click', closeaction).on('click', closeaction);
        
        leanOverlay.css({ 'display' : 'block', opacity : 0 });
        modalObj.css({'opacity' : 0});

        reposition( modalId, options ); 

        leanOverlay.fadeTo(200, options.overlay);
        modalObj.fadeTo(200,1);

        if (typeof e !== 'undefined') {
            e.preventDefault();
        }

        if ( typeof options.onOpen === 'function' ) {
            options.onOpen();
        }
    };



    // Direct controller version - no trigger needed.
    // Returns an object which uses methods to open/close.
    var leanModalCtrl = function( options ) {
        appendOverlay();
        options = setOptions( options );
        
        var obj = this;
        var oid = '#'+obj.attr('id');

        return {
            open: function() {
                openModal( oid, options );
            },
            close: function() {
                closeModal( oid, options );
            },
            centerVertical: function() {
                centerVertical( obj );
            },
            refresh: function() {
                reposition( oid, options ); 
            }
        }

    };

    
    // Functionality of leanModal 1 - uses trigger element
    // and opens the modal on click based on trigger's href.
    var leanModal = function(options) {
        appendOverlay();
        if ( typeof options.top === 'undefined' ) {
            options.top = 100;
        }
        options = setOptions( options );

        this.each(function() {           
            $(this).click(function(e) {
                var modalId = $(this).attr("href");
                openModal( modalId, options, e )
            });
        });

        return {
            close: function() { 
                this.each(function() {
                    var modalId = $(this).attr("href");
                    closeModal(modalId) 
                })
            }
        }

    };
 

    // extend jquery
    $.fn.extend({ 
        leanModal: leanModal,
        leanModalCtrl: leanModalCtrl
    });
     
})(jQuery);
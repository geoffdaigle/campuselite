
( function() {

	var lastUsernameChecked = '';
	var originalUsername = '';
	var checkingUsername = false;

	var lastEmailChecked = '';
	var originalEmail = '';
	var checkingEmail = false;

	var usernameIsValid = true;
	var emailIsValid = true;

	var csUser = null;
	var csEmail = null;
	var vu = null;
	var ve = null;

	var slugify = function ( str ) {
		if ( str.length > 30 ){
			str = str.substring(0,30);
		}
	    return $.trim( str.replace( /[`~!@#$%^&*()|+=?;:'",<>\{\}\[\]\\\/]/gi, '' ) );
	}

	var checkUsername = function( e ) {
		var obj = $(this);
		var username = obj.val();

		if (typeof e.keyCode !== 'undefined') {
			var k = parseInt(e.keyCode);
			// ignore backspace and arrow keys
			if ( ( k < 37 || k > 40 ) && k !== 8) {
				var nv = slugify( obj.val() );
				if ( nv !== username ) {
					obj.val( nv );
					username = nv;
				}
			}
		}

		if (username === originalUsername) {
			csUser.hide();
			usernameIsValid = true;
		} else {
			clearTimeout( vu );
			vu = setTimeout( function() {
				usernameIsValid = false;
				if ( username === '' ) {
					csUser.hide();
					usernameIsValid = true;
				} else {
					if ( username !== lastUsernameChecked && ! checkingUsername ) {
						checkingUsername = true;
						csUser.find('.success, .error').hide();
						csUser.show();
						csUser.find('.loading').show();
						campuselite.serverQuery('/check-username', {
							data: { username: username },
							dataType: 'json',
							onDone: function( returned ){
								checkingUsername = false;
								csUser.find('.loading').hide();
								if (returned.available) {
									csUser.find('.success').show();
									usernameIsValid = true;
								} else {
									csUser.find('.error').show();
								}
								lastUsernameChecked = username;
							}
						});
					}
				}
			}, 500);
		}
	};


	var checkEmail = function() {
		var obj = $(this);
		var email = obj.val();
		if (email === originalEmail) {
			csEmail.find('.success, .error, .loading, .notvalid').hide();
			csEmail.hide();
			emailIsValid = true;
			ve = setTimeout( function() {
				csEmail.hide();
				emailIsValid = true;
			}, 501);
		} else {
			clearTimeout( ve );
			ve = setTimeout( function() {
				emailIsValid = false;
				if ( ! campuselite.validEmail(email) ) {
						csEmail.find('.success, .error, .loading').hide();
						csEmail.find('.notvalid').show();
						csEmail.show();
				} else {
					if ( email !== lastEmailChecked && ! checkingEmail ) {
						checkingEmail = true;
						csEmail.find('.success, .error, .notvalid').hide();
						csEmail.show();
						csEmail.find('.loading').show();
						campuselite.serverQuery('/check-email', {
							data: { email: email },
							dataType: 'json',
							onDone: function( returned ){
								checkingEmail = false;
								csEmail.find('.loading').hide();
								if (returned.available) {
									csEmail.find('.success').show();
									emailIsValid = true;
								} else {
									csEmail.find('.error').show();
								}
								lastEmailChecked = email;
							}
						});
					}
				}

			}, 500);
		}
	};


	var formCheck = function( form ) {	
		csEmail.hide();
		if ( usernameIsValid && emailIsValid ) {
			form.submit();
		} else {
			if ( ! usernameIsValid ) {
				$('#id_username').focus();
			} else if ( ! emailIsValid ) {
				$('#id_email').focus();
			}
		}
	};



	$(document).ready(function() {

		csUser = $('.check-state.username');
		csEmail = $('.check-state.email');

		$('#id_username').keyup( checkUsername ).change( checkUsername )
			.attr("autocomplete","off");

		$('#id_email').keyup( checkEmail ).change( checkEmail )
			.attr("autocomplete","off");

		lastUsernameChecked = $('#id_username').val();
		originalUsername = $('#id_username').val();

		lastEmailChecked = $('#id_email').val();
		originalEmail = $('#id_email').val();

		$('form').validate({
			debug: true,
			submitHandler: formCheck,
			onfocusout: false,
			onkeyup: false,
			errorElement:"div",
			errorClass: "validate-error",
			ignore: "#id_email",
			rules: {
				first_name: {
					required: true
				},
				last_name: {
					required: true
				}
			}
		});

	});
	

})();

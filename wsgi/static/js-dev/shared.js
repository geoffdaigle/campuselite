

// Global campuselite utility object
window.campuselite = {
	templates: {} // assumes precompiled
};


(function() {


	/**
	 * -----------------------------------------------------------------------------------
	 *	INTERNAL FUNCTIONS
	 * -----------------------------------------------------------------------------------
	 */

	// Pad a string
	// from http://stackoverflow.com/questions/2686855/is-there-a-javascript-function-that-can-pad-a-string-to-get-to-a-determined-leng
	var STR_PAD_LEFT = 1;
	var STR_PAD_RIGHT = 2;
	var STR_PAD_BOTH = 3;

	function pad(str, len, pad, dir) {

	    if (typeof(len) == "undefined") { var len = 0; }
	    if (typeof(pad) == "undefined") { var pad = ' '; }
	    if (typeof(dir) == "undefined") { var dir = STR_PAD_RIGHT; }

	    if (len + 1 >= str.length) {
	        switch (dir){
	            case STR_PAD_LEFT:
	                str = Array(len + 1 - str.length).join(pad) + str;
	            break;

	            case STR_PAD_BOTH:
	                var right = Math.ceil((padlen = len - str.length) / 2);
	                var left = padlen - right;
	                str = Array(left+1).join(pad) + str + Array(right+1).join(pad);
	            break;

	            default:
	                str = str + Array(len + 1 - str.length).join(pad);
	            break;

	        } // switch
	    }
	    return str;
	}


	/**
	 * -----------------------------------------------------------------------------------
	 *	GLOBAL UTILITY FUNCTIONS
	 * -----------------------------------------------------------------------------------
	 */

	/**
	 *  Detect enter press for forms
	 *	http://stackoverflow.com/questions/6524288/jquery-event-for-user-pressing-enter-in-a-textbox
	 */
	$.fn.onEnter = function(fn) {  
		return this.each(function() {  
			$(this).bind('enterPress', fn);
			$(this).keyup(function(e){
				if(e.keyCode == 13) {
					$(this).trigger("enterPress");
				}
			})
		});  
	}; 



	/**
	 *  Do an input click+selectall simpultaneously 
	 *	http://stackoverflow.com/questions/5797539/jquery-select-all-text-from-a-textarea
	 */
	$.fn.clickSelect = function (fn) {
		return this.focus(function() {
		    var $this = $(this);
		    $this.select();

		    // Work around Chrome's little problem
		    $this.mouseup(function() {
		        // Prevent further mouseup intervention
		        $this.unbind("mouseup");
		        return false;
		    });
		});
	}


	/**
	 *	Typical ajax fail response
	 */
	window.ajaxFail = function( e ) {
		console.error( 'Error ('+e.status+'): '+e.statusText );
		alert('Hm... something went wong on our end. Please try again, or contact support so we can fix it!');
	};


	/**
	 *	Looks for 'timeago' class and applies plugin
	 */
	var setupTimeago = function() {
  		$(".timeago").each(function() {
  			$(this).removeClass('timeago').timeago();
  		});
	};


	/**
	 *	Utility to create a dropdown menu
	 */
	var createDropdown = function( sel, dropdownSel, activeClass, onOpen, onClose ) {
		// group item
		var obj = $(sel);

		// alternative class to trigger the active toggle
		if ( typeof activeClass === 'undefined' ) {
			var act = obj
		} else {
			var act = $(activeClass);
		}

		act.addClass('ce-dropdown');

		// click the screen and open dropdown it closes
		$(document.body).click( function() {
			act.removeClass('active');
		});

		// open/close dropdown when button is clicked
		obj.click( function( e ) {
	 		e.stopPropagation();
	 		// hide other active dropdowns 
	 		var opening = false;
	 		if ( ! act.hasClass('active') ) {
	 			$('.ce-dropdown').removeClass('active');
	 			opening = true;
	 		}
	 		act.toggleClass('active');

	 		if (opening) {
	 			if ( typeof onOpen === 'function' ) {
	 				onOpen();
	 			}
	 		} else {
	 			// only fires on direct-click close... this isn't too useful yet
	 			if ( typeof onOpen === 'function' ) {
	 				onClose();
	 			}
	 		}
	 	});

	 	// if you click the dropdown, prevent auto-close
		act.find(dropdownSel).click( function( e ) {
	 		e.stopPropagation();
	 	});
	};


	/**
	 *	Dropdown menu behavior for top navigation
	 *  NOTE: reuse this code for other stuff... it's handy and simple
	 */
	var setupNavigation = function() {
		createDropdown( '.menu-dropdown', 'ul' );
	};



	/**
	 *	Dropdown menu and scroll behavior for notifications
	 */
	var setupNotifications = function() {

		var notifWrap = $('.notifications');
		var notifObj = $('.notifications-list');
		var alertCounter = $('.notif-alert-counter');
		var notifBod = notifObj.find('.notifs-body')
		var notifPage = 1;
		var fetchMore = true;
		var loading = false;
		var prefetched = false;
		var nextHeight = 0;
		var subTenDone = {};
		var idslist = [];
		var newestId = 0;

		var fetchNotifications = function() {
			if ( loading ) return false;
			if ( ! fetchMore ) return false;
			notifObj.find('.notifs-loading').show();
			loading = true;
			var url = '/notifications/list?page='+notifPage; 
			url += (! prefetched)? "&prefetch=1" : '';
			campuselite.serverQuery( url, {
				method: 'get',
				onDone: appendNotifs, 
				onFail: function() { console.log('notifications query failed'); }
			});
		};

		var infinitescroll = function() {
			if (nextHeight - notifBod.scrollTop()  <=  50) {
				fetchNotifications();
			} 
		};


		var calcNextHeight = function() {
			var invisibletrick = false;
			if ( ! notifWrap.hasClass('active') ) { 
				invisibletrick = true;
				notifObj.css({visibility:'hidden'});
				notifWrap.addClass('active');
			}
			
			nextHeight = notifObj.find('.notifs-body ul').height() - notifObj.find('.notifs-body').height();
			if ( nextHeight < 0 ) {
				nextHeight = 0;
			} 

			if ( invisibletrick ) { 
				notifObj.css({visibility:'visible'});
				notifWrap.removeClass('active');
			}
		}

		var prependNotifs = function( data ) {
			notifObj.find('.notifs-body ul').prepend( data );
			notifObj.find('.timeago').removeClass('timeago').timeago();
			calcNextHeight();
		}

		var appendNotifs = function( data ) {
			notifObj.find('.notifs-loading').hide();
			notifObj.find('.notifs-body ul').append( data );
			notifObj.find('.timeago').removeClass('timeago').timeago();
			calcNextHeight();
			if (notifObj.find('.end-of-notifs').length > 0) {
				fetchMore = false;
			}
			if ( ! prefetched ) { 
				newestId = notifBod.find('.notif-instance').first().data('nid');
				notifObj.find('.notifs-body').on('scroll', infinitescroll);
			} else {
				doubleCheckShown();
			}
			notifPage = notifPage + 1;
			loading = false;
			prefetched = true;
		};

		var updateCounter = function( num ) {
			var num = parseInt(num);
			if ( num < 1 ) {
				alertCounter.hide();
			} else {
				alertCounter.html( num );
				alertCounter.show();
			}

		}

		var doubleCheckShown = function() {
			notifObj.find('.notif-instance').each( function() {
				var nid = $(this).data('nid');
				if (idslist.indexOf(nid) === -1 ){
					idslist.push( nid );
				}
			});
			campuselite.serverQuery( '/notifications/shown', {
				method: 'post',
				data: { "n": idslist },
				dataType: 'json',
				onDone: function( json ) { 
					updateCounter( json.left );
				},
				onFail: function() { console.log('notifications query failed'); }
			});
		};

		var checkForNew = function() {
			campuselite.serverQuery( '/notifications/check?gt='+newestId, {
				method: 'get',
				onDone: function( returned ) { 
					if ( notifWrap.hasClass('active') ) return false;
					if ($.trim(returned) === '0') return false;
					html = $(returned);
					newestId = html.first().data('nid');
					var num = parseInt( alertCounter.html() );
					addNew = html.length;
					updateCounter( num + addNew );
					prependNotifs( html );
				},
				onFail: function() { console.log('notifications query failed'); }
			});
		};

		var prefetch = function() {
			fetchNotifications();	
		};

		var afterOpen = function() {
			doubleCheckShown();
		};

		var afterClose = function() {
			// not used yet
		};

		createDropdown( '.notifications a', '.notifications-list', '.notifications',
			afterOpen, afterClose );

		prefetch();

		setInterval( function() {
			checkForNew();
		}, 5000)
		
	};


	/**
	 *	Dropdown menu for post list items
	 */
	campuselite.setupPostEditActions = function() {

		var editPostItem = function( pid ) {
			var editobj = $('.c-post-'+pid);
			var cap = editobj.find('.caption');
			var capOrig = cap.html();
			var regex = /<br\s*[\/]?>/gi;
			var capTxt =  $.trim(capOrig).replace(regex, "\n");

			cap.html('<textarea style="margin-bottom: 6.5px; width:100%; height: 80px; padding: 6.5px; '+
					'box-sizing: boder-box; -webkit-box-sizing: border-box; -moz-box-sizing: border-box">'
					+capTxt+'</textarea>'+
					'<div style="text-align:right"><button class="save c-primary-action">'+
					'<i class="fa fa-check"></i> Save changes</button>'+
					'&nbsp;&nbsp;<button class="cancel"><i class="fa fa-times"></i>&nbsp;&nbsp;Discard</button></div>');
			$('body').click();
			cap.find('textarea').focus();

			
			cap.find('.cancel').click( function() {
				cap.html( capOrig );
			});

			cap.find('.save').click( function() {
				var caption = campuselite.cleanInput( $.trim( cap.find('textarea').val().replace(/^\s*[\r\n]/gm, "\n") ) );
				
				if (caption === ''){
					cap.html( capOrig );
					return false;
				} 

				campuselite.serverQuery('/post/edit/'+pid, {
					method: 'post',
					data: {caption: caption},
					dataType: 'json',
					onDone: function( returned ) {
						caption = caption.replace(/\n/gi, "<br>");
						cap.html( caption );
					}
				});
			});
		};

		var deletePostItem = function( pid, o ) {
			if (confirm('Are you sure you want to delete this post? -- Photos wil not be deleted by removing this post. All comments and fist bumps will be removed. This action cannont be undone.')){
				o.replaceWith('<a style="color: #333">Deleting...</a>');
				campuselite.serverQuery('/post/delete/'+pid, {
					method: 'get',
					dataType: 'json',
					onDone: function( returned ) {
						$('body').click();
						$('.c-post-'+pid).fadeOut(function() {
							$(this).parent('li').remove();
						});
					}
				});
			}
		};

		var flagPostItem = function( pid, o ) {
			o.replaceWith('<a style="color: #333">Sending...</a>');
			campuselite.serverQuery('/post/flag/'+pid, {
				method: 'get',
				dataType: 'json',
				onDone: function( returned ) {
					$('body').click();
					alert('Thanks! We have recieved your notification and will review this post as soon as possible.');
				}
			});
		};

		$('.post-actions-edit.triggerable').each(function() {
			var obj = $(this);
			obj.removeClass('triggerable');
			createDropdown( obj.find('.a-trigger'), obj.find('.action-list'), obj.find('.action-list') );
			obj.find('.action-list a').click( function( e ) {
				e.stopPropagation();
				var o = $(this);
				var pid = o.data('pid');
				if (o.hasClass('edit-post')) {
					editPostItem( pid );
				} else if (o.hasClass('delete-post')) {
					deletePostItem( pid, o );
				} else if (o.hasClass('flag-post')) {
					flagPostItem( pid, o );
				}
			});
		});
	};



	/**
	 *  Strips out html and trims a string
	 */
	campuselite.cleanInput = function( data ) {
		function htmlEntities(str) {
		    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
		}
	    return $.trim( htmlEntities(data).replace(/<(?:.|\n)*?>/gm, '') );
	};


	/**
	 *  Validates email address
	 *  http://stackoverflow.com/questions/2507030/email-validation-using-jquery
	 */
	campuselite.validEmail = function (email) {
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regex.test(email);
	}


	/**
	 *  Get query params by name or return null
	 *	http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
	 */
	campuselite.GET = function( qname ) {
	    qname = qname.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + qname + "=([^&#]*)"),
	    	results = regex.exec( window.location.search );
	    return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
	}


	/**
	 *  Takes a flat object (no children) and created a query string from it
	 *	http://stackoverflow.com/questions/1714786/querystring-encoding-of-a-javascript-object
	 */
	campuselite.toQueryString = function( obj ) {
		var str = [];
		for( var p in obj ) {
			if ( obj.hasOwnProperty(p) ) {
				str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			}
		}
		return str.join("&");
	};


	/**
	 *  Get an input element by name, to keep the code clean
	 */
	campuselite.INPUT = function( name, elType, context ) {
		if ( typeof elType === 'undefined' ) {
			elType = '';
	    } else {
	    	if ( $.inArray( $.trim(elType) , ['input', 'select', 'textarea'] ) === -1 ) {
				elType = '';
	    	}
	    }

	    if ( typeof context === 'undefined' ) {
	    	return $( $.trim( elType+'[name="'+name+'"]' ) );
	    } else {
	    	return $( $.trim( context + ' '+elType+'[name="'+name+'"]' ) );
	    }
	}


	/**
	 *	Shortcut to fetch html with POST and paste it into the dom
	 */
	campuselite.serverQuery = function( url, options ) {

		var opts = $.extend( {
			data: {},
			method: 'post',
			element: 'body',
			replace: true,
			prepend: false,
			onDone: null, // overrides the auto-paste process
			afterDone: null,
			onFail: null,
			dataType: 'html'
		}, options );

		$.ajax({
			url: url,
			data: opts.data,
			type: opts.method,
			dataType: opts.dataType
		}).done( function( returned ) {

			if ( typeof opts.onDone === 'function' ) {
				// manual override of success function
				opts.onDone( returned );
			} else {

				// default behavior
				var selectEl = $( opts.element );
				if ( opts.replace ) {
					selectEl.html( returned );
				} else {
					if ( opts.prepend ) {
						selectEl.prepend( returned );
					} else {
						selectEl.append( returned );
					}
				}
			}

			if ( typeof opts.afterDone === 'function' ) {
				// post-paste function
				opts.afterDone( returned );
			}

		}).fail( function( jqXHR ) {
			if ( typeof opts.onFail === 'function' ) {
				opts.onFail( jqXHR );
			} else {
				console.error('campuselite.serverQuery failed!')
				ajaxFail(jqXHR);
			}
		} );

	};



	/**
	 *  Handles the follow/unfollow mechanism
	 */
	campuselite.followAssignHandlers = function( context ) {

		var doingFollow = false;

		var doFollow = function() {

			if ( doingFollow ) { 
				return false;
			}

			doingFollow = true;

			var obj = $(this);
			var followData = {
				user_id: obj.data('uid')
			};

			var unfollowed = false;
			if ( obj.hasClass( 'followed' ) ) {
				followData.remove = true;
				unfollowed = true;
			}

			

			$.ajax({
				url: window.AppConfig.apiRoute+'users/'+window.AppConfig.uid+'/follows',
				type: 'post',
				data: followData,
				dataType: 'json'
			}).done(function( returned ){
				if (obj.hasClass('refresh')) {
					location.reload();
				} else {
					doingFollow = false;
					if (unfollowed) {
						obj.removeClass( 'followed' );
						obj.find('span').html('Motivate');
						obj.find('i').removeClass('fa-minus-circle').addClass('fa-plus-circle');
					} else {
						obj.addClass( 'followed' );
						obj.find('span').html('Motivating');
						obj.find('i').removeClass('fa-plus-circle').addClass('fa-minus-circle');
					}
				}
			}).fail( function( e ) {
				ajaxFail(e);
			});
		};

		if (typeof context !== 'undefined') {
			// context must be a jquery object
			context.find('.c-follow-action').click( doFollow );
		} else {
			$('.c-follow-action').click( doFollow );
		}
	};



	/**
	 *  Handles the follow/unfollow diets mechanism
	 */
	campuselite.followDietAssignHandlers = function( context ) {

		var doingFollow = false;

		var doFollow = function() {

			if ( doingFollow ) { 
				return false;
			}

			doingFollow = true;

			var obj = $(this);
			var followData = {
				user: obj.data('user'),
				follow: 1
			};

			var unfollowed = false;
			if ( obj.hasClass( 'followed' ) ) {
				followData.follow = 0;
				unfollowed = true
			}

			

			$.ajax({
				url: '/diets/follow',
				type: 'post',
				data: followData,
				dataType: 'json'
			}).done(function( returned ){
				if (obj.hasClass('refresh')) {
					location.reload();
				} else {
					doingFollow = false;
					if (unfollowed) {
						obj.removeClass( 'followed' );
						obj.html('Follow this diet');
					} else {
						obj.addClass( 'followed' );
						obj.html('Following diet');
					}
				}
			}).fail( function( e ) {
				ajaxFail(e);
			});
		};

		if (typeof context !== 'undefined') {
			// context must be a jquery object
			context.find('.follow-diet').click( doFollow );
		} else {
			$('.follow-diet').click( doFollow );
		}
	};



	/**
	 *  Handles the request-to-join, unrequest to join
	 */
	campuselite.joinEventAssignHandlers = function( context ) {

		var doRequest = function() {

			var obj = $(this);

			if (obj.hasClass('join-trigger') ) {
				alert('join-trigger');
				return false;
			}

			var actionData = {
				group_id: obj.data('event'),
				user_id: window.AppConfig.uid
			};

			var retracted = false;
			if ( obj.hasClass( 'pending' ) || obj.hasClass( 'attending' ) ) {
				actionData.attend_id = obj.data('attendid');
				actionData.remove = true;
				retracted = true;
			}

			var doAjax = function() {
				$.ajax({
					url: window.AppConfig.apiRoute+'groups/attendees',
					type: 'post',
					data: actionData,
					dataType: 'json'
				}).done(function( returned ){
					if (retracted) {
						obj.removeClass( 'pending' ).removeClass( 'attending' );
						obj.find('span').html('Request to attend');
						obj.find('i').removeClass('fa-check').removeClass('fa-clock-o').addClass('fa-plus');
					} else {
						obj.addClass( 'pending' );
						obj.attr('data-attendid', returned.id);
						obj.find('span').html('Request sent');
						obj.find('i').removeClass('fa-plus').addClass('fa-clock-o');
					}
				}).fail( function( e ) {
					ajaxFail(e);
				});
			};

			if (obj.hasClass( 'attending' )) {
				if(confirm('Are you sure you want to leave this event? You will have to request to attend again to rejoin.')) {
					doAjax();
				}
			} else {
				doAjax();
			}
		};


		if (typeof context !== 'undefined') {
			// context must be a jquery object
			context.find('.c-request-join-btn').click( doRequest );
		} else {
			$('.c-request-join-btn').click( doRequest );
		}
	};




	/**
	 *  Fist bump action
	 */
	campuselite.fistBumpAssignHandlers = function( context ) {

		var doBump = function() {

			var obj = $(this);
			var actionData = {
				post_id: obj.data('post')
			};

			var retracted = false;
			if ( obj.hasClass( 'bumped' )) {
				actionData.remove = true;
				retracted = true;
			}

			var actionsobj = obj.closest('.post-actions');
			var likesobj = actionsobj.find('.post-likes');
			var likesnum = parseInt( likesobj.data('num') );

			$.ajax({
				url: window.AppConfig.apiRoute+'posts/like',
				type: 'post',
				data: actionData,
				dataType: 'json'
			}).done(function( returned ){
				if (retracted) {
					obj.removeClass( 'bumped' );
					obj.html('<img style="height: 1em" src="'+window.AppConfig.staticRoot+'image/bump.svg"> Fist Bump');
					actionsobj.find('.you-bump').remove();
					var likecount =  likesnum - 1;
					likesobj.data('num', likecount);
					if ( likecount  === 0 ) {
						likesobj.hide();
					}
				} else {
					obj.addClass( 'bumped' );
					obj.html('Bumped');
					if (likesnum === 0) {
						actionsobj.find('.users-in-group-desc').html('<span class="you-bump">You gave a fist bump.</span>');
					} else {
						actionsobj.find('.users-in-group-desc').prepend('<span class="you-bump">You, </span>');
					}
					var likecount =  likesnum + 1;
					likesobj.data('num', likecount);
					likesobj.show();
				}
			}).fail( function( e ) {
				ajaxFail(e);
			});

		};


		if (typeof context !== 'undefined') {
			// context must be a jquery object
			context.find('.fist-bump.no-handler').removeClass('no-handler').click( doBump );
		} else {
			$('.fist-bump.no-handler').removeClass('no-handler').click( doBump );
		}
	};


	
	/**
	 *  Commenting edit/delete
	 */
	campuselite.commentEditActions = function() {
		// currently not in use - might be in the future though
		$('.comment-edit').removeClass('comment-edit').click(function() {
			var cid = $(this).data('comment');
			var li = $(this).closest('li');
			var commentBox = li.find('.comment-content .comment');
			var commentOrig = commentBox.html();

		});

		// delete comment
		$('.comment-delete').removeClass('comment-delete').click(function() {
			var cid = $(this).data('comment');
			var li = $(this).closest('li');
			if (confirm('Are you sure you want to delete this comment? This action cannont be undone.')){
				campuselite.serverQuery('/comment/delete/'+cid, {
					method: 'get',
					dataType: 'json',
					onDone: function( returned ) {
						li.fadeOut(function() {
							li.remove();
						});
					}
				});
			}
		});
	};


	/**
	 *  Commenting action
	 */
	campuselite.commentAssignHandlers = function( context ) {

		var doComment = function() {

			var obj = $(this);
			var actionData = {
				post_id: obj.data('post'),
				comment: campuselite.cleanInput( obj.val() )
			};

			var actionsobj = obj.closest('.post-actions');
			var commentDisplay = actionsobj.find('.post-comments');

			var insertIntoDom = function( commentId ) {
				var d = new XDate()

				var context = {
					id: commentId,
					avatar: window.AppConfig.avatarThumb,
					author: window.AppConfig.displayName,
					comment: actionData.comment,
					datetime: $.timeago(new Date()),
					datetimeStamp: d.toString('u'),
					editable: true
				}

				obj.val('');
				var template = campuselite.templates.comment.render( context );
				template = $(template);
				template.css('display', 'none');

				var commentCount =  parseInt( commentDisplay.data('num') ) + 1;
				commentDisplay.data('num', commentCount);
				commentDisplay.find('.comment-num').html(commentCount);
				commentDisplay.find('.comment-lang').html( ( commentCount!==1 )? 'comments' : 'comment' );
				commentDisplay.find('.c-comment-list ul').append( template );
				commentDisplay.show();
				setupTimeago();
				template.fadeIn();
				campuselite.commentEditActions();
			}

			// no double-submits allowed!
			obj.prop('disabled', true);

			$.ajax({
				url: window.AppConfig.apiRoute+'posts/comment',
				type: 'post',
				data: actionData,
				dataType: 'json'
			}).done(function( returned ){
				insertIntoDom( returned.id );
				obj.prop('disabled', false);
			}).fail( function( e ) {
				obj.prop('disabled', false);
				ajaxFail(e);
			});


		};

		if (typeof context !== 'undefined') {
			// context must be a jquery object
			context.find('.comment-button.no-handler').removeClass('no-handler').click( function() {
				doComment.call( $(this).prev() );
			} );
			context.find('.comment-input.no-handler').removeClass('no-handler').onEnter( doComment );
		} else {
			$('.comment-button.no-handler').removeClass('no-handler').click( function() {
				doComment.call( $(this).prev() );
			} );
			$('.comment-input.no-handler').removeClass('no-handler').onEnter( doComment );
		}
	};



	/**
	 * -----------------------------------------------------------------------------------
	 *	WIDGETS
	 * -----------------------------------------------------------------------------------
	 */



	/**
	 *	Lets you view all photos in a photoset up close and scroll through
	 */
	campuselite.photoCorral = function() {

		if (typeof $.fn.leanModalCtrl === 'undefined') {
			return false;
		}

		if (typeof imagesLoaded === 'undefined') {
			console.error('You have added leanModalCtrl but forgot to add imagesLoaded, which is required.');
			return false;
		}

		//// --------------------------------------

		var photoCache = {};
		var photoCursor = 0;
		var photoCurrentId = 0;
		var photoSetLength = 0;
		var photoCurrentUser = '';
		var navigationReady = false;

		var imgLoadChecker = null;
		var corralObj = $('#photo_corral');
		var corralObjEq = corralObj.eq(0)[0];
		// creates a modal instance with no built-in trigger
		var corralModal = corralObj.leanModalCtrl({closeButton: '.close_modal', top: 10});

		var isAuthUser = function() {
			if (typeof window.AppConfig === 'undefined') {
				return false;
			}
			if (typeof window.AppConfig.username === 'undefined') {
				return false;
			}

			if (window.AppConfig.username === photoCurrentUser) {
				return true;
			}
			return false;
		};


		//// --------------------------------------


		var openModalCallback = function() {
			var wh = $(window).height(); 
			corralObj.find('.wrapper').css('max-height', (wh - 50)+'px');
			corralObj.find('.image-display').css('max-height', (wh - 62)+'px');
			corralModal.open();
			corralModal.centerVertical();
			imgLoadChecker.off( 'always', openModalCallback );
			if ( isAuthUser() ) {
				corralObj.find('.delete_photo').show();
			} else {
				corralObj.find('.delete_photo').hide();
			}
		};

		

		var setCurrentIndex = function() {
			// do this now
			photoSetLength = photoCache[ photoCurrentUser ].length;

			$.each( photoCache[ photoCurrentUser ], function( key, val ) {
				var id = parseInt( val.id );
				if ( photoCurrentId === id ) {
					photoCursor = key;
					return false;
				}
			} );

			navigationReady = true;
		}

		// fetches a photo list via ajax to 
		var loadPhotos = function( userid ) {
			photoCurrentUser = userid;
			if (typeof photoCache[ photoCurrentUser ] === 'undefined') {
				campuselite.serverQuery('/user/'+userid+'/photos/load', {
					method: 'get',
					dataType: 'json',
					onDone: function( returned ){
						photoCache[ photoCurrentUser ] = returned[photoCurrentUser]
						setCurrentIndex();
						corralObj.find('.go-prev, .close_modal, .go-next').fadeTo(300, 0.8);
					}
				});
			} else {
				setCurrentIndex();
				corralObj.find('.go-prev, .close_modal, .go-next').fadeTo(300, 0.8);
			}
		}

		var navigateNext = function( e ) {
			if ( ! navigationReady) {
				return false;
			}
			if ( (photoCursor + 1) === photoSetLength ) {
				photoCursor = 0;
			} else {
				photoCursor = photoCursor + 1;
			}
			updatePhoto( photoCache[ photoCurrentUser ][ photoCursor ] );
			e.stopPropagation();
		};

		var navigatePrevious = function( e ) {
			if ( ! navigationReady) {
				return false;
			}
			if ( photoCursor === 0 ) {
				photoCursor = (photoSetLength - 1);
			} else {
				photoCursor = photoCursor - 1;
			}
			updatePhoto( photoCache[ photoCurrentUser ][ photoCursor ] );
			e.stopPropagation();
		};

		var updatePhoto = function( photoObject ) {
			var afterLoad = function() {
				setTimeout( function() {
					corralModal.refresh();
					corralModal.centerVertical();
					corralModal.refresh();
					corralModal.centerVertical();
				}, 50);
				imgLoadChecker.off( 'always', afterLoad );
			}
			imgLoadChecker = new imagesLoaded( corralObjEq ); 
			imgLoadChecker.on( 'always', afterLoad );
			photoCurrentId = photoObject.id;
			corralObj.find('.image-display').attr('src', photoObject.photo);
		};



		var deletePhoto = function() {
			if ( ! isAuthUser() ) {
				return false;
			}

			if(confirm('Delete this photo? This action will also delete photo posts on your profile where this is the only photo.')) {
				campuselite.serverQuery('/photo/delete/'+photoCurrentId, {
					method: 'post',
					dataType: 'json',
					onDone: function( returned ){
						// remove this image from the photo cache 
						photoCache[ photoCurrentUser ].splice(photoCursor, 1)
						photoSetLength = photoCache[ photoCurrentUser ].length;
						$('[data-pid="'+photoCurrentId+'"]').parent('li').remove();
						$('[data-pid="'+photoCurrentId+'"]').remove();
						if (photoSetLength === 0) {
							// close if none left
							corralObj.find('.close_modal').click();
						} else {
							if (photoSetLength - 1 < photoCursor) {
								photoCursor = 0
							}
							updatePhoto( photoCache[ photoCurrentUser ][ photoCursor ] );
						}
					}
				});
			}
		}


		//// --------------------------------------

		// deleting a photo
		corralObj.find('.delete_photo').click( deletePhoto );

		// assign navigation controls
		corralObj.find('.go-prev').click( navigatePrevious );
		corralObj.find('.go-next').click( navigateNext );
		corralObj.find('.image-display').click( navigateNext );

		// initialize corral when you click a marked picture on screen
		$('.photo-corral, .photo-corral-avatar').removeClass('photo-corral').each(function() {
			$(this).find('a').click(function() {
				navigationReady = false;
				var imgsrc = $(this).data('photo');
				photoCurrentId = parseInt( $(this).data('pid') );
				corralObj.find('.go-prev, .close_modal, .go-next').hide();

				imgLoadChecker = new imagesLoaded( corralObjEq ); 
				imgLoadChecker.on( 'always', openModalCallback );
				corralObj.find('.image-display').attr('src', imgsrc);
				loadPhotos( $(this).data('owner') );
			});
		});
	};




	/**
	 *	Flash messages that pop up on the top of the screen after events like saving forms
	 */
	var flashMessage = function() {

		var flashMessageObj = $('.c-flash-message');
		if (flashMessageObj.length > 1) {
			console.error('Multiple flash messages detected!')
			flashMessageObj = $( flashMessageObj.eq(0) );
		}

		//// functionality ////
		var dismiss = function() {
			$(this).closest('.c-flash-message').fadeOut();
		}

		//// triggers ////
		flashMessageObj.find('.dismiss').click( dismiss );
	};




	/**
	 *	Widget for creating new posts
	 */
	var postCreate = function() {

		var postTypeCache = 'text';

		var workoutLogDataCache = null;

		if (typeof $.fn.autosize === 'undefined') {
			return false;
		}

		var postCreateObject = $('.c-post-create');
		if (postCreateObject.length > 1) {
			console.error('Multiple post widgets detected! Why would you need more than one?')
			postCreateObject = $( postCreateObject.eq(0) );
		};

		//// functionality ////
		var changePostType = function() {
			var ptObj = $(this);
			var postType = ptObj.data('type');

			postCreateObject.find('.post-options .active').removeClass('active');
			ptObj.addClass('active');
			var t = postCreateObject.find('.post-'+postType).find('textarea');
			if (t.val() === '') {
				t.val('\n');
				t.trigger('autosize.resize');
				t.val('');
			}
			postCreateObject.find('.post-type.active').removeClass('active');
			postCreateObject.find('.post-'+postType).addClass('active');

			postCreateObject.find('.choose-workout').show();
			postCreateObject.find('.workout-logger').hide();

			postTypeCache = postType;
		};

		
		// saves the new post, no matter what type
		var sendPostAjax = function ( type, data, afterPost ) {
			var sendData = { type: type };
			$.each(data, function( key, val ){
				sendData[ key ] = val
			});


			$.ajax({
				url: window.getBaseUrl()+'/post/create',
				type: 'post',
				data: sendData,
				dataType: 'html'
			}).done(function( returned ){
				if (campuselite.postPostCreate !== null) {
					if (typeof campuselite.postPostCreate === 'function') {
						campuselite.postPostCreate( returned );
						return false;
					}
				}

				var ins = $( returned );
				ins.hide();
				$('.post-list-feed').prepend(ins);

				// animate user down to the new post in the feed and fade it in
				$('html, body').animate({
				    scrollTop: $(".post-list-feed").offset().top - 80
				}, 400, function() {
					ins.fadeIn();
					setupTimeago();
					campuselite.setupPostEditActions();
					campuselite.photoCorral();
					campuselite.commentAssignHandlers();
					campuselite.fistBumpAssignHandlers();
					if ( typeof afterPost === 'function' ) {
						afterPost();
					}
				});
			}).fail( function( e ) {
				ajaxFail(e);
			});

		};


		// ----------------------------------------------------------------
		// Text
		// ----------------------------------------------------------------


		var submitTextPost = function() {
			var status = campuselite.cleanInput( $.trim(postCreateObject.find('.post-text textarea').val()) );
			if ( status !== '') {
				sendPostAjax( 'text', {
					caption: status
				}, function() {
					postCreateObject.find('.post-text textarea').val('');
				} );
			}
		};


		// ----------------------------------------------------------------
		// Photo
		// ----------------------------------------------------------------

		var initializePhotoPost = function() {

			if (typeof $.fn.ajaxForm === 'undefined') {
				// This is required, so we better make sure it's available
				// before allowing uploads.
				return false;
			}

			var selectorInput = document.getElementById('photosToUpload');

			var promptInput = function() {
				postCreateObject.find('.select-photos').click();
			};

			// based on http://davidwalsh.name/multiple-file-upload
			var getFileList = function() {
				var fileArray = []
				for (var x = 0; x < selectorInput.files.length; x++) {
					fileArray.push( selectorInput.files[x] );
				}
				return fileArray;
			}

			var uploadSelectedFilesToTemp = function() {
				postCreateObject.find('.uploaded-photos .preview').append('<span class="loader">Uploading...</span>');
				postCreateObject.find('.photo-upload-form').submit();
				postCreateObject.find('.add-intial').hide();
				postCreateObject.find('.uploaded-photos').show();
			};

			var afterUpload = function( returned ) {
				returned = $.parseJSON( returned );
				postCreateObject.find('.select-photos').val('');
				var o = postCreateObject.find('.uploaded-photos .preview');
				o.find('.loader, .no-imgs-uploaded').remove();
				var numCounted = 0;
				$.each(returned.images, function( key, val ) {
					if (val === false) return true;
					numCounted++;
					var img = $('<img />').attr('src', window.AppConfig.staticRoot+val);
					var imgWrap = $('<div class="img-wrap" data-image="'+returned.imagesToSave[key]+'"></div>')
						.html( img ).prepend('<i class="remove fa fa-times"></i>'); 

					imgWrap.find('i').click( function() { 
						imgWrap.remove(); 
						if (postCreateObject.find('.img-wrap').length === 0) {
							postCreateObject.find('.uploaded-photos').hide();
							postCreateObject.find('.add-intial').show();
						}
					});
					o.append(imgWrap);
				});

				if (numCounted === 0) {
					o.append('<div style="font-size: 16px; color: #797979;" class="no-imgs-uploaded">No images were uploaded. FIle must end in: .jpg, .jpeg, .gif, .bmp, or .png</div>');
				}
			};

			var progressBar = postCreateObject.find('.progress-bar span')

			postCreateObject.find('.photo-upload-form').ajaxForm({
				beforeSend: function() {
					progressBar.html('0%');
					progressBar.parent().show();
			    },
			    uploadProgress: function( event, position, total, percentComplete ) {
			    	if ( percentComplete < 100 ){
				        progressBar.html( String( percentComplete )+"%" );
				    } else {
			        	progressBar.html( '100%<br>Rendering preview...' );
				    }
			    },
			    success: function() {
			        progressBar.html( '100%<br>Rendering preview...' );
			    },
				complete: function(xhr) {
					postCreateObject.find('.uploaded-preview').show();
					progressBar.parent().hide();
					afterUpload( xhr.responseText );
				}
			});
			postCreateObject.find('.add-intial button, .add-more-photos').click( promptInput );
			postCreateObject.find('.select-photos').change( uploadSelectedFilesToTemp );

		};

		var submitPhotoPost = function() {
			var o = postCreateObject.find('.uploaded-photos .preview');
			var images = []
			 o.find('.img-wrap').each( function( ) {
				images.push( $(this).data('image') );
			});

			if ( images.length === 0 ) {
				alert('Please add at least one image before posting.');
				return false;
			}

			var dataPackage = {
				caption: campuselite.cleanInput( $.trim(postCreateObject.find('.post-photo textarea').val())),
				images: images
			}
			sendPostAjax( 'photo', dataPackage, function() {
				o.html('');
				postCreateObject.find('.uploaded-photos').hide();
				postCreateObject.find('.add-intial').show();
				postCreateObject.find('.post-photo textarea').val('');
			} );
		};


		// ----------------------------------------------------------------
		// Video
		// ----------------------------------------------------------------



		var submitVideoPost = function() {
			var status = campuselite.cleanInput( $.trim(postCreateObject.find('.post-video textarea').val()) );
			var videoUrl = campuselite.cleanInput( $.trim(postCreateObject.find('.post-video input').val()) );

			var videoHost = '';
			var videoId = '';
			if (videoUrl.indexOf('vine.co') !== -1) {
				videoHost = 'vine';
				videoId = videoUrl.split('/v/')[1];
				videoId = videoId.split('/')[0];
				videoId = videoId.split('?')[0];

			} else if (videoUrl.indexOf('instagram.com') !== -1) {
				videoHost = 'instagram';
				videoId = videoUrl.split('/p/')[1];
				videoId = videoId.split('/')[0];
				videoId = videoId.split('?')[0];
				
			} else if (videoUrl.indexOf('youtube.com') !== -1) {
				videoHost = 'youtube';
				var qname = 'v';
				qname = qname.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
			    var regex = new RegExp("[\\?&]" + qname + "=([^&#]*)");
			    var results = regex.exec( videoUrl );
			    videoId = ( results === null ? null : decodeURIComponent(results[1].replace(/\+/g, " ")) );
						
			} else if (videoUrl.indexOf('youtu.be') !== -1) {
				videoHost = 'youtube';
				videoId = videoUrl.split('youtu.be/')[1];
				videoId = videoId.split('/')[0];
				videoId = videoId.split('?')[0];
			}

			if ( videoId && videoHost ) {
				sendPostAjax( 'video', {
					caption: status,
					video_host: videoHost,
					video_id: videoId
				}, function() {
					postCreateObject.find('.post-video textarea, .post-video input').val('');
				} );
			} else {
				if (videoUrl === ''){
					alert("Please add a video URL into the second box. (You can't post a video post without a video!)")
				} else {
					alert("We dont't support the video type that you have entered in the URL bar. Sorry! (We accept Youtube, Vine, and Instagram links)")
				}
			}
		};	


		// ----------------------------------------------------------------
		// Log
		// ----------------------------------------------------------------


		var logTypeWorkoutChoose = function() {
			var selVal = $(this).val();
			if ( selVal === 'new' ) {
				window.location.href= window.getBaseUrl()+"/workouts/new";
				return false;
			}

			if (parseInt(selVal) !== 0) {
				var wdata = campuselite.workoutData[ String(selVal) ];
				if (typeof wdata === 'undefined') {
					return false;
				}
				workoutLogDataCache = {
					workout_id: parseInt( selVal ),
					uses: $.extend( true, {}, wdata.uses ),
					weight_label: ((typeof wdata.weight_label_id !== 'undefined')?  parseInt(wdata.weight_label_id) : 0),
					score_label: ((typeof wdata.score_label_id !== 'undefined')?  parseInt(wdata.score_label_id) : 0),
					distance_label: ((typeof wdata.distance_label_id !== 'undefined')?  parseInt(wdata.distance_label_id) : 0)
				};

				var selectedText = postCreateObject.find('.choose-workout option[value="'+selVal+'"]').html();
				postCreateObject.find('.workout-logger .chosen-type-display').html( selectedText );

				postCreateObject.find('.workout-logger .form-item').hide();
				$.each(wdata.uses, function(key, val){
					if (val) {
						postCreateObject.find('.workout-logger .metric-'+key).show();
					}
				});
				if ( typeof wdata.weight_label !== 'undefined' ) {
					postCreateObject.find('label[for="id_weight"] span').html( wdata.weight_label );
				}
				if ( typeof wdata.score_label !== 'undefined' ) {
					postCreateObject.find('label[for="id_score"] span').html( wdata.score_label );
				}
				if ( typeof wdata.distance_label !== 'undefined' ) {
					postCreateObject.find('label[for="id_distance"] span').html( wdata.distance_label );
				}

				postCreateObject.find('.choose-workout').hide();
				postCreateObject.find('.workout-logger').show();
				postCreateObject.find('.choose-workout select').val(0).change();
			}
		};

		var logTypeGoBack = function() {
			postCreateObject.find('.choose-workout').show();
			postCreateObject.find('.workout-logger').hide();
		};


		var submitLogPost = function() {
			var dataPackage = $.extend(true, {}, workoutLogDataCache);
			delete dataPackage.uses;
			var logger = postCreateObject.find('.workout-logger');

			$.each(workoutLogDataCache.uses, function( key, val ) {
				if (val) {
					if (key === 'time') {
						var hr = logger.find('#id_hour').val();
						var min = logger.find('#id_minute').val();
						var sec = logger.find('#id_second').val();

						var timeStr = pad(hr, 2, '0', STR_PAD_LEFT)+':'+pad(min, 2, '0', STR_PAD_LEFT)+':'+
										pad(sec, 2, '0', STR_PAD_LEFT);

						dataPackage.time = timeStr;
					} else {
						dataPackage[key] = logger.find('#id_'+key).val();
					}
				}
			});

			sendPostAjax( 'log', dataPackage, function() {
				setTimeout(function() {
					$.each(workoutLogDataCache.uses, function( key, val ) {
						if (val) {
							if (key === 'time') {
								logger.find('#id_hour').val(0).change();
								logger.find('#id_minute').val(0).change();
								logger.find('#id_second').val(0).change();
							} else {
								logger.find('#id_'+key).val(0);
							}
						}
					});
				}, 500);
			} );
		};




		// ----------------------------------------------------------------
		// Init
		// ----------------------------------------------------------------

		// Because there is only one "post" button on the widget, we 
		// dynamically assign which function it calls when clicked.
		var createPostSubmit = function () {
			var actions = {
				text: submitTextPost,
				photo: submitPhotoPost,
				video: submitVideoPost,
				log: submitLogPost
			};

			actions[ postTypeCache ]();
		};

		//// triggers + setup ////
		postCreateObject.find('.workout-logger input[type="number"]').change(function() {
			var v = parseInt($(this).val());
			if (v < 0) {
				$(this).val( 0 );
			}
		});
		initializePhotoPost(); 

		postCreateObject.find('textarea').autosize();
		postCreateObject.find('.post-options a').click( changePostType );
		postCreateObject.find('.choose-workout select').change( logTypeWorkoutChoose );
		postCreateObject.find('.workout-logger .go-back').click( logTypeGoBack );
		postCreateObject.find('.submit-button').click( createPostSubmit );
		campuselite.setPostType = function( type ) {
			postTypeCache = type;
		};
	};
	campuselite.postPostCreate = null;


	/**
	 *	Global search bar on app header
	 */
	searchSetup = function() {
		var searching = false;
		var searchTO = null;
		var focusTO = null;
		var templateCache = null;
		var cursorIndex = 0;
		var searchBar = $('.global-search');
		var searchResults = $('.global-search-results');

		var serverSearch = function() {
			var term = campuselite.cleanInput( $.trim( searchBar.val() ) );
			if (term === '') {
				searchResults.hide();
			} else {
				searchResults.html( '<li style="padding: 1em; color: #999; text-align:center;">Searching...	</li>' );
				searchResults.show();
				campuselite.serverQuery('/search', {
					method: 'get',
					data: {term: term},
					dataType: 'json',
					onDone: function( returned ){
						var template = '';

						$.each( returned.results, function(key, val) {
							if ( val.type === 'user' ) {
								template += '<li><a href="'+val.link+'" class="result-user"><img src="'+val.image+'" />'+val.text+'</a></li>';
							} else if ( val.type === 'event' ) {
								template += '<li><a href="'+val.link+'" class="result-event"><i class="fa fa-calendar-o"></i>'+val.text+'</a></li>';
							} else if ( val.type === 'workout' ) {
								template += '<li><a href="'+val.link+'" class="result-workout"><img src="'+window.AppConfig.staticRoot+'image/weightIconSm.svg" />'+val.text+'</a></li>';
							}
						});
						cursorIndex = 0;

						if (returned.results.length === 0) {
							template = '<li style="padding: 1em; color: #999; text-align:center;">No results found</li>';
						} 

						templateCache = $(template);

						searchResults.html( templateCache );
						

						searching = false;
					}
				});
			}
		}

		var doSearch = function() {
			if (searching) { 
				return false; 
			}

			clearTimeout( searchTO );
			searchTO = setTimeout(function() {
				serverSearch();
			}, 350);
		}


		$('.search-mobile-toggle').click(function() {
			$('.page-title').addClass('hideme');
			searchBar.parent().addClass('focused');
			searchBar.focus();
		});

		searchBar.keyup(function() {
			doSearch();
		});

		searchBar.focus(function() {
			clearTimeout( focusTO );
		}).blur(function() {
			$('.page-title').removeClass('hideme');
			if (Modernizr.touch){
				focusTO = setTimeout(function() {
					searchBar.parent().removeClass('focused');
					searchResults.hide();
				}, 320);
			} else {
				focusTO = setTimeout(function() {
					searchBar.parent().removeClass('focused');
					searchResults.hide();
				}, 50);
			}
		});
	};



	// Start 'er up.
	$(document).ready(function() {
		setupNavigation();
		setupNotifications();
		setupTimeago();
		flashMessage();
		postCreate();		
		searchSetup();		
		campuselite.setupPostEditActions();
		campuselite.photoCorral();
		campuselite.followAssignHandlers();
		campuselite.followDietAssignHandlers();
		campuselite.joinEventAssignHandlers();
		campuselite.fistBumpAssignHandlers();
		campuselite.commentEditActions();
		campuselite.commentAssignHandlers();
	});


})();
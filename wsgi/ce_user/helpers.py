
from ce_user.models import *
import random


def UserHelper_SuggestedFriends( user, limit=2, add_extra=False ):

    fol = user.following_newest()
    fol_ids = [ user.pk ]
    for f in fol:
        fol_ids.append( f.follower_user_id )

    interests = user.interests.all()
    int_ids = []
    for i in interests:
        int_ids.append( i.interest_type )

    users = []
    user_pks = []
    interestmatches = CeUserInterest.objects.filter(user__verified=True,  user__deleted=False, interest_type__in=int_ids).exclude(user_id__in=fol_ids).exclude(user__profile__avatar_thumb='').select_related('user').order_by('-user__date_joined')[:200]
    for i in interestmatches:
        if i.user.pk not in user_pks:
            users.append( i.user )
            user_pks.append( i.user.pk )

    if users:
        random.shuffle( users )

        lusers = len(users)
        if add_extra and lusers < limit:
            cutoff = limit - lusers
            uextra = list(CeUser.objects.filter(verified=True, deleted=False).exclude(pk__in=fol_ids).exclude(profile__avatar_thumb='').exclude(pk__in=user_pks)[:cutoff] )
            random.shuffle( uextra )
            users.extend( uextra )


    elif not users:
        users = list(CeUser.objects.filter(verified=True, deleted=False).exclude(profile__avatar_thumb='').exclude(pk__in=fol_ids)[:200])
        random.shuffle( users )

    
    
    if limit == False:
        return users
    else:
        return users[:limit]
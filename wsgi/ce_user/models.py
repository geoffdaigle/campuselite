from django.db import models as m
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
from campuselite import settings
from ce_user.fixtures import *
from ce_diet import models as diet_models

import datetime


###### ------------------------------------------------------------------
######
######  User object models
######
###### ------------------------------------------------------------------


''' 
    MANAGER required for CeUser
'''
class CeUserManage( BaseUserManager ):
    def create_user(self, email, password=None):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=CeUserManage.normalize_email(email)
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password):
        u = self.create_user(username,
            password=password
        )
        u.is_admin = True
        u.save(using=self._db)
        return u



class CeUserFollow( m.Model):
    datetime_created = m.DateTimeField(auto_now_add=True)
    follower_user = m.ForeignKey( 'CeUser', related_name='follower_user' )
    following_user = m.ForeignKey( 'CeUser', related_name='following_user' )

    class Meta:
        ordering = ['-datetime_created']


''' 
    The core user class. Extends Django's base user class.
'''
class CeUser( AbstractBaseUser ):
    email = m.EmailField( verbose_name='email address', max_length=255, unique=True, db_index=True )
    username = m.CharField(max_length=255, blank=False, unique=True, db_index=True)
    first_name = m.CharField(max_length=255, blank=True)
    last_name = m.CharField(max_length=255, blank=True)
    is_superuser = m.BooleanField( default=False )
    verified = m.BooleanField( default=False )
    deleted = m.BooleanField( default=False )
    date_joined = m.DateTimeField(auto_now_add=True)

    followers = m.ManyToManyField('self', related_name='following', symmetrical=False, through='CeUserFollow')

    objects = CeUserManage()
    
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'username', 'first_name', 'last_name']

    def __unicode__(self):
        return self.email

    def full_name( self ):
        return (self.first_name + ' ' + self.last_name).strip()

    def profile_path( self ):
        return '/user/' + self.username

    def profile_path_absolute( self ):
        return settings.BASE_URL+'user/' + self.username

    def profile_path_short( self ):
        return '/u/' + self.username

    def profile_path_short_absolute( self ):
        return settings.BASE_URL+'u/' + self.username

    def follower_count(self):
        return self.following.count()

    def following_count(self):
        return  self.followers.count()

   
    def following_newest(self):
        return CeUserFollow.objects.filter(following_user_id=self.pk)

    def followers_newest(self):
        return CeUserFollow.objects.filter(follower_user_id=self.pk)

    # a-z default
    def following_sorted(self, sort="first_name"):
        return CeUserFollow.objects.filter(follower_user_id=self.pk).order_by( 'follower_user__'+sort )

    # a-z default
    def followers_sorted(self, sort="first_name"):
        return CeUserFollow.objects.filter(following_user_id=self.pk).order_by( 'following_user__'+sort )


    def unread_notifs( self ):
        return self.notifications.filter(shown=False).count()


    def buddies( self, only_id=False ):
        following = self.following_newest()
        followers = self.followers_newest()
        buddies = []
        buddies_ids = []
        for flr in followers:
            for flg in following:
                if flg.follower_user_id == flr.following_user_id and flr.following_user_id != self.pk and flr.following_user_id not in buddies_ids:
                    if only_id:
                        buddies_ids.append( flr.following_user_id )
                        buddies.append( flr.following_user_id )
                    else:
                        buddies_ids.append( flr.following_user_id )
                        buddies.append( flr.following_user )

        return buddies


    def interest_list( self ):
        ints = self.interests.all()
        intlist = []
        for i in ints:
            intlist.append( '<a class="interest-item" href="/similar/interests/'+str(i.interest_type)+'">'+i.get_interest_type_display()+'</a>' )

        return ', '.join( intlist )

    def interest_list_nolink( self ):
        ints = self.interests.all()
        intlist = []
        for i in ints:
            intlist.append( i.get_interest_type_display() )

        return ', '.join( intlist )


    def get_diet(self):
        try:
            d = list(self.diets.all())
            return d[0]
        except IndexError:
            return None


    def is_following( self, user ):
        try:
            u = CeUserFollow.objects.get( follower_user_id=user.pk, following_user_id=self.pk )
            return True
        except CeUserFollow.DoesNotExist:
            return False


    def follows_diet( self, user ):
        try:
            u = diet_models.CeDietFollow.objects.get(diet_user=user, user=self)
            return True
        except diet_models.CeDietFollow.DoesNotExist:
            return False


def generate_image_large_path(instance, filename):
    return u"uploads/user/profile/%s.jpg" % instance.image_hash

def generate_image_medium_path(instance, filename):
    return u"uploads/user/profile/%s_medium.jpg" % instance.image_hash

def generate_image_thumb_path(instance, filename):
    return u"uploads/user/profile/%s_thumb.jpg" % instance.image_hash

class CeUserProfile( m.Model ):
    user = m.OneToOneField( CeUser, related_name="profile")
    college = m.IntegerField(default=0, choices=CE_COLLEGE_CHOICES) # this is char, but we only allow options from a school
    major = m.CharField(max_length=255, blank=True)
    minor = m.CharField(max_length=255, blank=True)
    bio = m.TextField(max_length=2000, blank=True)
    fitness_level = m.IntegerField(default=0, choices=CE_FITNESS_CHOICES)
    goals = m.TextField(max_length=1000, blank=True)
    body_weight = m.IntegerField(default=0)
    body_fat_percent = m.IntegerField(default=0)
    avatar_large = m.ImageField(blank=True, upload_to=generate_image_large_path)
    avatar_medium = m.ImageField(blank=True, upload_to=generate_image_medium_path)
    avatar_thumb = m.ImageField(blank=True, upload_to=generate_image_thumb_path)
    avatar_hash = m.CharField(max_length=255, blank=True)

    suppliment_protein = m.CharField(max_length=255, blank=True)
    suppliment_energy = m.CharField(max_length=255, blank=True)
    suppliment_fatburn = m.CharField(max_length=255, blank=True)
    suppliment_recovery = m.CharField(max_length=255, blank=True)
    suppliment_meals = m.CharField(max_length=255, blank=True)
    suppliment_health = m.CharField(max_length=255, blank=True)


    @property
    def avatar_thumb_path(self):
        if str(self.avatar_thumb) == '':
            return settings.STATIC_URL + 'image/avatar.jpg'
        return settings.STATIC_URL + 'image/user/'+ str(self.avatar_thumb)

    @property
    def avatar_medium_path(self):
        if str(self.avatar_medium) == '':
            return settings.STATIC_URL + 'image/avatar.jpg'
        return settings.STATIC_URL + 'image/user/'+str(self.avatar_medium)

    @property
    def avatar_large_path(self):
        if str(self.avatar_large) == '':
            return settings.STATIC_URL + 'image/avatar.jpg'
        return settings.STATIC_URL + 'image/user/'+str(self.avatar_large)



class CeUserSettings( m.Model ):
    user = m.OneToOneField( CeUser, related_name="settings")



class CeUserNotificationSettings( m.Model ):
    user = m.OneToOneField( CeUser, related_name="notification_settings")
    essential_alerts = m.BooleanField(default=True)
    interaction_alerts = m.BooleanField(default=True)
    dev_updates_news = m.BooleanField(default=True)
    marketing_promos = m.BooleanField(default=True)





###### ------------------------------------------------------------------
######
######  User Interests, Likes, and Follows
######
###### ------------------------------------------------------------------





def get_interest_display( intid ):
    for key, val in CE_GROUP_TYPE_CHOICES:
        if key == intid:
            return val
    return None



# this is for connecting users based on predefined items
class CeUserInterest( m.Model ):
    user = m.ForeignKey(CeUser, related_name="interests")
    interest_type = m.PositiveSmallIntegerField(choices=CE_GROUP_TYPE_CHOICES, db_index=True)
    datetime_created = m.DateTimeField(auto_now_add=True)



class CeUserLike( m.Model ):
    user = m.ForeignKey(CeUser, related_name="likes")
    post = m.ForeignKey('ce_post.CePost', related_name="postlikes")
    datetime_created = m.DateTimeField(auto_now_add=True)






class CeUserTrophies( m.Model ):
    user = m.ForeignKey(CeUser, related_name="trophies")
    type = m.PositiveSmallIntegerField(choices=CE_TROPHY_CHOICES)
    datetime_created = m.DateTimeField(auto_now_add=True)





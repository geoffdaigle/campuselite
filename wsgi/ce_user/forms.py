from django import forms as f
from ce_user.models import *
from ce_user.fixtures import *
from ce_post.models import *
from ce_messaging.helpers import *

'''
	Data validation forms related to this app
'''

class UserForm(f.Form):
	user_id = f.IntegerField(min_value=1)
	

class UserProfileForm(f.ModelForm):
    class Meta:
        model = CeUserProfile
        fields = ['college', 'major', 'minor', 'bio', 'goals', 'fitness_level', 'body_weight', 'body_fat_percent',
        			'suppliment_protein', 'suppliment_energy', 'suppliment_fatburn', 'suppliment_recovery', 'suppliment_meals',
        			'suppliment_health']




class UserFollowForm(f.Form):
	user_id = f.IntegerField( min_value=1 )
	remove = f.BooleanField( required=False )

	def save_data( self, user ):
		cd = self.cleaned_data

		try:
			u = CeUser.objects.get(pk=cd['user_id'])
		except CeUser.DoesNotExist:
			return { "status": "error" }

		if cd['remove']:
			try:
				CeUserFollow.objects.get(follower_user_id=cd['user_id'], following_user_id=user.pk).delete()
			except CeUserFollow.DoesNotExist:
				pass
		else:
			ui = CeUserFollow()
			ui.follower_user_id = cd['user_id']
			ui.following_user = user
			ui.save()

			MessageHelper_CreateNotification({
	            "user": u,
	            "text": "<strong>"+user.full_name()+"</strong> started motivating you.",
	            "icon": 3,
	            "link": user.profile_path(),
	            "primary_image": user.profile.avatar_thumb_path,	
	        })

	        # if they shut this off... skip it
	        if u.notification_settings.interaction_alerts and not cd['remove']:
	        	subject = user.full_name()+" started motivating you"
	        	m = "<p>Hi there!</p>"
		        m += "<p><strong>"+user.full_name()+"</strong> started motivating you on Campus Elite.</p>"
		        m += "<p><a href='"+user.profile_path_absolute()+"'>Check out their profile &rarr;</a></p><br>"
		        m += '''<p>Keep on keepin' on,</p>
		        <p>Campus Elite</p>
		        '''

		        t = compile_email_template({ "body_content": m })
		        send_user_email( message={
		            "subject": subject,
		            "message": t,
		            "to": ['highwaytoinfinity@gmail.com' if settings.DEBUG else u.email],
		            "from": 'Campus Elite <alerts@thecampuselite.com>',
		        } )


		return { "status": "success" }



class UserLikeForm(f.Form):
	post_id = f.IntegerField( min_value=1 )
	remove = f.BooleanField( required=False )

	def save_data( self, user ):
		cd = self.cleaned_data

		if cd['remove']:
			try:
				CeUserLike.objects.get(post_id=cd['post_id'], user_id=user.pk).delete()
			except CeUserLike.DoesNotExist:
				pass
		else:

			try:
				p = CePost.objects.get(pk=cd['post_id'])
			except CePost.DoesNotExist:
				return { "status": "error" }

			# unlike if liked, like otherwise
			try:
				CeUserLike.objects.get(post_id=cd['post_id'], user_id=user.pk).delete()
			except CeUserLike.DoesNotExist:
				ul = CeUserLike()
				ul.post_id = cd['post_id']
				ul.user_id = user.pk
				ul.save()
				UserLikeNotification( p, user )

		return { "status": "success" }



class UserInterestForm(f.Form):
	interests = f.MultipleChoiceField(choices=CE_GROUP_TYPE_CHOICES)

	def save_data( self, user ):
		cd = self.cleaned_data

		# delete all interest and re-insert them
		CeUserInterest.objects.filter(user_id=user.pk).delete()
		for i in cd['interests']:
			ui = CeUserInterest()
			ui.user = user
			ui.interest_type = i 
			ui.save()

		return CeUserInterest.objects.filter(user_id=user.pk)



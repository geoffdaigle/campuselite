# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'CeUser.deleted'
        db.add_column(u'ce_user_ceuser', 'deleted',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'CeUser.deleted'
        db.delete_column(u'ce_user_ceuser', 'deleted')


    models = {
        u'ce_post.cepost': {
            'Meta': {'object_name': 'CePost'},
            'creator': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'posts'", 'to': u"orm['ce_user.CeUser']"}),
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'}),
            'datetime_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'edited': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'post_id': ('django.db.models.fields.IntegerField', [], {}),
            'post_type': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'ce_user.ceuser': {
            'Meta': {'object_name': 'CeUser'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'followers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'following'", 'symmetrical': 'False', 'through': u"orm['ce_user.CeUserFollow']", 'to': u"orm['ce_user.CeUser']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'verified': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'ce_user.ceuserfollow': {
            'Meta': {'ordering': "['-datetime_created']", 'object_name': 'CeUserFollow'},
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'follower_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'follower_user'", 'to': u"orm['ce_user.CeUser']"}),
            'following_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'following_user'", 'to': u"orm['ce_user.CeUser']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'ce_user.ceuserinterest': {
            'Meta': {'object_name': 'CeUserInterest'},
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interest_type': ('django.db.models.fields.PositiveSmallIntegerField', [], {'db_index': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'interests'", 'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_user.ceuserlike': {
            'Meta': {'object_name': 'CeUserLike'},
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'post': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'postlikes'", 'to': u"orm['ce_post.CePost']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'likes'", 'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_user.ceusernotificationsettings': {
            'Meta': {'object_name': 'CeUserNotificationSettings'},
            'dev_updates_news': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'essential_alerts': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interaction_alerts': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'marketing_promos': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'notification_settings'", 'unique': 'True', 'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_user.ceuserprofile': {
            'Meta': {'object_name': 'CeUserProfile'},
            'avatar_hash': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'avatar_large': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'avatar_medium': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'avatar_thumb': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'bio': ('django.db.models.fields.TextField', [], {'max_length': '2000', 'blank': 'True'}),
            'body_fat_percent': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'body_weight': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'college': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'fitness_level': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'goals': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'major': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'minor': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'suppliment_energy': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'suppliment_fatburn': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'suppliment_health': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'suppliment_meals': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'suppliment_protein': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'suppliment_recovery': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'profile'", 'unique': 'True', 'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_user.ceusersettings': {
            'Meta': {'object_name': 'CeUserSettings'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'settings'", 'unique': 'True', 'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_user.ceusertrophies': {
            'Meta': {'object_name': 'CeUserTrophies'},
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'trophies'", 'to': u"orm['ce_user.CeUser']"})
        }
    }

    complete_apps = ['ce_user']
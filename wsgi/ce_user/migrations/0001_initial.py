# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CeUserFollow'
        db.create_table(u'ce_user_ceuserfollow', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('datetime_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('follower_user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='follower_user', to=orm['ce_user.CeUser'])),
            ('following_user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='following_user', to=orm['ce_user.CeUser'])),
        ))
        db.send_create_signal(u'ce_user', ['CeUserFollow'])

        # Adding model 'CeUser'
        db.create_table(u'ce_user_ceuser', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('email', self.gf('django.db.models.fields.EmailField')(unique=True, max_length=255)),
            ('username', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('is_superuser', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('verified', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('date_joined', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'ce_user', ['CeUser'])

        # Adding model 'CeUserProfile'
        db.create_table(u'ce_user_ceuserprofile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(related_name='profile', unique=True, to=orm['ce_user.CeUser'])),
            ('college', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('major', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('minor', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('bio', self.gf('django.db.models.fields.TextField')(max_length=2000, blank=True)),
            ('goals', self.gf('django.db.models.fields.TextField')(max_length=1000, blank=True)),
            ('body_weight', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('body_fat_percent', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('avatar_large', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('avatar_medium', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('avatar_thumb', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('avatar_hash', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
        ))
        db.send_create_signal(u'ce_user', ['CeUserProfile'])

        # Adding model 'CeUserSettings'
        db.create_table(u'ce_user_ceusersettings', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(related_name='settings', unique=True, to=orm['ce_user.CeUser'])),
        ))
        db.send_create_signal(u'ce_user', ['CeUserSettings'])

        # Adding model 'CeUserNotificationSettings'
        db.create_table(u'ce_user_ceusernotificationsettings', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(related_name='notification_settings', unique=True, to=orm['ce_user.CeUser'])),
            ('essential_alerts', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('interaction_alerts', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('dev_updates_news', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('marketing_promos', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'ce_user', ['CeUserNotificationSettings'])

        # Adding model 'CeUserInterest'
        db.create_table(u'ce_user_ceuserinterest', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='interests', to=orm['ce_user.CeUser'])),
            ('interest_type', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('datetime_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'ce_user', ['CeUserInterest'])

        # Adding model 'CeUserLike'
        db.create_table(u'ce_user_ceuserlike', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='likes', to=orm['ce_user.CeUser'])),
            ('post', self.gf('django.db.models.fields.related.ForeignKey')(related_name='postlikes', to=orm['ce_post.CePost'])),
            ('datetime_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'ce_user', ['CeUserLike'])

        # Adding model 'CeUserTrophies'
        db.create_table(u'ce_user_ceusertrophies', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='trophies', to=orm['ce_user.CeUser'])),
            ('type', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('datetime_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'ce_user', ['CeUserTrophies'])


    def backwards(self, orm):
        # Deleting model 'CeUserFollow'
        db.delete_table(u'ce_user_ceuserfollow')

        # Deleting model 'CeUser'
        db.delete_table(u'ce_user_ceuser')

        # Deleting model 'CeUserProfile'
        db.delete_table(u'ce_user_ceuserprofile')

        # Deleting model 'CeUserSettings'
        db.delete_table(u'ce_user_ceusersettings')

        # Deleting model 'CeUserNotificationSettings'
        db.delete_table(u'ce_user_ceusernotificationsettings')

        # Deleting model 'CeUserInterest'
        db.delete_table(u'ce_user_ceuserinterest')

        # Deleting model 'CeUserLike'
        db.delete_table(u'ce_user_ceuserlike')

        # Deleting model 'CeUserTrophies'
        db.delete_table(u'ce_user_ceusertrophies')


    models = {
        u'ce_post.cepost': {
            'Meta': {'object_name': 'CePost'},
            'creator': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'posts'", 'to': u"orm['ce_user.CeUser']"}),
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'datetime_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'edited': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'post_id': ('django.db.models.fields.IntegerField', [], {}),
            'post_type': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'ce_user.ceuser': {
            'Meta': {'object_name': 'CeUser'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'followers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'following'", 'symmetrical': 'False', 'through': u"orm['ce_user.CeUserFollow']", 'to': u"orm['ce_user.CeUser']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'verified': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'ce_user.ceuserfollow': {
            'Meta': {'ordering': "['-datetime_created']", 'object_name': 'CeUserFollow'},
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'follower_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'follower_user'", 'to': u"orm['ce_user.CeUser']"}),
            'following_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'following_user'", 'to': u"orm['ce_user.CeUser']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'ce_user.ceuserinterest': {
            'Meta': {'object_name': 'CeUserInterest'},
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interest_type': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'interests'", 'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_user.ceuserlike': {
            'Meta': {'object_name': 'CeUserLike'},
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'post': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'postlikes'", 'to': u"orm['ce_post.CePost']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'likes'", 'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_user.ceusernotificationsettings': {
            'Meta': {'object_name': 'CeUserNotificationSettings'},
            'dev_updates_news': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'essential_alerts': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interaction_alerts': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'marketing_promos': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'notification_settings'", 'unique': 'True', 'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_user.ceuserprofile': {
            'Meta': {'object_name': 'CeUserProfile'},
            'avatar_hash': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'avatar_large': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'avatar_medium': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'avatar_thumb': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'bio': ('django.db.models.fields.TextField', [], {'max_length': '2000', 'blank': 'True'}),
            'body_fat_percent': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'body_weight': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'college': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'goals': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'major': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'minor': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'profile'", 'unique': 'True', 'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_user.ceusersettings': {
            'Meta': {'object_name': 'CeUserSettings'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'settings'", 'unique': 'True', 'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_user.ceusertrophies': {
            'Meta': {'object_name': 'CeUserTrophies'},
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'trophies'", 'to': u"orm['ce_user.CeUser']"})
        }
    }

    complete_apps = ['ce_user']
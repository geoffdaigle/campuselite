
###### ------------------------------------------------------------------
######
######  Static choices for interests, goals, majors, etc
######
###### ------------------------------------------------------------------



'''
	BOSTON UNIVERSITY COLLEGES
'''

CE_COLLEGE_CGS = 1
CE_COLLEGE_CAS = 2
CE_COLLEGE_CFA = 3
CE_COLLEGE_SHA = 4
CE_COLLEGE_STH = 5
CE_COLLEGE_ENG = 6
CE_COLLEGE_SMG = 7
CE_COLLEGE_COM = 8
CE_COLLEGE_SAR = 9
CE_COLLEGE_SSW = 10
CE_COLLEGE_MET = 11
CE_COLLEGE_SDM = 12
CE_COLLEGE_SED = 13
CE_COLLEGE_LAW = 14
CE_COLLEGE_MED = 15
CE_COLLEGE_SPH = 16
CE_COLLEGE_ROTC = 17
CE_COLLEGE_KHC = 18
CE_COLLEGE_GMS = 19
CE_COLLEGE_GRS = 20

CE_COLLEGE_CHOICES = (
    (0, ''),
    (CE_COLLEGE_CGS, 'College of General Studies'),
    (CE_COLLEGE_CAS, 'College of Arts & Sciences'),
    (CE_COLLEGE_SAR, 'College of Health & Rehabilitation Sciences: Sargent College'),
    (CE_COLLEGE_SSW, 'School of Social Work'),
    (CE_COLLEGE_CFA, 'College of Fine Arts'),
    (CE_COLLEGE_SHA, 'School of Hospitality Administration'),
    (CE_COLLEGE_STH, 'School of Theology'),
    (CE_COLLEGE_ENG, 'College of Engineering'),
    (CE_COLLEGE_SMG, 'School of Management'),
    (CE_COLLEGE_COM, 'College of Communication'),
    (CE_COLLEGE_MET, 'Metropolitan College'),
    (CE_COLLEGE_SDM, 'Henry M. Goldman School of Dental Medicine'),
    (CE_COLLEGE_GMS, 'Division of Graduate Medical Sciences'),
    (CE_COLLEGE_SED, 'School of Education'),
    (CE_COLLEGE_LAW, 'School of Law'),
    (CE_COLLEGE_MED, 'School of Medicine'),
    (CE_COLLEGE_SPH, 'School of Public Health'),
    (CE_COLLEGE_ROTC, 'ROTC: Division of Military Education'),
    (CE_COLLEGE_KHC, 'Arvind and Chandan Nandlal Kilachand Honors College'),
    (CE_COLLEGE_GRS, 'Graduate School of Arts & Sciences'),
)


'''
	BOSTON UNIVERSITY MAJORS/MINORS
'''

CE_MAJOR_ACTING = 1
CE_MAJOR_AERO = 2
CE_MAJOR_AMER = 3
CE_MAJOR_ANCGREEK = 4
CE_MAJOR_ANCLATIN = 5
CE_MAJOR_ANTHRO = 6
CE_MAJOR_ANTREL = 7
CE_MAJOR_ARCHEO = 8
CE_MAJOR_ARCHIT = 9
CE_MAJOR_ART = 10
CE_MAJOR_ASTRO = 11
CE_MAJOR_ASTROPH = 12
CE_MAJOR_ATHLET = 13
CE_MAJOR_BEHAV = 14
CE_MAJOR_BILING = 15
CE_MAJOR_BIOCHEM = 16
CE_MAJOR_BIO = 17
CE_MAJOR_BIOMED = 18
CE_MAJOR_BSAERO = 19
CE_MAJOR_BIZACC = 20
CE_MAJOR_BIZENT = 21
CE_MAJOR_BIZFIN = 22
CE_MAJOR_BIZGEN = 23
CE_MAJOR_BIZINT = 24
CE_MAJOR_BIZLAW = 25
CE_MAJOR_BIZMAN = 26
CE_MAJOR_BIZMAR = 27
CE_MAJOR_BIZOPT = 28
CE_MAJOR_BIZORG = 29
CE_MAJOR_CFACAS = 30
CE_MAJOR_CHEMCON = 31
CE_MAJOR_CHINESE = 32
CE_MAJOR_CLASCIV = 33
CE_MAJOR_CLASPHI = 34
CE_MAJOR_CLASREL = 35
CE_MAJOR_COMMAD = 36
CE_MAJOR_COMMCS = 37
CE_MAJOR_COMMPR = 38
CE_MAJOR_COMPLI = 39
CE_MAJOR_COMPEN = 40
CE_MAJOR_COMSCI = 41
CE_MAJOR_DEAFST = 42
CE_MAJOR_DESIGN = 43
CE_MAJOR_EARCHI = 44
CE_MAJOR_EARTHS = 45
CE_MAJOR_EASIAN = 46
CE_MAJOR_ECONOM = 47
CE_MAJOR_ECOMAT = 48
CE_MAJOR_ELECEN = 49
CE_MAJOR_ELEMED = 50
CE_MAJOR_ENTECH = 51
CE_MAJOR_ENGLIS = 52
CE_MAJOR_ENGLED = 53
CE_MAJOR_ENVANA = 54
CE_MAJOR_ENVEAR = 55
CE_MAJOR_ENVSCI = 56
CE_MAJOR_EUROPE = 57
CE_MAJOR_FILFIL = 58
CE_MAJOR_FILTEL = 59
CE_MAJOR_FRELIN = 60
CE_MAJOR_FRESTU = 61
CE_MAJOR_GEOHUM = 62
CE_MAJOR_GEOPHY = 63
CE_MAJOR_GEOPLA = 64
CE_MAJOR_GERMAN = 65
CE_MAJOR_GRAPHI = 66
CE_MAJOR_HEALTH = 67
CE_MAJOR_HISPAN = 68
CE_MAJOR_HISTOR = 69
CE_MAJOR_HISTAR = 70
CE_MAJOR_HOSPAD = 71
CE_MAJOR_HUMANP = 72
CE_MAJOR_INTERN = 73
CE_MAJOR_ITALIA = 74
CE_MAJOR_ITALST = 75
CE_MAJOR_JAPANE = 76
CE_MAJOR_JAPLIT = 77
CE_MAJOR_JOURBJ = 78
CE_MAJOR_JOURNE = 79
CE_MAJOR_JOUROJ = 80
CE_MAJOR_JOURPJ = 81
CE_MAJOR_LATIN = 82
CE_MAJOR_LATIAM = 83
CE_MAJOR_LIGHDE = 84
CE_MAJOR_LINGUI = 85
CE_MAJOR_LINGPH = 86
CE_MAJOR_MANENG = 87
CE_MAJOR_MANMEC = 88
CE_MAJOR_MARINE = 89
CE_MAJOR_MATHEM = 90
CE_MAJOR_MATCOM = 91
CE_MAJOR_MATPHI = 92
CE_MAJOR_MATEDU = 93
CE_MAJOR_MATHED = 94
CE_MAJOR_MECHENG = 95
CE_MAJOR_MIDEAS = 96
CE_MAJOR_MODFOR = 97
CE_MAJOR_MUNONP = 98
CE_MAJOR_MUPERF = 99
CE_MAJOR_MUSEDU = 100
CE_MAJOR_MUTHEO = 101
CE_MAJOR_MUSICO = 102
CE_MAJOR_NANOTE = 103
CE_MAJOR_NEUROS = 104
CE_MAJOR_NUTRIT = 105
CE_MAJOR_PAINTI = 106
CE_MAJOR_PHILOS = 107
CE_MAJOR_PHILPH = 108
CE_MAJOR_PHILPO = 109
CE_MAJOR_PHILPY = 110
CE_MAJOR_PHILRE = 111
CE_MAJOR_PHYSTH = 112
CE_MAJOR_PHYSIC = 113
CE_MAJOR_POLISC = 114
CE_MAJOR_PREDEN = 115
CE_MAJOR_PRELAW = 116
CE_MAJOR_PREMED = 117
CE_MAJOR_PREVET = 118
CE_MAJOR_PRINTM = 119
CE_MAJOR_PSYCHO = 120
CE_MAJOR_RELIGI = 121
CE_MAJOR_RUSSIA = 122
CE_MAJOR_SCENEA = 123
CE_MAJOR_SCIENC = 124
CE_MAJOR_SCULPT = 125
CE_MAJOR_SEDCAS = 126
CE_MAJOR_SOCIAL = 127
CE_MAJOR_SOCIOL = 128
CE_MAJOR_SOUNDD = 129
CE_MAJOR_SPANIS = 130
CE_MAJOR_SPECIA = 131
CE_MAJOR_SPEECH = 132
CE_MAJOR_STAGEM = 133
CE_MAJOR_THEATR = 134
CE_MAJOR_THEPRO = 135
CE_MAJOR_THETEC = 136
CE_MAJOR_CHEM = 137
CE_MAJOR_CHEMBIO = 138


CE_MAJOR_CHOICES = (
    (0, ''),
    (CE_MAJOR_ACTING, 'Acting'),
	(CE_MAJOR_AERO, 'Aerospace Engineering Concentration'),
	(CE_MAJOR_AMER, 'American & New England Studies'),
	(CE_MAJOR_ANCGREEK, 'Ancient Greek'),
	(CE_MAJOR_ANCLATIN, 'Ancient Greek and Latin'),
	(CE_MAJOR_ANTHRO, 'Anthropology'),
	(CE_MAJOR_ANTREL, 'Anthropology and Religion'),
	(CE_MAJOR_ARCHEO, 'Archaeology'),
	(CE_MAJOR_ARCHIT, 'Architectural Studies'),
	(CE_MAJOR_ART, 'Art Education'),
	(CE_MAJOR_ASTRO, 'Astronomy'),
	(CE_MAJOR_ASTROPH, 'Astronomy and Physics'),
	(CE_MAJOR_ATHLET, 'Athletic Training'),
	(CE_MAJOR_BEHAV, 'Behavior & Health'),
	(CE_MAJOR_BILING, 'Bilingual Education (English as a Second Language)'),
	(CE_MAJOR_BIOCHEM, 'Biochemistry & Molecular Biology'),
	(CE_MAJOR_BIO, 'Biology'),
	(CE_MAJOR_BIOMED, 'Biomedical Engineering'),
	(CE_MAJOR_BSAERO, 'BS Aerospace/MS Mechanical'),
	(CE_MAJOR_BIZACC, 'Business Administration & Management-Concentration: Accounting'),
	(CE_MAJOR_BIZENT, 'Business Administration & Management-Concentration: Entrepreneurship'),
	(CE_MAJOR_BIZFIN, 'Business Administration & Management-Concentration: Finance'),
	(CE_MAJOR_BIZGEN, 'Business Administration & Management-Concentration: General Management'),
	(CE_MAJOR_BIZINT, 'Business Administration & Management-Concentration: International Management'),
	(CE_MAJOR_BIZLAW, 'Business Administration & Management-Concentration: Law'),
	(CE_MAJOR_BIZMAN, 'Business Administration & Management-Concentration: Management Information Systems'),
	(CE_MAJOR_BIZMAR, 'Business Administration & Management-Concentration: Marketing'),
	(CE_MAJOR_BIZOPT, 'Business Administration & Management-Concentration: Operations & Technology Management'),
	(CE_MAJOR_BIZORG, 'Business Administration & Management-Concentration: Organizational Behavior'),
	(CE_MAJOR_CFACAS, 'CFA/CAS Double Degree Program'),
	(CE_MAJOR_CHEM, 'Chemistry'),
	(CE_MAJOR_CHEMBIO, 'Chemistry: Biochemistry'),
	(CE_MAJOR_CHEMCON, 'Chemistry: Concentration in Teaching'),
	(CE_MAJOR_CHINESE, 'Chinese Language & Literature'),
	(CE_MAJOR_CLASCIV, 'Classical Civilization'),
	(CE_MAJOR_CLASPHI, 'Classics and Philosophy'),
	(CE_MAJOR_CLASREL, 'Classics and Religion'),
	(CE_MAJOR_COMMAD, 'Communication with Specialization in Advertising'),
	(CE_MAJOR_COMMCS, 'Communication with Specialization in Communication Studies'),
	(CE_MAJOR_COMMPR, 'Communication with Specialization in Public Relations'),
	(CE_MAJOR_COMPLI, 'Comparative Literature'),
	(CE_MAJOR_COMPEN, 'Computer Engineering'),
	(CE_MAJOR_COMSCI, 'Computer Science'),
	(CE_MAJOR_DEAFST, 'Deaf Studies'),
	(CE_MAJOR_DESIGN, 'Design'),
	(CE_MAJOR_EARCHI, 'Early Childhood Education'),
	(CE_MAJOR_EARTHS, 'Earth Sciences'),
	(CE_MAJOR_EASIAN, 'East Asian Studies'),
	(CE_MAJOR_ECONOM, 'Economics'),
	(CE_MAJOR_ECOMAT, 'Economics & Mathematics'),
	(CE_MAJOR_ELECEN, 'Electrical Engineering'),
	(CE_MAJOR_ELEMED, 'Elementary Education'),
	(CE_MAJOR_ENTECH, 'Energy Technologies & Environmental Engineering Concentration'),
	(CE_MAJOR_ENGLIS, 'English'),
	(CE_MAJOR_ENGLED, 'English Education'),
	(CE_MAJOR_ENVANA, 'Environmental Analysis & Policy'),
	(CE_MAJOR_ENVEAR, 'Environmental Earth Sciences'),
	(CE_MAJOR_ENVSCI, 'Environmental Science'),
	(CE_MAJOR_EUROPE, 'European Studies'),
	(CE_MAJOR_FILFIL, 'Film & Television with Specialization in Film'),
	(CE_MAJOR_FILTEL, 'Film & Television with Specialization in Television'),
	(CE_MAJOR_FRELIN, 'French & Linguistics'),
	(CE_MAJOR_FRESTU, 'French Studies'),
	(CE_MAJOR_GEOHUM, 'Geography with Specialization in Human Geography'),
	(CE_MAJOR_GEOPHY, 'Geography with Specialization in Physical Geography'),
	(CE_MAJOR_GEOPLA, 'Geophysics & Planetary Sciences'),
	(CE_MAJOR_GERMAN, 'German Language & Literature'),
	(CE_MAJOR_GRAPHI, 'Graphic Design'),
	(CE_MAJOR_HEALTH, 'Health Science'),
	(CE_MAJOR_HISPAN, 'Hispanic Language & Literature'),
	(CE_MAJOR_HISTOR, 'History'),
	(CE_MAJOR_HISTAR, 'History of Art & Architecture'),
	(CE_MAJOR_HOSPAD, 'Hospitality Administration'),
	(CE_MAJOR_HUMANP, 'Human Physiology'),
	(CE_MAJOR_INTERN, 'International Relations'),
	(CE_MAJOR_ITALIA, 'Italian & Linguistics'),
	(CE_MAJOR_ITALST, 'Italian Studies'),
	(CE_MAJOR_JAPANE, 'Japanese & Linguistics'),
	(CE_MAJOR_JAPLIT, 'Japanese Language & Literature'),
	(CE_MAJOR_JOURBJ, 'Journalism with Specialization: Broadcast Journalism'),
	(CE_MAJOR_JOURNE, 'Journalism with Specialization: News Editorial'),
	(CE_MAJOR_JOUROJ, 'Journalism with Specialization: Online Journalism'),
	(CE_MAJOR_JOURPJ, 'Journalism with Specialization: Photojournalism'),
	(CE_MAJOR_LATIN, 'Latin'),
	(CE_MAJOR_LATIAM, 'Latin American Studies'),
	(CE_MAJOR_LIGHDE, 'Lighting Design'),
	(CE_MAJOR_LINGUI, 'Linguistics'),
	(CE_MAJOR_LINGPH, 'Linguistics & Philosophy'),
	(CE_MAJOR_MANENG, 'Manufacturing Engineering Concentration (BS Aerospace/MS Mechanical)'),
	(CE_MAJOR_MANMEC, 'Manufacturing Engineering Concentration (Mechanical Engineering)'),
	(CE_MAJOR_MARINE, 'Marine Science'),
	(CE_MAJOR_MATHEM, 'Mathematics'),
	(CE_MAJOR_MATCOM, 'Mathematics & Computer Science'),
	(CE_MAJOR_MATPHI, 'Mathematics & Philosophy'),
	(CE_MAJOR_MATEDU, 'Mathematics and Education'),
	(CE_MAJOR_MATHED, 'Mathematics Education'),
	(CE_MAJOR_MECHENG, 'Mechanical Engineering'),
	(CE_MAJOR_MIDEAS, 'Middle East and North Africa Studies'),
	(CE_MAJOR_MODFOR, 'Modern Foreign Languages Education'),
	(CE_MAJOR_MUNONP, 'Music (Nonperformance)'),
	(CE_MAJOR_MUPERF, 'Music (Performance)'),
	(CE_MAJOR_MUSEDU, 'Music Education'),
	(CE_MAJOR_MUTHEO, 'Music Theory & Composition'),
	(CE_MAJOR_MUSICO, 'Musicology'),
	(CE_MAJOR_NANOTE, 'Nanotechnology Concentration'),
	(CE_MAJOR_NEUROS, 'Neuroscience'),
	(CE_MAJOR_NUTRIT, 'Nutritional Science'),
	(CE_MAJOR_PAINTI, 'Painting'),
	(CE_MAJOR_PHILOS, 'Philosophy'),
	(CE_MAJOR_PHILPH, 'Philosophy & Physics'),
	(CE_MAJOR_PHILPO, 'Philosophy & Political Science'),
	(CE_MAJOR_PHILPY, 'Philosophy & Psychology'),
	(CE_MAJOR_PHILRE, 'Philosophy & Religion'),
	(CE_MAJOR_PHYSTH, 'Physical Therapy'),
	(CE_MAJOR_PHYSIC, 'Physics'),
	(CE_MAJOR_POLISC, 'Political Science'),
	(CE_MAJOR_PREDEN, 'Pre-Dentistry'),
	(CE_MAJOR_PRELAW, 'Pre-Law'),
	(CE_MAJOR_PREMED, 'Pre-Medicine'),
	(CE_MAJOR_PREVET, 'Pre-Veterinary Medicine'),
	(CE_MAJOR_PRINTM, 'Printmaking'),
	(CE_MAJOR_PSYCHO, 'Psychology'),
	(CE_MAJOR_RELIGI, 'Religion'),
	(CE_MAJOR_RUSSIA, 'Russian Language & Literature'),
	(CE_MAJOR_SCENEA, 'Scene Design'),
	(CE_MAJOR_SCIENC, 'Science Education'),
	(CE_MAJOR_SCULPT, 'Sculpture'),
	(CE_MAJOR_SEDCAS, 'SED/CAS Double Degree Program'),
	(CE_MAJOR_SOCIAL, 'Social Studies Education'),
	(CE_MAJOR_SOCIOL, 'Sociology'),
	(CE_MAJOR_SOUNDD, 'Sound Design'),
	(CE_MAJOR_SPANIS, 'Spanish & Linguistics'),
	(CE_MAJOR_SPECIA, 'Special Education'),
	(CE_MAJOR_SPEECH, 'Speech, Language & Hearing Sciences'),
	(CE_MAJOR_STAGEM, 'Stage Management'),
	(CE_MAJOR_THEATR, 'Theatre Arts'),
	(CE_MAJOR_THEPRO, 'Theatre Production (Costume)'),
	(CE_MAJOR_THETEC, 'Theatre Production (Technical)'),
)


'''
	INTERESTS / GROUP TYPES
'''

CE_GROUP_TYPE_RUNNING = 1
CE_GROUP_TYPE_WEIGHTLIFTING = 2
CE_GROUP_TYPE_BODYBUILDING = 3
CE_GROUP_TYPE_POWERLIFTING = 4
CE_GROUP_TYPE_CROSSFIT = 5
CE_GROUP_TYPE_BOARDSPORTS = 6
CE_GROUP_TYPE_REQUETSPORTS = 7
CE_GROUP_TYPE_WATERSPORTS = 8
CE_GROUP_TYPE_COMBATSPORTS = 9
CE_GROUP_TYPE_SNOWSPORTS = 10
CE_GROUP_TYPE_CLIMBINGSPORTS = 11
CE_GROUP_TYPE_AIRSPORTS = 12
CE_GROUP_TYPE_MOTORSPORTS = 13
CE_GROUP_TYPE_TAGGAMES = 14
CE_GROUP_TYPE_TABLESPORTS = 15
CE_GROUP_TYPE_BIKING = 16
CE_GROUP_TYPE_FRISBEE = 17
CE_GROUP_TYPE_SOCCER = 18
CE_GROUP_TYPE_FOOTBALL = 19
CE_GROUP_TYPE_FLAGFOOTBALL = 20
CE_GROUP_TYPE_BASEBALL = 21
CE_GROUP_TYPE_WIFFLEBALL = 22
CE_GROUP_TYPE_BASKETBALL = 23
CE_GROUP_TYPE_KICKBALL = 24
CE_GROUP_TYPE_DODGEBALL = 25
CE_GROUP_TYPE_QUIDDITCH = 26
CE_GROUP_TYPE_GOLF = 27
CE_GROUP_TYPE_ARCHERY = 28
CE_GROUP_TYPE_SHOOTING = 29
CE_GROUP_TYPE_ICEHOCKEY = 30
CE_GROUP_TYPE_FIELDHOCKEY = 31
CE_GROUP_TYPE_PARKOUR = 32
CE_GROUP_TYPE_DANCE = 33
CE_GROUP_TYPE_GYMNASTICS = 34
CE_GROUP_TYPE_YOGA = 35

CE_GROUP_TYPE_CHOICES = (
    # general groups
    (CE_GROUP_TYPE_RUNNING, 'Running/Cardio'),
    (CE_GROUP_TYPE_WEIGHTLIFTING, 'Weighlifting'),
    (CE_GROUP_TYPE_BODYBUILDING, 'Bodybuilding'),
    (CE_GROUP_TYPE_POWERLIFTING, 'Powerlifting'),
    (CE_GROUP_TYPE_CROSSFIT, 'Crossfit'),
    (CE_GROUP_TYPE_BOARDSPORTS, 'Board Sports'),
    (CE_GROUP_TYPE_REQUETSPORTS, 'Raquet Sports'),
    (CE_GROUP_TYPE_WATERSPORTS, 'Water Sports'),
    (CE_GROUP_TYPE_COMBATSPORTS, 'Combat Sports'),
    (CE_GROUP_TYPE_SNOWSPORTS, 'Snow Sports'),
    (CE_GROUP_TYPE_CLIMBINGSPORTS, 'Climbing Sports'),
    (CE_GROUP_TYPE_AIRSPORTS, 'Air Sports'),
    (CE_GROUP_TYPE_MOTORSPORTS, 'Motor Sports'),
    (CE_GROUP_TYPE_TAGGAMES, 'Tag Games'),
    (CE_GROUP_TYPE_TABLESPORTS, 'Table Sports'),

    # specific sports
    (CE_GROUP_TYPE_BIKING, 'Biking'),
    (CE_GROUP_TYPE_FRISBEE, 'Frisbee'),
    (CE_GROUP_TYPE_SOCCER, 'Soccer'),
    (CE_GROUP_TYPE_FOOTBALL, 'Football'),
    (CE_GROUP_TYPE_FLAGFOOTBALL, 'Flag Football'),
    (CE_GROUP_TYPE_BASEBALL, 'Baseball'),
    (CE_GROUP_TYPE_WIFFLEBALL, 'Wiffleball'),
    (CE_GROUP_TYPE_BASKETBALL, 'Basketball'),
    (CE_GROUP_TYPE_KICKBALL, 'Kickball'),
    (CE_GROUP_TYPE_DODGEBALL, 'Dodgeball'),
    (CE_GROUP_TYPE_QUIDDITCH, 'Quidditch'),
    (CE_GROUP_TYPE_GOLF, 'Golf'),
    (CE_GROUP_TYPE_ARCHERY, 'Archery'),
    (CE_GROUP_TYPE_SHOOTING, 'Shooting'),
    (CE_GROUP_TYPE_ICEHOCKEY, 'Ice Hockey'),
    (CE_GROUP_TYPE_FIELDHOCKEY, 'Field Hockey'),
    (CE_GROUP_TYPE_PARKOUR, 'Parkour/Freerunning'),
    (CE_GROUP_TYPE_DANCE, 'Dance'),
    (CE_GROUP_TYPE_GYMNASTICS, 'Gymnastics'),
    (CE_GROUP_TYPE_YOGA, 'Yoga'),
)

CE_FITNESS_NONE = 0
CE_FITNESS_BEGINNER = 1
CE_FITNESS_INTERMEDIATE = 2
CE_FITNESS_ADVANCED = 3 
CE_FITNESS_PRO = 4
CE_FITNESS_CHOICES = (
	(CE_FITNESS_NONE,''),
	(CE_FITNESS_BEGINNER,'Beginner'),
	(CE_FITNESS_INTERMEDIATE,'Intermediate'),
	(CE_FITNESS_ADVANCED,'Advanced'),
	(CE_FITNESS_PRO,'Professional'),
)



'''
	TROPHY TYPES
'''

CE_TROPHIES_MOST_FOLLOWERS = 1
CE_TROPHY_CHOICES = (
    (CE_TROPHIES_MOST_FOLLOWERS, 'Most followers'),
)


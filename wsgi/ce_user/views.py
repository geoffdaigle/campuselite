from django.shortcuts import render, redirect
from django.http import HttpResponse
from campuselite import shortcuts
import datetime
from django.utils import timezone
from django.contrib.auth import hashers

from ce_user.models import CeUser, CeUserProfile
from ce_post.models import CePost, CeStatus, CeLink, CePhoto, CePhotoSet, CeVideo
from ce_diet.models import CeDiet
from ce_workout.models import CeWorkoutType, CeWorkout, CeWorkoutLog


def clearusers(rq):
    CeUser.objects.all().delete()
    return redirect("/api/me")


# shorthand for creating a bunch of test data
def createuser(rq):

    # create two users
    user = CeUser()
    user.is_superuser = 1
    user.email = 'geoff@campuselite.com'
    user.username = 'geoff'
    user.first_name = 'Geoff'
    user.last_name = 'Daigle'
    user.password = hashers.make_password('097410')
    user.save()

    up = CeUserProfile()
    up.user = user
    up.bio = "I am a developer"
    up.goals = 'I like to lift stuff up and put them down'
    up.save()


    user2 = CeUser()
    user2.is_superuser = 0
    user2.email = 'mike@campuselite.com'
    user2.username = 'mike'
    user2.first_name = 'Mike'
    user2.last_name = 'Bardeaux'
    user2.password = hashers.make_password('097410')
    user2.save()

    up2 = CeUserProfile()
    up2.user = user2
    up2.bio = "I am a guy"
    up2.goals = 'I like to throw parties'
    up2.save()


    user3 = CeUser()
    user3.is_superuser = 0
    user3.email = 'test@campuselite.com'
    user3.username = 'test'
    user3.first_name = 'Test'
    user3.last_name = 'Man'
    user3.password = hashers.make_password('okaysure')
    user3.save()

    up3 = CeUserProfile()
    up3.user = user3
    up3.bio = "I am a test"
    up3.goals = 'I like failing tests'
    up3.save()

    # ADDING followers means that ...
    user.followers.add(user2)
    ## ^^ user2 is FOLLOWING user

    user2.followers.add(user)
    user2.followers.add(user3)


    # make them post some stuff
    status1 = CeStatus()
    status1.user = user
    status1.caption = "I love campus elite it's fucking raaaaaad"
    status1.save()

    post1 = CePost()
    post1.creator = user
    post1.post_type = 3
    post1.post_id = status1.pk
    post1.save()

    status2 = CeStatus()
    status2.user = user
    status2.caption = "Wait, I changed my mind"
    status2.save()

    post2 = CePost()
    post2.creator = user
    post2.post_type = 3
    post2.post_id = status2.pk
    post2.save()

    status3 = CeStatus()
    status3.user = user2
    status3.caption = "I am pretty cool man"
    status3.save()

    post3 = CePost()
    post3.creator = user2
    post3.post_type = 3
    post3.post_id = status3.pk
    post3.save()

    return shortcuts.render_template( rq, 'layout', {"name": "User created"} )








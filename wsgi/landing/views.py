from django.shortcuts import render
from django.http import HttpResponse
from campuselite import shortcuts

# Create your views here.


def home( rq ):
	link_list = ['/me',
				'/me/profile',
				'/me/settings',
				'/me/likes',
				'/me/posts',
				'/me/posts/1',
				'/me/diet',
				'/me/followers',
				'/me/following',
				'/me/following/1',
				'/me/interests',
				'/me/trophies',
				'/me/workouts',
				'/me/workouts/1',
				'/me/workouts/1/log',
				'/me/workouts/1/log/1',
				'/me/diets/following',
				'/me/workouts/following',
				'/me/groups',
				'/me/groups/1',
			]

	user_list = ['/users/2',
				'/users/2/posts',
				'/users/2/likes',
				'/users/2/following',
				'/users/2/followers',
				'/users/2/interests',
				'/users/2/diet',
				'/users/2/workouts',
				'/users/2/trophies',
				'/users/2/groups'
			]

	return shortcuts.render_template( rq, 'layout', {"name": "Campus Elite", "link_list": link_list, "user_list": user_list} )
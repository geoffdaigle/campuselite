from django.conf.urls import patterns, url, include
from ce_api import views, views_me


me_urls = patterns('',

    # /me/diets/following
    url(r'^/groups/(?P<pk>[0-9]+)$', views_me.MyGroupsDetails.as_view(), name='mygroups-details'),

    # /me/diets/following
    url(r'^/groups$', views_me.MyGroupsList.as_view(), name='mygroups-list'),

    # /me/diets/following
    url(r'^/diets/following$', views_me.MyDietsFollowingList.as_view(), name='mydietsfollowing-list'),

    # /me/workouts/following
    url(r'^/workouts/following$', views_me.MyWorkoutsFollowingList.as_view(), name='myworkoutsfollowing-list'),

    # /me/trophies
    url(r'^/trophies$', views_me.MyUserTrophiesList.as_view(), name='myusertrophies-list'),

    # /me/workouts/[id]/log/[id]
    url(r'^/workouts/(?P<pk>[0-9]+)/log/(?P<wid>[0-9]+)$', views_me.MyUserWorkoutsLogDetails.as_view(), name='myuserworkoutslog-details'),

    # /me/workouts/[id]/log
    url(r'^/workouts/(?P<pk>[0-9]+)/log$', views_me.MyUserWorkoutsLogList.as_view(), name='myuserworkoutslog-list'),

    # /me/workouts/[id]
    url(r'^/workouts/(?P<pk>[0-9]+)$', views_me.MyUserWorkoutsDetails.as_view(), name='myuserworkouts-details'),

    # /me/workouts
    url(r'^/workouts$', views_me.MyUserWorkoutsList.as_view(), name='myuserworkouts-list'),

    # /me/diet/log
    url(r'^/diet/log$', views_me.MyUserDietLogList.as_view(), name='myuserdietlog-list'),

    # /me/diet
    url(r'^/diet$', views_me.MyUserDietDetails.as_view(), name='myuserdiet-details'),

    # /me/interests
    url(r'^/interests$', views_me.MyUserInterestList.as_view(), name='myuserinterest-list'),

    # /me/likes
    url(r'^/likes$', views_me.MyUserLikesList.as_view(), name='myuserlikes-list'),

    # /me/posts
    url(r'^/posts$', views_me.MyUserPostList.as_view(), name='myuserpost-list'),

    # /me/posts/[id]
    url(r'^/posts/(?P<pk>[0-9]+)$', views_me.MyUserPostDetails.as_view(), name='myuserpost-details'),

    # /me/settings
    url(r'^/settings$', views_me.MyUserSettingsDetails.as_view(), name='myusersettings-details'),

    # /me/profile
    url(r'^/profile$', views_me.MyUserProfileDetails.as_view(), name='myuserprofile-details'),
    
    # /me/
    url(r'^$', views_me.MyUserDetails.as_view(), name='myuser-details'),
)



user_urls = patterns('',
    url(r'^/(?P<pk>[0-9]+)/interests$', views.InterestList.as_view(), name='interest-list'),

    # /users/[id]/groups
    url(r'^/(?P<pk>[0-9]+)/groups$', views.UserGroupsList.as_view(), name='usergroups-list'),

    # /users/[id]/posts
    url(r'^/(?P<pk>[0-9]+)/posts$', views.UserPostList.as_view(), name='userpost-list'),

    # /users/[id]/likes
    url(r'^/(?P<pk>[0-9]+)/likes$', views.UserLikesList.as_view(), name='userlikes-list'),

    # /users/[id]/diet
    url(r'^/(?P<pk>[0-9]+)/diet$', views.UserDietDetail.as_view(), name='userdiet-detail'),

    # /users/[id]/workouts
    url(r'^/(?P<pk>[0-9]+)/workouts$', views.UserWorkoutsList.as_view(), name='userworkouts-list'),
    
    # /users/[id]/trophies
    url(r'^/(?P<pk>[0-9]+)/trophies$', views.UserTrophiesList.as_view(), name='usertrophies-list'),

    # /users/[id]/profile
    url(r'^/(?P<pk>[0-9]+)/profile$', views.UserProfileDetail.as_view(), name='userprofile-detail'),

    url(r'^/(?P<pk>[0-9]+)/follows$', views.FollowList.as_view(), name='follow-list'),

    # /users/[id]
    url(r'^/(?P<pk>[0-9]+)$', views.UserDetail.as_view(), name='user-detail'),

    # /users/
    url(r'^$',views.UserList.as_view(), name='user-list'),
)



post_urls = patterns('',
    # /posts/[id]/comments[id]
    url(r'^/(?P<pk>\d+)/comments/(?P<cid>\d+)$', views.PostCommentsDetail.as_view(), name='postcomments-detail'),

    # /posts/[id]/comments
    url(r'^/(?P<pk>\d+)/comments$', views.PostCommentsList.as_view(), name='postcomments-list'),

    # /posts/[id]/likes 
    url(r'^/(?P<pk>\d+)/likes$', views.PostLikesList.as_view(), name='postlikes-list'),

    url(r'^/like$', views.PostLikesCreate.as_view(), name='postlikes-create'),
    url(r'^/comment$', views.PostCommentsCreate.as_view(), name='postcomments-create'),

    # /posts/[id]
    url(r'^/(?P<pk>\d+)$', views.PostDetail.as_view(), name='post-detail'),

    # /posts/
    url(r'^$', views.PostList.as_view(), name='post-list'),
)


diets_urls = patterns('',
    # /diets/followers/[id] 
    url(r'^/followers/(?P<pk>\d+)$', views.DietDeleteFollowDetail.as_view(), name='dietdeletefollow-detail'),

    # /diets/[user_id]/followers
    url(r'^/(?P<pk>\d+)/followers$', views.DietsFollowersList.as_view(), name='dietsfollowers-list'),

    # /diets/[user_id]/log
    url(r'^/(?P<pk>\d+)/log$', views.DietsLogList.as_view(), name='dietslog-list'),

    # /diets/[id]
    url(r'^/(?P<pk>\d+)$', views.DietsDetail.as_view(), name='diets-detail'),
)

workouts_urls = patterns('',
    # /workouts/followers/[id]
    url(r'^/followers/(?P<pk>\d+)$', views.WorkoutDeleteFollowDetail.as_view(), name='workoutdeletefollow-detail'),

    # /workouts/[id]/followers
    url(r'^/(?P<pk>\d+)/followers$', views.WorkoutsFollowersList.as_view(), name='workoutsfollowers-list'),

    # /workouts/[id]/log
    url(r'^/(?P<pk>\d+)/log$', views.WorkoutsLogList.as_view(), name='workoutslog-list'),

    # /workouts/[id]
    url(r'^/(?P<pk>\d+)$', views.WorkoutsDetail.as_view(), name='workouts-detail'),
)

feed_urls = patterns('',
    # /feed/all
    url(r'^/all$', views.FeedAllList.as_view(), name='feedall-list'),

    # /feed/
    url(r'^$', views.FeedList.as_view(), name='feed-list'),
)


group_urls = patterns('',
    # /groups/[id]/attendees/[id]
    #url(r'^/(?P<pk>\d+)/attendees/(?P<aid>\d+)$', views.GroupsDetail.as_view(), name='groups-detail'),

    # /groups/[id]/attendees
    #url(r'^/(?P<pk>\d+)/attendees$', views.GroupsAttendeesDetail.as_view(), name='groups-detail'),

    url(r'^/attendees$', views.GroupsAttendeesCreate.as_view(), name='groupsattendees-create'),

    # /groups/[id]
    url(r'^/(?P<pk>\d+)$', views.GroupsDetails.as_view(), name='groups-details'),

    # /groups/
    url(r'^$', views.GroupsList.as_view(), name='groups-list'),
)


'''
messages_urls = patterns('',
    # /messages/
    url(r'^/$', views.MessagesList.as_view(), name='messages-list'),
)

leaderboard_urls = patterns('',
    # /leaderboard/
    url(r'^/$', views.LeaderboardList.as_view(), name='leaderboard-list'),
)'''



####
####  Base patterns
####

urlpatterns = patterns('',
    url(r'^me', include(me_urls)),
    url(r'^users', include(user_urls)),
    url(r'^posts', include(post_urls)),
    url(r'^diets', include(diets_urls)),
    url(r'^workouts', include(workouts_urls)),
    url(r'^feed', include(feed_urls)),
    url(r'^groups', include(group_urls)),
)


'''
    url(r'^messages', include(messages_urls)),
    url(r'^leaderboard', include(leaderboard_urls)),'''
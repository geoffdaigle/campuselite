from rest_framework import serializers

from ce_user import models as user_models
from ce_post import models as post_models
from ce_workout import models as workout_models
from ce_diet import models as diet_models
from ce_group import models as group_models
from ce_messaging import models as messaging_models
from ce_leaderboard import models as leaderboard_models




###### ------------------------------------------------------------------
######
######  USER 
######
###### ------------------------------------------------------------------


class UserProfileSerializer(serializers.ModelSerializer):
    user = serializers.Field(source='user_id')

    class Meta:
        model = user_models.CeUserProfile
        fields = ('id', 'user', 'college', 'major', 'minor', 'bio', 'goals', 'body_weight', 'body_fat_percent',
            'avatar_large', 'avatar_medium', 'avatar_thumb')


class UserSerializer(serializers.ModelSerializer):
    profile = UserProfileSerializer() 
    followers = serializers.Field(source='follower_count')
    following = serializers.Field(source='following_count')

    class Meta:
        model = user_models.CeUser
        fields = ('id', 'is_superuser', 'username', 'first_name', 'last_name', 'email', 
                    'profile', 'followers', 'following'
                )



class UserProfileBasicSerializer(serializers.ModelSerializer):
    class Meta:
        model = user_models.CeUserProfile
        fields = ('major', 'avatar_large', 'avatar_medium', 'avatar_thumb')



class UserBasicSerializer(serializers.ModelSerializer):
    profile = UserProfileBasicSerializer()

    class Meta:
        model = user_models.CeUser
        fields = ('id', 'username', 'first_name', 'last_name', 'profile')



class UserInterestSerializer(serializers.ModelSerializer):
    class Meta:
        model = user_models.CeUserInterest
        fields = ('id', 'user', 'interest_type', 'datetime_created')



# for viewing who likes a single post, not total likes of a single user
# the post id is assumed
class UserLikeSerializer(serializers.ModelSerializer):
    user = UserBasicSerializer(required=False)

    class Meta:
            model = user_models.CeUserLike
            fields = ('id', 'user', 'datetime_created')



class UserSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = user_models.CeUserSettings
        fields = ('id', 'user')



class UserNotificationSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = user_models.CeUserNotificationSettings
        fields = ('id', 'user')



class UserTrophiesSerializer(serializers.ModelSerializer):
    class Meta:
        model = user_models.CeUserTrophies
        fields = ('id', 'user', 'type', 'datetime_created')



class UserFollowerSerializer(serializers.ModelSerializer):
    class Meta:
        model = user_models.CeUserFollow
        fields = ('id', 'follower_user', 'following_user', 'datetime_created')


class UserFollowingSerializer(serializers.ModelSerializer):
    class Meta:
        model = user_models.CeUserFollow
        fields = ('id', 'follower_user', 'following_user', 'datetime_created')


###### ------------------------------------------------------------------
######
######  DIETS
######
###### ------------------------------------------------------------------

# for /me/diets/following to show the actual diets
class UserDietFollowSerializer(serializers.ModelSerializer):
    diet = serializers.SerializerMethodField('get_diet_details')

    class Meta:
        model = diet_models.CeDietFollow
        fields = ('id', 'diet', 'datetime_created')

    def get_diet_details( self, obj ):
        return obj.get_user_diet( serialize=True )




class DietFollowSerializer(serializers.ModelSerializer):
    user = UserBasicSerializer(required=False)
    #diet_user = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = diet_models.CeDietFollow
        fields = ('id', 'user', 'diet_user', 'datetime_created')


class DietSerializer(serializers.ModelSerializer):
    user = UserBasicSerializer(required=False)

    class Meta:
        model = diet_models.CeDiet
        fields = ('id', 'user', 'description', 'calories', 'fat', 'protein', 'carbs', 'datetime_updated')


# no user info
class DietBasicSerializer(serializers.ModelSerializer):
    class Meta:
        model = diet_models.CeDiet
        fields = ('id', 'description', 'calories', 'fat', 'protein', 'carbs', 'datetime_updated')





###### ------------------------------------------------------------------
######
######  WORKOUTS
######
###### ------------------------------------------------------------------


class WorkoutFollowSerializer(serializers.ModelSerializer):
    user = UserBasicSerializer(required=False)
    #workout = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = workout_models.CeWorkoutFollow
        fields = ('id', 'user', 'workout', 'datetime_created')


class WorkoutLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = workout_models.CeWorkoutLog
        fields = ('id', 'workout', 'reps', 'time', 'weight', 'weight_label', 'score', 'score_label', 'distance', 'distance_label', 'datetime_created')


class WorkoutSerializer(serializers.ModelSerializer):
    type = serializers.Field(source='get_workout_type')
    logs = serializers.SerializerMethodField('get_latest_log')
    score_label = serializers.SerializerMethodField('get_score_label')
    weight_label = serializers.SerializerMethodField('get_weight_label')
    distance_label = serializers.SerializerMethodField('get_distance_label')

    class Meta:
        model = workout_models.CeWorkout
        fields = ('id', 'user', 'type', 'name', 'score_label',  'weight_label', 'distance_label', 'datetime_updated', 'logs')

    def get_latest_log( self, obj ):
        return [ obj.get_latest_log( serialize=False ).pk ]

    def get_score_label( self, obj ):
        return obj.get_score_label_display()

    def get_weight_label( self, obj ):
        return obj.get_weight_label_display()
       
    def get_distance_label( self, obj ):
        return obj.get_distance_label_display()


class WorkoutDetailsSerializer(serializers.ModelSerializer):
    type = serializers.Field(source='get_workout_type')
    logentries = WorkoutLogSerializer(required=False) 

    class Meta:
        model = workout_models.CeWorkout
        fields = ('id', 'type', 'name', 'score_label', 'distance_label', 'datetime_updated', 'logentries')


# no user info
class WorkoutBasicSerializer(serializers.ModelSerializer):
    type = serializers.Field(source='get_workout_type')
    logs = serializers.SerializerMethodField('get_latest_log')
    score_label = serializers.SerializerMethodField('get_score_label')
    weight_label = serializers.SerializerMethodField('get_weight_label')
    distance_label = serializers.SerializerMethodField('get_distance_label')

    class Meta:
        model = workout_models.CeWorkout
        fields = ('id', 'type', 'name', 'score_label', 'weight_label', 'distance_label', 'datetime_updated', 'logs')

    def get_latest_log( self, obj ):
        return [ obj.get_latest_log( serialize=False ).pk ]

    def get_score_label( self, obj ):
        return obj.get_score_label_display()

    def get_weight_label( self, obj ):
        return obj.get_weight_label_display()
       
    def get_distance_label( self, obj ):
        return obj.get_distance_label_display()






###### ------------------------------------------------------------------
######
######  GROUPS
######
###### ------------------------------------------------------------------


class UserGroupAttendeeSerializer(serializers.ModelSerializer):
    user = UserBasicSerializer(required=False)

    class Meta:
        model = group_models.CeGroupAttendee
        fields = ('id', 'user', 'approved', 'datetime_updated', 'datetime_created')


class UserGroupInstanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = group_models.CeGroupInstance
        fields = ('id', 'location', 'event_datetime', 'completed', 'notes')


class UserGroupInstanceAttendeesSerializer(serializers.ModelSerializer):
    attendees = serializers.SerializerMethodField('get_attendees')

    class Meta:
        model = group_models.CeGroupInstance
        fields = ('id', 'location', 'event_datetime', 'completed', 'attendees', 'notes')

    def get_attendees( self, obj ):
        return obj.get_attendees( serialize=True )


class UserGroupSerializer(serializers.ModelSerializer):
    creator = UserBasicSerializer(required=False)
    instances = serializers.SerializerMethodField('get_instances')

    class Meta:
        model = group_models.CeGroup
        fields = ('id', 'name', 'description', 'location', 'category', 'max_attendees', 'creator', 'datetime_created', 'instances')

    def get_instances( self, obj ):
        return obj.get_instances( attendees=False, serialize=True )


class UserGroupDetailsSerializer(serializers.ModelSerializer):
    creator = UserBasicSerializer(required=False)
    instances = serializers.SerializerMethodField('get_instances')

    class Meta:
        model = group_models.CeGroup
        fields = ('id', 'name', 'description', 'location', 'category', 'max_attendees', 'creator', 'datetime_created', 'instances')

    def get_instances( self, obj ):
        return obj.get_instances( attendees=True, serialize=True )



###### ------------------------------------------------------------------
######
######  POSTS
######
###### ------------------------------------------------------------------


####
####  Global post wrapper serializer
####

class PostSerializer(serializers.ModelSerializer):
    creator = UserBasicSerializer(required=False)
    data = serializers.SerializerMethodField('get_data')
    likes = serializers.SerializerMethodField('get_likes')
    comments = serializers.SerializerMethodField('get_comments')

    class Meta:
        model = post_models.CePost
        fields = ('id', 'creator', 'post_type', 'post_id', 'datetime_created',
            'datetime_updated', 'edited', 'data', 'likes', 'comments')

    # get the post's data, which needs to self-determinate it's own serializer
    def get_data( self, obj ):
        return obj.get_data( serialize=True )

    def get_likes( self, obj ):
        return obj.get_likes( serialize=True )

    def get_comments( self, obj ):
        return obj.get_comments( serialize=True )




####
####  These are the separate serializers for each post type
####

class PhotoPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = post_models.CePhoto
        fields = ('id', 'caption')


class VideoPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = post_models.CeVideo
        fields = ('id', 'caption', 'video_id', 'video_host')


class StatusPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = post_models.CeStatus
        fields = ('id', 'caption')


class LinkPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = post_models.CeLink
        fields = ('id', 'caption')


class PostCommentSerializer(serializers.ModelSerializer):
    user = UserBasicSerializer(required=False)
    edited = serializers.Field(source='is_edited')

    class Meta:
        model = post_models.CeComment
        fields = ('id', 'user', 'comment', 'edited')


# when we get the post model first, as opposed to the comment model
class PostCommentReverseSerializer(serializers.ModelSerializer):
    class Meta:
        model = post_models.CeComment
        fields = ['comment']


from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework import authentication, permissions, generics
from rest_framework.decorators import api_view

from django.core.paginator import Paginator

from ce_user import models as user_models, forms as user_forms
from ce_post import models as post_models, forms as post_forms
from ce_workout import models as workout_models, forms as workout_forms
from ce_diet import models as diet_models, forms as diet_forms
from ce_group import models as group_models, forms as group_forms
from ce_messaging import models as messaging_models, forms as messaging_forms
from ce_leaderboard import models as  leaderboard_models

from ce_api import serializers as s


'''
    # auth fields
    
    /me
    /me/profile
    /me/settings
    /me/likes
    /me/posts
    /me/posts/[id]
    /me/diet
    /me/followers
    /me/following
    /me/following/[id]
    /me/interests
    /me/trophies
    /me/workouts
    /me/workouts/[id]
    /me/workouts/[id]/log
    /me/workouts/[id]/log/[id]
    /me/diets/following
    /me/workouts/following
    

'''

###### ------------------------------------------------------------------
######
######  Auth'd user - varaible endpoints
######
###### ------------------------------------------------------------------

# /me
class MyUserDetails(APIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
   
    def get(self, request, format=None):
        try:
            u = user_models.CeUser.objects.get(pk=request.user.pk)
            return Response( s.UserSerializer( u ).data )
        except user_models.CeUser.DoesNotExist:
            return Response( {}, status=status.HTTP_400_BAD_REQUEST )


# /me/profile
class MyUserProfileDetails( APIView ):
    permission_classes = [
        permissions.IsAuthenticated
    ]
   
    def get(self, request, format=None):
        u = user_models.CeUserProfile.objects.get(user_id=request.user.pk)
        return Response( s.UserProfileSerializer( u ).data )

    def post(self, request, format=None):
        u = user_models.CeUserProfile.objects.get(user_id=request.user.pk)
        return Response( s.UserProfileSerializer( u ).data )



# /me/settings
class MyUserSettingsDetails( APIView ):
    permission_classes = [
        permissions.IsAuthenticated
    ]
   
    def get(self, request, format=None):
        n1 = user_models.CeUserSettings.objects.get(user_id=request.user.pk)
        s1 = s.UserSettingsSerializer( n1 ).data
        n2 = user_models.CeUserNotificationSettings.objects.get(user_id=request.user.pk)
        s2 = s.UserNotificationSettingsSerializer( n2 ).data
        return Response( { "settings": s1, "notification_settings": s2 } )

    def post(self, request, format=None):
        n1 = user_models.CeUserSettings.objects.get(user_id=request.user.pk)
        s1 = s.UserSettingsSerializer( n1 ).data
        n2 = user_models.CeUserNotificationSettings.objects.get(user_id=request.user.pk)
        s2 = s.UserNotificationSettingsSerializer( n2 ).data
        return Response( { "settings": s1, "notification_settings": s2 } )



# /me/posts
class MyUserPostList( generics.ListCreateAPIView ):
    permission_classes = [
        permissions.IsAuthenticated
    ]
   
    model = post_models.CePost
    serializer_class = s.PostSerializer

    def get_queryset(self):
        queryset = super(MyUserPostList, self).get_queryset()
        return queryset.filter(creator_id=self.request.user.pk).exclude(post_type=10, deleted=True).order_by('-datetime_created')


    def create( request, *args, **kwargs ):
        rq = args[0]
        postdata = rq.DATA.copy()
        post_type = int( postdata['post_type'] )

        postdata['creator_id'] = rq.user.pk

        data_dict = { "user_id": rq.user.pk }
        for key, i in postdata.iteritems():
            if 'data--' in key:
                data_dict[key.replace('data--', '')] = i


        # TEMP for status post
        
        ps = post_models.CeStatus()
        ps.caption = data_dict['caption']
        ps.user_id = data_dict['user_id']
        ps.save()

        p = post_models.CePost()
        p.post_type = post_type
        p.creator_id = postdata['creator_id']
        p.post_id = ps.pk
        p.save()

        return Response( s.PostSerializer ( p ).data )



# /me/posts/[id]
class MyUserPostDetails(generics.RetrieveUpdateDestroyAPIView):
    model = post_models.CePost
    serializer_class = s.PostSerializer
    permission_classes = [
        permissions.IsAuthenticated
    ]



# /user/likes
class MyUserLikesList(generics.ListAPIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
   
    model = user_models.CeUserLike
    serializer_class = s.PostSerializer

    def get_queryset(self):
        queryset = super(MyUserLikesList, self).get_queryset()
        likes = queryset.filter(user_id=self.request.user.pk).order_by('-datetime_created').select_related('post')
        returned = []
        for l in likes:
            returned.append( l.post )
        return returned


# /user/interests
class MyUserInterestList(generics.ListAPIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
   
    model = user_models.CeUserInterest
    serializer_class = s.UserInterestSerializer

    def get_queryset(self):
        queryset = super(MyUserInterestList, self).get_queryset()
        return queryset.filter(user_id=self.request.user.pk)

    def create( request, *args, **kwargs ):
        rq = args[0]
        f = user_forms.UserInterestForm( rq.DATA )
        if f.is_valid():
            newlist = f.save_data( rq.user )
            resp = {"status": "success"}
            resp["data"] = s.UserInterestSerializer( newlist ).data
            return Response( resp )
        else:
            return Response( {"status": "error"} )



# /me/followers
# orders by most recent by default
class MyUserFollowerList(generics.ListAPIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
   
    model = user_models.CeUserFollow
    serializer_class = s.UserFollowerSerializer

    def get_queryset(self):
        sort = self.request.QUERY_PARAMS.get('sort', None)

        try:
            u = user_models.CeUser.objects.get(pk=self.request.user.pk)
            if sort == 'az':
                returned = u.followers_sorted()
            else:
                returned = u.followers_newest()
            return returned 
        except user_models.CeUser.DoesNotExist:
            return []


# /me/following
# orders by most recent by default
class MyUserFollowingList(generics.ListAPIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
   
    model = user_models.CeUserFollow
    serializer_class = s.UserFollowingSerializer

    def get_queryset(self):
        sort = self.request.QUERY_PARAMS.get('sort', None)

        try:
            u = user_models.CeUser.objects.get(pk=self.request.user.pk)
            if sort == 'az':
                returned = u.following_sorted()
            else:
                returned = u.following_newest()
            return returned 
        except user_models.CeUser.DoesNotExist:
            return []

    def create( request, *args, **kwargs ):
        rq = args[0]
        f = user_forms.UserFollowForm( rq.DATA )
        if f.is_valid():
            status = f.save_data( rq.user )
            return Response( status )
        else:
            return Response( {"status": "error"} )


# /me/following/[id]
class MyUserFollowingDetails( APIView ):
    permission_classes = [
        permissions.IsAuthenticated
    ]
   
    def delete(self, request, uid, format=None):
        try:
            uf = user_models.CeUserFollow.objects.get( follower_user_id=request.user.pk, following_user_id=int(uid) )
            uf.delete()
            return Response( {"status": "success"} )
        except user_models.CeUserFollow.DoesNotExist:
            return Response( {"status": "error"} )



# /me/diet
# also allows updating
class MyUserDietDetails(APIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
   
    def get_object(self, pk):
        try:
            u = user_models.CeUser.objects.get(pk=pk)   
            d = u.get_diet()
            if d is None:
                return Response( {}, status=status.HTTP_400_BAD_REQUEST )
            else:
                return d
        except user_models.CeUser.DoesNotExist:
            return Response( {}, status=status.HTTP_400_BAD_REQUEST )
            

    def get(self, request, format=None):
        diet = self.get_object( request.user.pk )
        serializer = s.DietBasicSerializer( diet )
        return Response( serializer.data )

    def post(self, request, format=None):
        d = diet_models.CeDiet()
        d.description = request.DATA['description']
        d.calories = request.DATA['calories']
        d.fat = request.DATA['fat']
        d.protein = request.DATA['protein']
        d.carbs = request.DATA['carbs']
        d.user_id =  request.user.pk
        d.save()

        diet = self.get_object( request.user.pk )
        serializer = s.DietBasicSerializer( diet )
        return Response( serializer.data )



# /me/diet/log
class MyUserDietLogList(generics.ListAPIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]

    model = diet_models.CeDiet
    serializer_class = s.DietBasicSerializer

    def get_queryset(self):
        queryset = super(MyUserDietLogList, self).get_queryset()
        return queryset.filter(user_id=self.request.user.pk)



# /me/workouts
class MyUserWorkoutsList(generics.ListCreateAPIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
   
    model = workout_models.CeWorkout
    serializer_class = s.WorkoutBasicSerializer

    def get_queryset(self):
        queryset = super(MyUserWorkoutsList, self).get_queryset()
        return queryset.filter(user_id=self.request.user.pk)



# /me/workouts/[id]
# View workout details
class MyUserWorkoutsDetails(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
   
    model = workout_models.CeWorkout
    serializer_class = s.WorkoutDetailsSerializer

    def update(request, *args, **kwargs):
        rq = args[0]
        postdata = rq.DATA

        # otherwise, we are updating the workout info 
        f = workout_forms.WorkoutForm( postdata )
        if f.is_valid():
            status = f.update_data( kwargs['pk'] )
            return Response( s.WorkoutDetailsSerializer( status ).data )
        else:
            return Response( {"status": "error"} )


    def destroy(request, *args, **kwargs):
        try:
            workout_models.CeWorkout.objects.get(pk=kwargs['pk'], user_id=rq.user.pk).delete()
            return Response( {"status":"success"} )
        except workout_models.CeWorkout.DoesNotExist:
            return Response( {"status":"error"} )



# /me/workouts/[id]/log
# Logging portal for a workout
class MyUserWorkoutsLogList(generics.ListCreateAPIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
   
    model = workout_models.CeWorkoutLog
    serializer_class = s.WorkoutLogSerializer

    def create( request, *args, **kwargs ):
        rq = args[0]
        postdata = rq.DATA

        f = workout_forms.WorkoutLogForm( postdata )
        if f.is_valid():
            status = f.save_data( kwargs['pk'] )
            return Response( { "status": "success", "data": s.WorkoutLogSerializer( status ).data } )
        else:
            return Response( {"status": "error"} )



# /me/workouts/[id]/log/[id]
# Edit and delete log details
class MyUserWorkoutsLogDetails(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
   
    model = workout_models.CeWorkoutLog
    serializer_class = s.WorkoutLogSerializer

    def retrieve(request, *args, **kwargs):
        try:
            log = workout_models.CeWorkoutLog.objects.get(workout_id=kwargs['pk'], pk=kwargs['wid'])
            return Response( s.WorkoutLogSerializer( log ).data )
        except workout_models.CeWorkoutLog.DoesNotExist:
            return Response( {}, status=status.HTTP_400_BAD_REQUEST )


    def update( request, *args, **kwargs ):
        rq = args[0]
        postdata = rq.DATA

        f = workout_forms.WorkoutLogForm( postdata )
        if f.is_valid():
            status = f.update_data( kwargs['wid'] )
            return Response( s.WorkoutLogSerializer( status ).data )
        else:
            return Response( {"status": "error"} )

    def destroy(request, *args, **kwargs):
        try:
            workout_models.CeWorkoutLog.objects.get(workout_id=kwargs['pk'], pk=kwargs['wid']).delete()
            return Response( {"status":"success"} )
        except workout_models.CeWorkoutLog.DoesNotExist:
            return Response( {"status":"error"} )



# /me/trophies
class MyUserTrophiesList( generics.ListAPIView ):
    permission_classes = [
        permissions.IsAuthenticated
    ]
   
    model = user_models.CeUserTrophies
    serializer_class = s.UserTrophiesSerializer

    def get_queryset(self):
        queryset = super(MyUserTrophiesList, self).get_queryset()
        return queryset.filter(user_id=self.request.user.pk)




# /me/workouts/following
# Workouts that are being followed by this user
class MyWorkoutsFollowingList( generics.ListAPIView ):
    permission_classes = [
        permissions.IsAuthenticated
    ]

    model = workout_models.CeWorkoutFollow
    serializer_class = s.WorkoutFollowSerializer

    def get_queryset(self):    
        try:
            workouts = user_models.CeUser.objects.get(pk=self.request.user.pk).followed_workouts.all()
            return workouts
        except user_models.CeUser.DoesNotExist:
            return Response( {}, status=status.HTTP_400_BAD_REQUEST )




# /me/diets/following
# Diets that are being followed by this user
class MyDietsFollowingList( generics.ListAPIView ):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    
    model = diet_models.CeDietFollow
    serializer_class = s.UserDietFollowSerializer

    def get_queryset(self):    
        try:
            diets = list( user_models.CeUser.objects.get(pk=self.request.user.pk).followed_diets.all() )
            return diets
        except user_models.CeUser.DoesNotExist:
            return Response( {}, status=status.HTTP_400_BAD_REQUEST )





class MyGroupsList( generics.ListAPIView ):
    permission_classes = [
        permissions.IsAuthenticated
    ]

    model = group_models.CeGroup
    serializer_class = s.UserGroupSerializer

    def get_queryset(self):    
        queryset = super(MyGroupsList, self).get_queryset()
        return queryset.filter(creator_id=self.request.user.pk)


class MyGroupsDetails( generics.RetrieveAPIView ):
    permission_classes = [
        permissions.IsAuthenticated
    ]

    model = group_models.CeGroup
    serializer_class = s.UserGroupDetailsSerializer





from rest_framework import authentication
from rest_framework import exceptions
from ce_user.models import CeUser

'''
    Custom user authentication for the api
'''

class CeAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        try:
            uid = request.session['_auth_user_id']
            if not uid:
                return None
        except KeyError:
            return None

        try:
            user = CeUser.objects.get(pk=uid)
        except CeUser.DoesNotExist:
            raise exceptions.AuthenticationFailed('No such user')

        return (user, None)
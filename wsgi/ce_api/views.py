from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework import authentication, permissions, generics
from rest_framework.decorators import api_view

from django.core.paginator import Paginator

from ce_user import models as user_models, forms as user_forms
from ce_post import models as post_models, forms as post_forms
from ce_workout import models as workout_models, forms as workout_forms
from ce_diet import models as diet_models, forms as diet_forms
from ce_group import models as group_models, forms as group_forms
from ce_messaging import models as messaging_models, forms as messaging_forms
from ce_messaging.helpers import *
from ce_leaderboard import models as  leaderboard_models

from ce_api import serializers as s


'''
    Most or all of these view classes are using the 
    Django REST Framework generic view shortcut classes.
    
    Learn more: 
    http://www.django-rest-framework.org/api-guide/generic-views

    Uses POST in place of "CREATE" and "UPDATE" operations if possible. 
    Defaults to PUT if also updaing with POST
    Uses DELETE for deleting.

    All sorting is done in GET with query vars. ( /users/1/followers?sort=az )

    

    # public viewing fields

    + /users/[id]                     # [READ] a single user's details including profile
    + /users/[id]/posts               # [READ] a list of a users posts ordered by most recent
    + /users/[id]/likes               # [READ] a list of a user's liked posts ordered by most recently liked
    + /users/[id]/interests           # [READ] a user's interests (cardio, crossfit, lifting, etc)
    + /users/[id]/followers           # [READ] a user's followers
    + /users/[id]/following           # [READ] other people a user is following
    + /users/[id]/diet                # [READ] a user's current diet (most recent update - one per user)
    + /users/[id]/workouts            # [READ] a user's workouts and personal bests
    + /users/[id]/diets/following     # [READ] diets that a user follows
    + /users/[id]/workouts/following  # [READ] workouts that a user follows
    + /users/[id]/trophies            # [READ] Shows trophies given to a user
    + /users/                         # [ADMIN] a list of all users

    /posts/[id]                     # [READ/UPDATE/DELETE] details on one particular posts
    /posts/[id]/likes               # [CREATE/DELETE] Allows a user to like a post
    /posts/[id]/comments            # [READ/CREATE] View all comments on a post. Also so users can add a comment
    /posts/[id]/comments/[id]       # [UPDATE/DELETE] Edit or delete a comment
    + /posts/                         # [CREATE] Allows users to post a new update ( [ADMIN] a list of all posts )
  
    + /diets/[id]                     # [READ] A single diet's details by pk
    + /diets/[user_id]/followers      # [READ] a list of people who follow a particular user's diet
    + /diets/[user_id]/log            # [READ] a log of past changes for a user's diet
    + /diets/followers/[id]           # [DELETE] removes a diet follower by pk

    + /workouts/[id]                  # [READ] details on a single workout
    + /workouts/[id]/followers        # [READ] people who follow a single workout
    + /workouts/[id]/log              # [READ/UPDATE] a log of records for a particular workout
    + /workouts/followers/[id]        # [DELETE] removes a workout follower by pk

    /feed/all                       # [READ] a chronological feed made of posts without a following filter
    /feed/                          # [READ] the user's activity feed made of posts from people they follow

    /groups/[id]                    # [READ/UPDATE/DELETE]  Show details on a group instance (clicked calendar) / update or delete instance
    /groups/[id]/attendees          # [READ/CREATE] Show list of attendees / add unapproved instance of user
    /groups/[id]/attendees/[id]     # [UPDATE] Approve or disapprove of a user joining a group
    /groups/                        # [READ/CREATE] Show groups and filter groups, also create new groups

    /messages/                      # [CREATE] Send message to another user on the platform

    /leaderboard/                   # [READ] Shows all leaderboard rankings

'''









###### ------------------------------------------------------------------
######
######  Users and profile info
######
###### ------------------------------------------------------------------


# /users/
class UserList(generics.ListAPIView):
    model = user_models.CeUser
    serializer_class = s.UserSerializer
    permission_classes = [
        permissions.AllowAny
    ]


# /users/[id]
class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    model = user_models.CeUser
    serializer_class = s.UserSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly
    ]

    def get_object(self, pk):
        try:
            u = user_models.CeUser.objects.get(pk=self.kwargs.get('pk'))   
            return u
        except user_models.CeUser.DoesNotExist:
            return None
            

    def get(self, request, pk, format=None):
        u = self.get_object(pk)
        if u is None:
            return Response( {}, status=status.HTTP_400_BAD_REQUEST )
        serializer = s.UserSerializer( u )
        return Response( { "user": serializer.data } )


# /users/[id]/profile
class UserProfileDetail(generics.RetrieveUpdateDestroyAPIView):
    model = user_models.CeUserProfile
    serializer_class = s.UserProfileSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly
    ]

    def get_object(self, pk):
        try:
            u = user_models.CeUserProfile.objects.get(user_id=self.kwargs.get('pk'))   
            return u
        except user_models.CeUserProfile.DoesNotExist:
            return None
            

    def get(self, request, pk, format=None):
        u = self.get_object(pk)
        if u is None:
            return Response( {}, status=status.HTTP_400_BAD_REQUEST )
        serializer = s.UserProfileSerializer( u )
        return Response( { "userProfile": serializer.data } )



# /users/[id]/posts
class UserPostList(generics.ListAPIView):
    model = post_models.CePost
    serializer_class = s.PostSerializer

    def get_queryset(self):
        queryset = super(UserPostList, self).get_queryset()
        return queryset.filter(creator_id=self.kwargs.get('pk')).exclude(post_type=10, deleted=True).order_by('-datetime_created')




# /user/[id]/likes
class UserLikesList(generics.ListAPIView):
    model = user_models.CeUserLike
    serializer_class = s.PostSerializer

    def get_queryset(self):
        queryset = super(UserLikesList, self).get_queryset()
        likes = queryset.filter(user_id=self.kwargs.get('pk')).order_by('-datetime_created').select_related('post')
        returned = []
        for l in likes:
            returned.append( l.post )
        return returned





# /users/[id]/diet
class UserDietDetail(APIView):
    def get_object(self, pk):
        try:
            u = user_models.CeUser.objects.get(pk=self.kwargs.get('pk'))   
            d = u.get_diet()
            return d
        except user_models.CeUser.DoesNotExist:
            return None
            

    def get(self, request, pk, format=None):
        diet = self.get_object(pk)
        if diet is None:
            return Response( {}, status=status.HTTP_400_BAD_REQUEST )
        serializer = s.DietBasicSerializer( diet )
        return Response( serializer.data )




# /users/[id]/workouts
class UserWorkoutsList(generics.ListAPIView):
    model = workout_models.CeWorkout
    serializer_class = s.WorkoutBasicSerializer

    def get_queryset(self):
        queryset = super(UserWorkoutsList, self).get_queryset()
        return queryset.filter(user_id=self.kwargs.get('pk'))



# /user/[id]/diets/following
# Diets that are being followed by a single user
class UserDietsFollowingList(generics.ListAPIView):
    model = diet_models.CeDiet
    serializer_class = s.DietSerializer

    def get_queryset(self):    
        try:
            diets = list( user_models.CeUser.objects.get(pk=self.kwargs.get('pk')).followed_diets.all() )
            diet_list = []
            for d in diets:
                diet_list.append( d.diet_user.get_diet() )
            return diet_list
        except user_models.CeUser.DoesNotExist:
            return Response( {}, status=status.HTTP_400_BAD_REQUEST )



# /user/[id]/workouts/following
# Workouts that are being followed by a single user
class UserWorkoutsFollowingList( generics.ListAPIView ):
    model = workout_models.CeWorkout
    serializer_class = s.WorkoutSerializer

    def get_queryset(self):    
        try:
            workouts = list( user_models.CeUser.objects.get(pk=self.kwargs.get('pk')).followed_workouts.all() )
            workouts_list = []
            for w in workouts:
                workouts_list.append( w.workout )
            return workouts_list
        except user_models.CeUser.DoesNotExist:
            return Response( {}, status=status.HTTP_400_BAD_REQUEST )



class UserSettingsDetail( APIView ):

    def get(self, request, pk, format=None):
        try:
            u = user_models.CeUser.objects.get(pk=pk)   
            settings = u.settings
            serializer1 = s.UserSettingsSerializer( settings )
            not_settings = u.notification_settings
            serializer2 = s.UserNotificationSettingsSerializer( not_settings )
            return Response( {"settings": serializer1.data, "notifcation_settings": serializer2.data }  )
        except user_models.CeUser.DoesNotExist:
            return Response( {}, status=status.HTTP_400_BAD_REQUEST )


    def post(self, request, pk, format=None):
        return {}


class UserProfileDetail( generics.RetrieveUpdateAPIView ):
    model = user_models.CeUserProfile
    serializer_class = s.UserProfileSerializer


class UserTrophiesList( generics.ListAPIView ):
    model = user_models.CeUserTrophies
    serializer_class = s.UserTrophiesSerializer

    def get_queryset(self):
        queryset = super(UserTrophiesList, self).get_queryset()
        return queryset.filter(user_id=self.kwargs.get('pk'))



class UserGroupsList( generics.ListAPIView ):
    permission_classes = [
        permissions.IsAuthenticated
    ]

    model = group_models.CeGroup
    serializer_class = s.UserGroupSerializer

    def get_queryset(self):    
        queryset = super(UserGroupsList, self).get_queryset()
        return queryset.filter(creator_id=self.kwargs.get('pk'))



###### ------------------------------------------------------------------
######
######  Diets
######
###### ------------------------------------------------------------------


# calling a diet by it's id
class DietsDetail(generics.RetrieveUpdateDestroyAPIView):
    model = diet_models.CeDiet
    serializer_class = s.DietSerializer



# For deleting only
class DietDeleteFollowDetail(generics.DestroyAPIView):
    model = diet_models.CeDietFollow
    serializer_class = s.DietFollowSerializer




class DietsFollowersList(generics.ListCreateAPIView):
    model = diet_models.CeDietFollow
    serializer_class = s.DietFollowSerializer

    def get_queryset(self):
        queryset = super(DietsFollowersList, self).get_queryset()
        follows = queryset.filter(diet_user_id=self.kwargs.get('pk')).order_by('-datetime_created').select_related('user')
        return follows


    def create( request, *args, **kwargs ):
        f = diet_forms.DietFollowForm( args[0].DATA )
        if f.is_valid():
            return Response( f.save_data() )
        else:
            return Response( {"status": "error"} )



class DietsLogList(generics.ListAPIView):
    model = diet_models.CeDiet
    serializer_class = s.DietBasicSerializer

    def get_queryset(self):
        queryset = super(DietsLogList, self).get_queryset()
        return queryset.filter(user_id=self.kwargs.get('pk'))





###### ------------------------------------------------------------------
######
######  Workouts
######
###### ------------------------------------------------------------------






class WorkoutsDetail(generics.RetrieveUpdateDestroyAPIView):
    model = workout_models.CeWorkout
    serializer_class = s.WorkoutSerializer



# For deleting only
class WorkoutDeleteFollowDetail(generics.DestroyAPIView):
    model = workout_models.CeWorkoutFollow
    serializer_class = s.WorkoutFollowSerializer



class WorkoutsFollowersList(generics.ListAPIView):
    model = workout_models.CeWorkoutFollow
    serializer_class = s.WorkoutFollowSerializer

    def get_queryset(self):
        queryset = super(WorkoutsFollowersList, self).get_queryset()
        follows = queryset.filter(workout_id=self.kwargs.get('pk')).order_by('-datetime_created').select_related('user')
        return follows



class WorkoutsLogList(generics.ListAPIView):
    model = workout_models.CeWorkoutLog
    serializer_class = s.WorkoutLogSerializer

    def get_queryset(self):
        queryset = super(WorkoutsLogList, self).get_queryset()
        return queryset.filter(workout_id=self.kwargs.get('pk'))





###### ------------------------------------------------------------------
######
######  Posts, userPosts, and userLikes
######
###### ------------------------------------------------------------------



class PostList(generics.ListCreateAPIView):
    model = post_models.CePost
    serializer_class = s.PostSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly
    ]

    def get_queryset(self):
        queryset = super(PostList, self).get_queryset()
        return queryset.exclude(post_type=10, deleted=True).order_by('-datetime_created')


class PostDetail(generics.RetrieveUpdateDestroyAPIView):
    model = post_models.CePost
    serializer_class = s.PostSerializer
    permission_classes = [
        permissions.AllowAny
    ]



class PostLikesList( APIView ):
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly
    ]

    def get( self, request, pk, format=None ):
        try:
            p = post_models.CePost.objects.get(pk=pk)
            likes = p.get_likes( serialize=True )
            return Response( likes )
        except post_models.CePost.DoesNotExist:
            return Response( {}, status=status.HTTP_400_BAD_REQUEST )
        


class PostLikesCreate( generics.CreateAPIView ):
    model = user_models.CeUserLike

    def create( request, *args, **kwargs ):
        rq = args[0]
        f = user_forms.UserLikeForm( rq.DATA )
        if f.is_valid():
            status = f.save_data( rq.user )
            return Response( status )
        else:
            return Response( {"status": "error"} )


class PostCommentsCreate( generics.CreateAPIView ):
    model = post_models.CeComment

    def create( request, *args, **kwargs ):
        rq = args[0]
        f = post_forms.CommentForm( rq.DATA )
        if f.is_valid():
            status = f.save_data( rq.user )
            return Response( status )
        else:
            return Response( {"status": "error"} )



class PostCommentsList( APIView ):
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly
    ]

    def get( self, request, pk, format=None ):
        try:
            p = post_models.CePost.objects.get(pk=pk)
            comments = p.get_comments( serialize=True )
            return Response( comments )
        except post_models.CePost.DoesNotExist:
            return Response( {}, status=status.HTTP_400_BAD_REQUEST )



class PostCommentsDetail( APIView ):
    def get(self, request, pk, cid, format=None):
        try:
            c = post_models.CeComment.objects.get(pk=cid)
            ser = s.PostCommentSerializer( c )
            return Response( ser.data )
        except post_models.CeComment.DoesNotExist:
            return Response( {}, status=status.HTTP_400_BAD_REQUEST )



###### ------------------------------------------------------------------
######
######  Feed
######
###### ------------------------------------------------------------------




class FeedList(generics.ListAPIView):
    model = post_models.CePost
    serializer_class = s.PostSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly
    ]



class FeedAllList(generics.ListAPIView):
    model = post_models.CePost
    serializer_class = s.PostSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly
    ]



###### ------------------------------------------------------------------
######
######  Followers/Following
######
###### ------------------------------------------------------------------

# /follows?id=user&type=[following,followers]
# orders by most recent by default
class FollowList(generics.ListCreateAPIView):
    model = user_models.CeUserFollow
    serializer_class = s.UserFollowerSerializer

    def get_queryset(self):
        sort = self.request.QUERY_PARAMS.get('sort', None)
        uid = self.kwargs.get('pk')
        ftype = self.request.QUERY_PARAMS.get('type', 'following')

        try:
            u = user_models.CeUser.objects.get(pk=uid)

            if ftype == 'followers':
                if sort == 'az':
                    returned = u.followers_sorted()
                else:
                    returned = u.followers_newest()
            else:
                if sort == 'az':
                    returned = u.following_sorted()
                else:
                    returned = u.following_newest()
            return returned 
        except user_models.CeUser.DoesNotExist:
            return []

    def create( request, *args, **kwargs ):
        rq = args[0]
        f = user_forms.UserFollowForm( rq.DATA )
        if f.is_valid():
            status = f.save_data( rq.user )
            return Response( status )
        else:
            return Response( {"status": "error"} )


###### ------------------------------------------------------------------
######
######  Interests
######
###### ------------------------------------------------------------------


# /interests
class InterestList(generics.ListCreateAPIView):
    model = user_models.CeUserInterest
    serializer_class = s.UserInterestSerializer

    def get_queryset(self):
        uid = self.kwargs.get('pk')
        
        queryset = super(InterestList, self).get_queryset()
        return queryset.filter(user_id=uid)

    def create( request, *args, **kwargs ):
        rq = args[0]
        f = user_forms.UserInterestForm( rq.DATA )
        if f.is_valid():
            newlist = f.save_data( rq.user )
            resp = {"status": "success"}
            resp["data"] = s.UserInterestSerializer( newlist ).data
            return Response( resp )
        else:
            return Response( {"status": "error"} )




###### ------------------------------------------------------------------
######
######  Groups
######
###### ------------------------------------------------------------------


class GroupsList(generics.ListAPIView):
    model = group_models.CeGroup
    serializer_class = s.UserGroupSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly
    ]


class GroupsDetails(generics.RetrieveAPIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]

    model = group_models.CeGroup
    serializer_class = s.UserGroupDetailsSerializer


class GroupsAttendeesCreate(generics.CreateAPIView):
    model = group_models.CeGroupAttendee
    serializer_class = s.UserGroupInstanceAttendeesSerializer

    def create( request, *args, **kwargs ):
        rq = args[0]
        f = group_forms.GroupAttendForm( rq.DATA )
        if f.is_valid():
            status = f.save_data( rq.user )
            return Response( status )
        else:
            return Response( {"status": "error"} )




###### ------------------------------------------------------------------
######
######  Messages
######
###### ------------------------------------------------------------------




###### ------------------------------------------------------------------
######
######  Leaderboard
######
###### ------------------------------------------------------------------

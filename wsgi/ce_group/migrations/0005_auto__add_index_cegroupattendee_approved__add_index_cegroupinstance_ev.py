# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding index on 'CeGroupAttendee', fields ['approved']
        db.create_index(u'ce_group_cegroupattendee', ['approved'])

        # Adding index on 'CeGroupInstance', fields ['event_datetime']
        db.create_index(u'ce_group_cegroupinstance', ['event_datetime'])

        # Adding index on 'CeGroup', fields ['category']
        db.create_index(u'ce_group_cegroup', ['category'])

        # Adding index on 'CeGroup', fields ['level']
        db.create_index(u'ce_group_cegroup', ['level'])


    def backwards(self, orm):
        # Removing index on 'CeGroup', fields ['level']
        db.delete_index(u'ce_group_cegroup', ['level'])

        # Removing index on 'CeGroup', fields ['category']
        db.delete_index(u'ce_group_cegroup', ['category'])

        # Removing index on 'CeGroupInstance', fields ['event_datetime']
        db.delete_index(u'ce_group_cegroupinstance', ['event_datetime'])

        # Removing index on 'CeGroupAttendee', fields ['approved']
        db.delete_index(u'ce_group_cegroupattendee', ['approved'])


    models = {
        u'ce_group.cegroup': {
            'Meta': {'ordering': "['-datetime_created']", 'object_name': 'CeGroup'},
            'category': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'creator': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'groups'", 'to': u"orm['ce_user.CeUser']"}),
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '9999'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.IntegerField', [], {'default': '1', 'db_index': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'max_attendees': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'private': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'withuser': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ce_user.CeUser']", 'null': 'True'})
        },
        u'ce_group.cegroupattendee': {
            'Meta': {'ordering': "['-approved', '-datetime_updated']", 'object_name': 'CeGroupAttendee'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'datetime_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'attendees'", 'to': u"orm['ce_group.CeGroupInstance']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_group.cegroupinstance': {
            'Meta': {'ordering': "['event_datetime']", 'object_name': 'CeGroupInstance'},
            'cancelled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'completed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'event_datetime': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'instances'", 'to': u"orm['ce_group.CeGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'notes': ('django.db.models.fields.TextField', [], {'max_length': '9999'})
        },
        u'ce_group.cegroupinvite': {
            'Meta': {'object_name': 'CeGroupInvite'},
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'datetime_sent': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'invitations'", 'to': u"orm['ce_group.CeGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'ce_user.ceuser': {
            'Meta': {'object_name': 'CeUser'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'followers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'following'", 'symmetrical': 'False', 'through': u"orm['ce_user.CeUserFollow']", 'to': u"orm['ce_user.CeUser']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'verified': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'ce_user.ceuserfollow': {
            'Meta': {'ordering': "['-datetime_created']", 'object_name': 'CeUserFollow'},
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'follower_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'follower_user'", 'to': u"orm['ce_user.CeUser']"}),
            'following_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'following_user'", 'to': u"orm['ce_user.CeUser']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['ce_group']
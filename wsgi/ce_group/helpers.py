from ce_group.models import *
import datetime, random

'''
	gets suggested eventsjhgdf 
'''

def GroupHelper_SuggestedGroups( user, limit=2 ):
	today = datetime.datetime.now()
	groups = CeGroupAttendee.objects.filter(user=user, group__event_datetime__gt=today, group__group__creator__verified=True, group__group__creator__deleted=False).select_related('group')
	groups_list = []
	for g in groups:
		groups_list.append( g.group.group_id )

	groups = list( CeGroupInstance.objects.filter(group__private=False, cancelled=False, event_datetime__gt=today, group__deleted=False, group__creator__verified=True, group__creator__deleted=False).exclude(group__creator_id=user.pk).exclude(group_id__in=groups_list).select_related('group').order_by('?') )
	random.shuffle( groups )
	return groups[:limit]

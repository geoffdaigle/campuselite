from django import forms as f
from ce_group.models import *
from ce_messaging.helpers import *
from django.forms import widgets

'''
    Data validation forms related to this app
'''

class GroupForm(f.ModelForm):

    max_attendees = f.IntegerField( min_value=1 )

    class Meta:
        model = CeGroup
        fields = ['name', 'description', 'location', 'category', 'level', 'max_attendees', 'private']
        widgets = {
            'private': widgets.HiddenInput(),
        }
    
    def save_data( self, user ):
        group = self.save(commit=False)
        group.creator = user
        group.save()
        return group


class GroupInstanceForm(f.ModelForm):
    class Meta:
        model = CeGroupInstance
        fields = ['notes', 'location', 'event_datetime']
    
    def save_data( self, group ):
        groupInstance = self.save(commit=False)
        groupInstance.group = grou
        groupInstance.save()
        return groupInstance


class GroupAttendForm(f.Form):
    approved = f.BooleanField( required=False )
    group_id = f.IntegerField( min_value=1 )
    attend_id = f.IntegerField( required=False )
    user_id = f.IntegerField( min_value=1 )
    remove = f.BooleanField( required=False )

    def save_data( self, user ):
        cd = self.cleaned_data
        status_json = { "status": "success" }

        if cd['remove']:
            try:
                CeGroupAttendee.objects.get(pk=cd['attend_id']).delete()
            except CeGroupAttendee.DoesNotExist:
                pass
        else:
            if cd['approved']:
                try:
                    a = CeGroupAttendee.objects.get(pk=cd['attend_id'])
                except CeGroupAttendee.DoesNotExist:
                    return { "status": "error" }

                a.approved = True
                a.save()

                GroupAttendApproveNotification( a.group.group, a.user )

            else:
                a = CeGroupAttendee()
                a.user_id = user.pk
                a.group_id = cd['group_id']
                a.save()
                status_json['id'] = a.pk

                GroupAttendRequestNotification( a.group.group, user )

        return status_json
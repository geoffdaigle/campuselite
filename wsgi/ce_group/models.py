from django.db import models as m
from ce_user.fixtures import *
from campuselite import settings

CE_GROUP_LEVEL_ANYONE = 1
CE_GROUP_LEVEL_BEGINNER = 2
CE_GROUP_LEVEL_INTERMEDIATE = 3
CE_GROUP_LEVEL_ADVANCED = 4
CE_GROUP_LEVEL_CHOICES = (
    (CE_GROUP_LEVEL_ANYONE, 'Anyone'),
    (CE_GROUP_LEVEL_BEGINNER, 'Beginner'),
    (CE_GROUP_LEVEL_INTERMEDIATE, 'Intermediate'),
    (CE_GROUP_LEVEL_ADVANCED, 'Advanced'),
)


class CeGroup( m.Model ):
    creator = m.ForeignKey('ce_user.CeUser', related_name="groups")
    name = m.CharField(max_length=100)
    description = m.TextField(max_length=9999)
    location = m.CharField(max_length=100)
    category = m.IntegerField(choices=CE_GROUP_TYPE_CHOICES, db_index=True)
    level = m.IntegerField(default=1, choices=CE_GROUP_LEVEL_CHOICES, db_index=True)
    max_attendees = m.IntegerField(default=1)
    datetime_created = m.DateTimeField(auto_now_add=True)
    deleted = m.BooleanField( default=False ) 
    private = m.BooleanField( default=False ) 
    withuser = m.ForeignKey('ce_user.CeUser', null=True)

    class Meta:
        ordering = ['-datetime_created']

    def details_path( self ):
        return '/events/' + str( self.pk )

    def details_path_absolute( self ):
        return settings.BASE_URL+'events/' + str( self.pk )

    def details_path_absolute_invite( self ):
        return settings.BASE_URL+'events/' + str( self.pk )+'/invite?s=1'

    def get_instances( self, serialize=False, attendees=False ):
    	from ce_api import serializers as s
    	if not attendees:
    		return s.UserGroupInstanceSerializer( self.instances.all() ).data
    	else:
    		return s.UserGroupInstanceAttendeesSerializer( self.instances.all() ).data


class CeGroupInstance( m.Model ):
    group = m.ForeignKey( CeGroup, related_name="instances")
    notes = m.TextField(max_length=9999)
    location = m.CharField(max_length=100)
    event_datetime = m.DateTimeField(db_index=True) #when the event is actually occuring
    completed = m.BooleanField( default=False )
    datetime_created = m.DateTimeField(auto_now_add=True)
    cancelled = m.BooleanField( default=False ) 

    class Meta:
        ordering = ['event_datetime']

    def get_attendees( self, serialize=False ):
    	from ce_api import serializers as s
    	return s.UserGroupAttendeeSerializer( self.attendees.all() ).data

    def can_join( self ):
        max_size = self.group.max_attendees
        return True if max_size > self.attendees.filter(approved=True).count() else False

    def get_approved( self ):
        return self.attendees.filter(approved=True)
       

    def get_pending( self ):
        return self.attendees.filter(approved=False)
        

    def is_attending( self, user ):
        status = {
            "pending": False,
            "attending": False,
            "id": None,
        }
        try:
            a = CeGroupAttendee.objects.get( group_id=self.pk, user=user )
            status['id'] = a.pk
            if a.approved:
                status['attending'] = True
            else:
                status['pending'] = True
        except CeGroupAttendee.DoesNotExist:
            pass

        return status



class CeGroupAttendee( m.Model ):
    group = m.ForeignKey( CeGroupInstance, related_name="attendees")
    user = m.ForeignKey( 'ce_user.CeUser' )
    approved = m.BooleanField( default=False, db_index=True )
    datetime_updated = m.DateTimeField(auto_now=True)
    datetime_created = m.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-approved', '-datetime_updated']



class CeGroupInvite( m.Model ):
    group = m.ForeignKey( CeGroup, related_name="invitations")
    email = m.EmailField( verbose_name='email address', max_length=255, unique=True )
    datetime_created = m.DateTimeField(auto_now_add=True)
    datetime_sent = m.DateTimeField(null=True)



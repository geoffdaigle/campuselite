"""
    Setup files for Campus Elite on Unix-based systems

    To get the requirements: 
        pip freeze > requirements.txt
"""

try:
    from setuptools import setup, find_packages
except ImportError:
    from ez_setup import use_setuptools
    use_setuptools()
    from setuptools import setup, find_packages

setup(
    name='Campus Elite',
    version='0.1',
    description='',
    author='Geoff Daigle',
    author_email='',
    url='',
    install_requires=[
        "django==1.6.5",
        "django-extensions==1.3.3", # a bunch of manage.py extensions to make django easier. runserver_plus and shell_plus are very useful
        "werkzeug==0.9.3", # required by django_extensions to use runserver_plus. general wsgi library
        "django-debug-toolbar==0.11.0", # another django debug tool
        "mysql-python==1.2.3",
        "python-memcached==1.48", # used to cache stuff in memory
        "WebHelpers==1.3", # used for their pagination class.
        "pylint", # not necessary but a nice tool to evaluate code
        "pytz==2012j", # olson style timezone handling... e.g. America/New_York or America/Chicago
        "djangorestframework==2.3.14", # easy rest api creation
        "markdown==2.4.1", # for rest admin display
        "django-filter==0.7",  # for rest api
        "South==0.8.4", # for database migrations
        "Pillow==2.5.3", # Image manipulation library,
        "boto==2.32.1", # S3 buckets
        "django-storages==1.1.8",
    ],
    packages=find_packages(exclude=['ez_setup']),
    include_package_data=True,
    package_data={'Campus Elite': ['i18n/*/LC_MESSAGES/*.mo']},
)


from django.db import models as m
from campuselite import settings
from ce_user.models import *

CE_NOTIF_ICON_BUMP = 1
CE_NOTIF_ICON_COMMENT = 2
CE_NOTIF_ICON_MOTIVATE = 3
CE_NOTIF_ICON_EVENT = 4
CE_NOTIF_ICON_CHECK = 5
CE_NOTIF_ICON_TROPHY = 6
CE_NOTIF_ICON_CHOICES = (
	(CE_NOTIF_ICON_BUMP, '<img src="'+settings.STATIC_URL+'image/bump-grey.svg" style="height: 1em; position: relative; top: 1px" />'),
	(CE_NOTIF_ICON_COMMENT, '<i class="fa fa-comment"></i>'),
	(CE_NOTIF_ICON_MOTIVATE, '<i class="fa fa-plus"></i>'),
	(CE_NOTIF_ICON_EVENT, '<i class="fa fa-calendar-o"></i>'),
	(CE_NOTIF_ICON_CHECK, '<i class="fa fa-check-circle"></i>'),
	(CE_NOTIF_ICON_TROPHY, '<i class="fa fa-trophy"></i>'),
)


class CeNotification( m.Model ):
    user = m.ForeignKey('ce_user.CeUser', related_name="notifications")
    text = m.TextField() # includes user names and event names in bold
    icon = m.IntegerField(choices=CE_NOTIF_ICON_CHOICES)
    link = m.CharField(max_length=255)
    primary_image = m.CharField(max_length=255)
    preview_image = m.CharField(max_length=255)
    datetime_created = m.DateTimeField(auto_now_add=True)
    clicked = m.BooleanField( default=False )  # aka read
    shown = m.BooleanField( default=False ) 

    class Meta:
        ordering = ['-pk']



class EmailObject( m.Model ):
    html = m.TextField() 
    text = m.TextField() 
    subject = m.TextField() 
    fromuser = m.CharField(max_length=255) 
    to = m.CharField(max_length=255)
    datetime_created = m.DateTimeField(auto_now_add=True)
    sent = m.BooleanField( default=False ) 

    class Meta:
        ordering = ['-pk']
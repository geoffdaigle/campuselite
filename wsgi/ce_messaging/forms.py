from django import forms as f

'''
	Data validation forms related to this app
'''

class MessageForm(f.Form):
	user_id = f.IntegerField(min_value=1)

from campuselite import settings
from ce_post.models import *
from ce_messaging.models import *
from campuselite.shortcuts import *
import datetime


def MessageHelper_SendVerify( rq ):
    import hashlib
    passkey = hashlib.sha224('cesalt'+rq.user.email).hexdigest()[:15]

    m = "<p><strong>%s, welcome to The Campus Elite!</strong></p><br>" % rq.user.first_name

    m += "<p>Follow this link to verify your account and kick-start your experience:</p>"
    m += "<p><a href=\"%s/verify-account/%s\">%s/verify-account/%s</a></p>" % (settings.MAIN_SITE, passkey, settings.MAIN_SITE, passkey)
    m += "<br><p>Thanks!</p>"
    m += "<p>- Campus Elite</p>"

    t = compile_email_template({ "body_content": m })
    send_user_email( rq, {
        "subject": rq.user.first_name+", verify your email address",
        "message": t,
        "to": [rq.user.email],
        "from": 'Campus Elite <alerts@thecampuselite.com>',
    } )



def MessageHelper_CreateNotification( options ):

    n = CeNotification()
    n.user = options['user'] # required
    n.text = options['text'] # required
    n.icon = options['icon'] # required
    n.link = options['link'] # required
    n.primary_image = options['primary_image'] # required
    n.preview_image = options.get('preview_image', '') 
    n.save();

    return n


def GroupBuddyupNotification( g, user ):
    MessageHelper_CreateNotification({
        "user": user,
        "text": "<strong>"+g.creator.full_name()+"</strong> wants to BuddyUp with you:<br>&rarr; <strong>"+g.name+"</strong>",
        "icon": 4,
        "link": g.details_path(),
        "primary_image": g.creator.profile.avatar_thumb_path,
    })

    text = "<strong>"+g.creator.full_name()+"</strong> wants to BuddyUp with you:<p></p><strong>"+g.name+"</strong>"
    subject = g.creator.first_name+" wants to BuddyUp with you"

    # if they shut this off... skip it
    if user.notification_settings.essential_alerts:
        m = "<p>Hi there!</p><p>Just letting you know that "+text+"</p>"
        m += "<p><a href='"+g.details_path_absolute()+"'>Go to event &rarr;</a></p><br>"
        m += '''<p>Keep on keepin' on,</p>
        <p>Campus Elite</p>
        '''

        t = compile_email_template({ "body_content": m })
        send_user_email( message={
            "subject": subject,
            "message": t,
            "to": ['highwaytoinfinity@gmail.com' if settings.DEBUG else user.email],
            "from": 'Campus Elite <alerts@thecampuselite.com>',
        } )



def GroupAttendRequestNotification( g, user ):
    MessageHelper_CreateNotification({
        "user": g.creator,
        "text": "<strong>"+user.full_name()+"</strong> has requested to attend your event \""+g.name+"\".",
        "icon": 4,
        "link": g.details_path(),
        "primary_image": user.profile.avatar_thumb_path,
    })


def GroupAttendApproveNotification( g, user ):
    MessageHelper_CreateNotification({
        "user": user,
        "text": "<strong>"+g.creator.full_name()+"</strong> has accepted your request to attend \""+g.name+"\".",
        "icon": 5,
        "link": g.details_path(),
        "primary_image": g.creator.profile.avatar_thumb_path,
    })


def GroupEditedNotification( g, user ):
    f = '%Y-%m-%d %H:%M:%S'
    dt = datetime.datetime.strptime(g.event_datetime, f)
    newtime = dt.strftime("%a, %b %d at %I:%M%p")
    MessageHelper_CreateNotification({
        "user": user,
        "text": "<strong>"+g.group.creator.full_name()+"</strong> has changed the time for their event \""+g.group.name+"\".<br><strong>"+newtime+"</strong>",
        "icon": 4,
        "link": g.group.details_path(),
        "primary_image": g.group.creator.profile.avatar_thumb_path,
    })

def GroupDeletedNotification( g, user ):
    MessageHelper_CreateNotification({
        "user": user,
        "text": "<strong>"+g.group.creator.full_name()+"</strong> has cancelled the event time for their event \""+g.group.name+"\". Click to view all dates.",
        "icon": 4,
        "link": g.group.details_path(),
        "primary_image": g.group.creator.profile.avatar_thumb_path,
    })


def UserLikeNotification( p, user ):
    data = p.get_data()

    if p.creator.pk == user.pk: 
        return False

    preview_img = ""

    if p.post_type == 1:
        if data.caption:
            text = "<strong>"+user.full_name()+"</strong> gave you a fist bump on your post \""+data.caption+"\".";
        else:
            text = "<strong>"+user.full_name()+"</strong> gave you a fist bump on your photo post.";
        preview_img = data.photoset.all()[:1][0]
        preview_img = preview_img.photo_file_thumb

    if p.post_type == 2:
        if data.caption:
            text = "<strong>"+user.full_name()+"</strong> gave you a fist bump on your post \""+data.caption+"\".";
        else:
            text = "<strong>"+user.full_name()+"</strong> gave you a fist bump on your video post.";

    if p.post_type == 3:
        text = "<strong>"+user.full_name()+"</strong> gave you a fist bump on your post \""+data.caption+"\".";
    if p.post_type == 6:
        text = "<strong>"+user.full_name()+"</strong> gave you a fist bump on your workout log for \""+data.workout.name+"\"";


    MessageHelper_CreateNotification({
        "user": p.creator,
        "text": text,
        "icon": 1,
        "link": p.get_public_path(),
        "primary_image": user.profile.avatar_thumb_path,
        "preview_image": preview_img,
    })



def UserCommentNotification( pid, user, comment ):

    try:
        p = CePost.objects.get(pk=pid)
    except CePost.DoesNotExist:
        return False

    if p.creator.pk == user.pk: 
        return False

    data = p.get_data()

    preview_img = ""

    if p.post_type <= 3:
        text = "<strong>"+user.full_name()+"</strong> commented on your post \""+comment+"\".";
        subject = user.first_name+" commented on your post"
        if p.post_type == 1:
            preview_img = data.photoset.all()[:1][0]
            preview_img = preview_img.photo_file_thumb

    if p.post_type == 6:
        text = "<strong>"+user.full_name()+"</strong> commented on your workout log for \""+data.workout.name+"\"";
        subject = user.first_name+" commented on your workout log"


    MessageHelper_CreateNotification({
        "user": p.creator,
        "text": text,
        "icon": 2,
        "link": p.get_public_path(),
        "primary_image": user.profile.avatar_thumb_path,
        "preview_image": preview_img,
    })


    # if they shut this off... skip it
    if p.creator.notification_settings.interaction_alerts:
        m = "<p>"+text+"</p>"
        m += "<p><a href='"+settings.MAIN_SITE+p.get_public_path()+"'>Go to post &rarr;</a></p><br>"
        m += '''<p>Keep on keepin' on,</p>
        <p>Campus Elite</p>
        '''

        t = compile_email_template({ "body_content": m })
        send_user_email( message={
            "subject": subject,
            "message": t,
            "to": ['highwaytoinfinity@gmail.com' if settings.DEBUG else p.creator.email],
            "from": 'Campus Elite <alerts@thecampuselite.com>',
        } )




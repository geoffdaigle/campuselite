
from ce_diet.models import *
from campuselite import settings

def DietHelper_FollowerPackage( user, rq_user ):
    us = list( CeDietFollow.objects.filter(diet_user=user) )
    user_list = []
    for u in us:
    	user_list.append( u.user )
    return ( user_list, rq_user, 'is following this diet', 'are following this diet', user.profile_path()+'/diet/followers' )
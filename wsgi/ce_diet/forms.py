from django import forms as f
from ce_diet.models import *
from ce_user.models import *

'''
    Data validation forms related to this app
'''

class DietFollowForm(f.Form):
    user = f.IntegerField(min_value=1)
    diet_user = f.IntegerField(min_value=1)

    def save_data( self ):
        cd = self.cleaned_data  
        m = CeDietFollow()

        try:
            user = CeUser.objects.get(pk=cd['user'])
            diet_user = CeUser.objects.get(pk=cd['diet_user'])
        except CeUser.DoesNotExist:
            return { "status": "error" }

        m.user = user
        m.diet_user = diet_user
        m.save()
        return { "status": "success", "id": m.pk }


class DietForm(f.ModelForm):
    class Meta:
        model = CeDiet
        fields = ['calories', 'fat', 'protein', 'carbs', 'description']

    def save_changes( self, user, create_post=True ):

        # save this user's new diet
        diet = self.save(commit=False)
        if diet.calories is None:
            diet.calories = 0
        if diet.fat is None:
            diet.fat = 0
        if diet.protein is None:
            diet.protein = 0
        if diet.carbs is None:
            diet.carbs = 0
        diet.user = user
        diet.save()

        # create a new auto-post to update their friends
        if create_post:
            # TODO
            testing = True

        return diet

    


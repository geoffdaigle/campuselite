# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CeDiet'
        db.create_table(u'ce_diet_cediet', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='diets', to=orm['ce_user.CeUser'])),
            ('description', self.gf('django.db.models.fields.TextField')(max_length=10000, blank=True)),
            ('calories', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('fat', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('protein', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('carbs', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('datetime_updated', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'ce_diet', ['CeDiet'])

        # Adding model 'CeDietFollow'
        db.create_table(u'ce_diet_cedietfollow', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='followed_diets', to=orm['ce_user.CeUser'])),
            ('diet_user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='following_diet', to=orm['ce_user.CeUser'])),
            ('datetime_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'ce_diet', ['CeDietFollow'])


    def backwards(self, orm):
        # Deleting model 'CeDiet'
        db.delete_table(u'ce_diet_cediet')

        # Deleting model 'CeDietFollow'
        db.delete_table(u'ce_diet_cedietfollow')


    models = {
        u'ce_diet.cediet': {
            'Meta': {'ordering': "['-datetime_updated']", 'object_name': 'CeDiet'},
            'calories': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'carbs': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'datetime_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '10000', 'blank': 'True'}),
            'fat': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'protein': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'diets'", 'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_diet.cedietfollow': {
            'Meta': {'ordering': "['-datetime_created']", 'object_name': 'CeDietFollow'},
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'diet_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'following_diet'", 'to': u"orm['ce_user.CeUser']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'followed_diets'", 'to': u"orm['ce_user.CeUser']"})
        },
        u'ce_user.ceuser': {
            'Meta': {'object_name': 'CeUser'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'followers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'following'", 'symmetrical': 'False', 'through': u"orm['ce_user.CeUserFollow']", 'to': u"orm['ce_user.CeUser']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'verified': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'ce_user.ceuserfollow': {
            'Meta': {'ordering': "['-datetime_created']", 'object_name': 'CeUserFollow'},
            'datetime_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'follower_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'follower_user'", 'to': u"orm['ce_user.CeUser']"}),
            'following_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'following_user'", 'to': u"orm['ce_user.CeUser']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['ce_diet']
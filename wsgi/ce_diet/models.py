from django.db import models as m


'''
    A one-to-many diet object which creates a new row for every update
'''
class CeDiet( m.Model ):
    user = m.ForeignKey('ce_user.CeUser', related_name="diets")
    description = m.TextField(max_length=10000, blank=True)
    calories = m.IntegerField(blank=True)
    fat = m.IntegerField(blank=True)
    protein = m.IntegerField(blank=True)
    carbs = m.IntegerField(blank=True)
    datetime_updated = m.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-datetime_updated']






# Allows users to follow a diet ( simpy a user-to-user follow because users cant have multiple diets )
class CeDietFollow( m.Model ):
    user = m.ForeignKey('ce_user.CeUser', related_name="followed_diets")
    diet_user = m.ForeignKey('ce_user.CeUser', related_name="following_diet")
    datetime_created = m.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-datetime_created']

    def __unicode__(self):
        return 'Diet follow: '+self.user.full_name()+' --> '+self.diet_user.full_name()

    def get_diet( self ):
        return self.diet_user.get_diet()

    def get_user_diet( self, serialize=False ):
        if serialize:
            from ce_api import serializers as s

        diet = self.diet_user.get_diet()
        if serialize:
            return s.DietSerializer( diet ).data
        else:
            return diet
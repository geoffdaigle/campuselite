The Campus Elite (CE)
------

__You are about to embark on a wonderful journey...__

Campus Elite is a fitness and motivation platform where users from each school can find, connect, and work out with each other. Its main focus is to:
- motivate through "buddy-finding" or joining groups with other motivators, 
- motivate through "fist bumps", comments, friend support, etc., 
- and to motivate through following role models' posts and diets.

This platform runs on these technologies:

- **Python** 
    * Django | https://www.djangoproject.com/
    * Django Rest Framework (occasionally) | http://www.django-rest-framework.org/
    * South (to support DB migrations - still new to Django)
    * Pillow (image manipulation)
- **Styling and skinning**
    * LessCSS | http://lesscss.org/
- **Javascript - server side**
    * NodeJS | http://nodejs.org/
    * Gulp | http://gulpjs.com/
    * Hogan | http://twitter.github.io/hogan.js/
- **Javascript - client side**
    * jQuery
    * modernizr.js | http://modernizr.com/
    * Hogan (again, sparingly)
    * jQuery Plugins (see static/js-dev/libs/ for all of them)
    * XDate | http://arshaw.com/xdate/
    * imagesloaded.js | http://imagesloaded.desandro.com/
- **Hardware**
    * Amazon EC2 w/ apache (two boxes - one for static/home pages, one for python app)
    * Amazon Elastic Beanstalk - single instance mode
    * Amazon S3
    * Amazon RDS w/ MySQL 5.6


__Local Environment Setup:__

- Install virtualenv (http://virtualenv.readthedocs.org/en/latest/virtualenv.html#installation)

- Install mkvirtualenv (http://virtualenvwrapper.readthedocs.org/en/latest/install.html)

- Make sure Node.js and NPM are installed (http://nodejs.org/)

- Create a virtual environment for CE - "mkvirtualenv **[your_environment_name]**". This will let you easily install python libaries without getting conflicts due to whats already installed on your computer.

- Make sure MySQL >= 5.6 is installed and running somewhere

- If on OSX: Add "PATH=$PATH:/usr/local/mysql/bin" to your ~/.virtualenvs/**[your_environment_name]**/bin/activate right before "export PATH" so that the mysql python module can find mysql.sock

- Run "workon **[your_environment_name]**" to update your PATH vars

- Clone the CE repository "git clone https://bitbucket.org/geoffdaigle/campuselite.git" DO NOT CLONE INTO /home/ OR DJANGO WILL THINK IT'S IN PRODUCTION MODE.

- Navigate to the CE folder and run "python setup.py" to install python app dependencies... _you might have trouble installing mysql-python and Pillow but usually you just have to install those separately with pip to solve the issue_.

- Run "python manage.py syncdb" to add initial db tables

- Run "python manage.py migrate" to add additional db tables

- Run "npm install" to install the build script dependencies 

- Install gulp.js globally (for easy command line useage) --> run "npm install -g gulp"
    
- Edit **settings.py** to use your local path and database settings

- Run "python manage.py runserver" to run the django test server on http://localhost:8000
- _(Alternatively, run "python manage.py runserver 1234" to run the django test server on http://localhost:1234)_

- Run "sh runtests" to run the django test suite and health-check your code. Failures means you may not have the app configured correctly. At this moment there are no tests (except a short passing one) but this is something that should be done soon... especially when deciding to add new features.

- Run "gulp" to start the task runner -- this will automatically compile your JS, LESS, and run your javascript tests (if any).

*Now you are ready to code!*



**TO PRECOMPILE HOGAN TEMPLATES FROM WEBROOT:**

- Make sure "hulk" is installed (should be if you installed Hogan.js with NPM)

- run ./compilehogan from the CE webroot directory

- _(Running this script will remove the first line of each compiled file automatically -- otherwise, hogan's attempt to define a window.property variable breaks the compiled JS)_


**TO LOG IN TO AWS SERVER INSTANCES:**

- Log in to aws console (ask prady for creds)
- Select EC2 Box to connect to
- Click "connect"
- Follow instructions for ssh connection with terminal OR use built-in java client.
- Does not firewall specific IPs (but think about changing that soon)

**TO LOG IN TO RDS WITH MYSQL WORKBENCH OR SIMILAR PROGRAM:**

- Ask me for creds
- Does not firewall specific IPs (but think about changing that soon)

**TO UPDATE WEBSERVER:**

- Change the LIBRARY_VERSION number in settings.py if you edit JS or CSS to avoid browser caching issues.
- Push changes to origin/master with git
- Log in to EC2 Python Application server
- run "sudo su" - **this is important because of the permissions git needs**
- run "sh ~/s_up.sh" to fetch/merge from git
- run "sh ~/s_migrate.sh" to do migrations if you need to
- run "sh ~/s_restart.sh" to restart webserver
- IF THE BOX IS TERMINATED - run "sh ~/s_config.sh" to copy the correct server config file into the config folder after new instance is started, then run s_up and s_restart to make sure you are up-to-date and online.

**WHAT ABOUT "COLLECTSTATIC"?**

The app in production mode is assuming that all static files are being served from S3. I have not fully set up the botoS3 pipeline, but I change only one of three css/js files at any given time. I just upload to S3 by hand right now.

On the other hand, image uploads from users are going straight to S3 already which will prevent a data-loss catastrophy if the site goes down.

**KEY DEVELOPMENT LOCATIONS INSIDE /wsgi/**

- **/ce_app/view.py** is where 90% of the app's page controllers are located.
- **/ce_api/view.py** is the REST framework. Not much being used (some is, look in the app's javascript) but the groundwork is laid out for a full-blown API.
- **/campuselite/views.py** is where login/join/other page controllers are located
- Other directories are used for specific model/form/helper groups which are imported into the main controller files.
- There are some left-over config files from when I was testing on Openshift (https://www.openshift.com/) but those arent being used anymore. Delete them unless you want to set up your own dev site with them.

**SOCIAL INTEGRATIONS OR ANYTHING?**

Nope, we are just using what the social networks give away for free sharing tools.

**ADD THIS DATA INTO DATABASE OR ELSE WORKOUTS WILL BREAK**

INSERT INTO `ce_workout_ceworkouttype` (`name`, `reps`, `weight`, `time`, `score`, `distance`) VALUES ('Sport - Points, Goals', '0', '0', '0', '1', '0');

INSERT INTO `ce_workout_ceworkouttype` (`name`, `reps`, `weight`, `time`, `score`, `distance`) VALUES ('Lifting - Weight and Reps', '1', '1', '0', '0', '0');

INSERT INTO `ce_workout_ceworkouttype` (`name`, `reps`, `weight`, `time`, `score`, `distance`) VALUES ('Cardio - Time and Distance', '0', '0', '1', '0', '1');

**DEBUGGING**

Currently, if system throws 500 errors it may not provide much explanation. The easiest way to diagnose until a more detailed method is implemented is to add "print SOMETHING" in various places in the suspected code, run the breaking feature, and run "sh s_log.sh" to see at which statement the code decided to die.

This is not ideal, obviously, but for a pretty small system this will give you a quick way to find the problematic code lines.

**CONTACT ME**

geoffreydaigle@gmail.com

                    